package id.nata.access360.network;

import java.util.List;

import id.nata.access360.model.Property;
import id.nata.access360.model.ResponeModel;
import id.nata.access360.view.downline.model.DownlineList;
import id.nata.access360.view.history.model.CallHistoryData;
import id.nata.access360.view.history.model.PersonalLeadHistoryData;
import id.nata.access360.view.history.model.ResponseHistoryData;
import id.nata.access360.view.hubungiKami.model.ContactUsResponse;
import id.nata.access360.view.intro.model.LaunchResponse;
import id.nata.access360.view.intro.model.SplashScreenRespone;
import id.nata.access360.view.lead.model.LeadList;
import id.nata.access360.view.lead.model.LeadProjectList;
import id.nata.access360.view.lead.model.ListResponse;
import id.nata.access360.view.lead.model.ListResponseGroup;
import id.nata.access360.view.lead.model.UpdateLeadResponse;
import id.nata.access360.view.login.model.LoginRespone;
import id.nata.access360.view.menuBank.Model.BankInformationResponse;
import id.nata.access360.view.menuBank.Model.BankListData;
import id.nata.access360.view.menuMain.model.MenuResponse;
import id.nata.access360.view.menuProfile.model.CityModel;
import id.nata.access360.view.menuProfile.model.CountryModel;
import id.nata.access360.view.menuProfile.model.ProfileACResponse;
import id.nata.access360.view.menuProfile.model.ProfileImageResponse;
import id.nata.access360.view.menuProfile.model.ProfileInfoResponse;
import id.nata.access360.view.menuProfile.model.ProvinceModel;
import id.nata.access360.view.menuProfile.model.UpdateProfileInformationResponse;
import id.nata.access360.view.note.model.NoteExpandResponse;
import id.nata.access360.view.nup.model.ListNupDetail;
import id.nata.access360.view.nup.model.StatusNup;
import id.nata.access360.view.register.model.RegisterRespone;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface NetworkService {

    //register
    @FormUrlEncoded
    @POST("POST_Registration")
    Call<RegisterRespone> PostRegisterUserSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostLoginSVC")
    Call<LoginRespone> PostLoginUserSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostLoginGoogleSVC")
    Call<LoginRespone> PostLoginGoogleSVC(@Field("JSON_IN") String jsonIn);

    //list invest property
    @FormUrlEncoded
    @POST("GetListPropertySVC")
    Call<List<Property>> GetListPropertySVC(@Field("JSON_IN") String jsonIn);

    //post sms code
    @FormUrlEncoded
    @POST("POST_VerificationCode")
    Call<LoginRespone> PostRegistrationCodeSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostResendingRegistrationCodeSVC")
    Call<ResponeModel> PostResendingRegistrationCodeSVC(@Field("JSON_IN") String jsonIn);

    //wishlist post
    @FormUrlEncoded
    @POST("PostWishlistPropertySVC")
    Call<ResponeModel> PostWishlistPropertySVC(@Field("JSON_IN") String jsonIn);

    //wishlist post
    @FormUrlEncoded
    @POST("PostRemoveWishlistPropertySVC")
    Call<ResponeModel> PostRemoveWishlistPropertySVC(@Field("JSON_IN") String jsonIn);

    //list wishlist
    @FormUrlEncoded
    @POST("GetDashboardWishlistSVC")
    Call<List<Property>> GetDashboardWishlistSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostContactUsSVC")
    Call<ResponeModel> PostContactUsSVC(@Field("JSON_IN") String jsonIn);

    //change pass
    @FormUrlEncoded
    @POST("PostChangePasswordSVC")
    Call<ResponeModel> PostChangePasswordSVC(@Field("JSON_IN") String jsonIn);

    //update biodata
    @FormUrlEncoded
    @POST("PostUpdateProfileSVC")
    Call<ResponeModel> PostUpdateProfileSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostUpdateProfileBankSVC")
    Call<ResponeModel> PostUpdateProfileBankSVC(@Field("JSON_IN") String jsonIn);

    //rsvp
    @FormUrlEncoded
    @POST("PostRSVPEventSVC")
    Call<ResponeModel> PostRSVPEventSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("POST_ForgotPassword")
    Call<ResponeModel> PostForgotPasswordSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostPropertyShareSellSVC")
    Call<ResponeModel> PostPropertyShareSellSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostPropertyShareSellCancelSVC")
    Call<ResponeModel> PostPropertyShareSellCancelSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostNotificationReadDeleteSVC")
    Call<ResponeModel> PostNotificationReadDeleteSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("PostChargeCreditCardResponseSVC")
    Call<ResponeModel> PostChargeCreditCardResponseSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GetAppsInfoSvc")
    Call<SplashScreenRespone> GetVersionApps(@Field("JSON_IN") String jsonIn);


    @FormUrlEncoded
    @POST("PostCancelInvestPropertySVC")
    Call<ResponeModel> PostCancelInvestPropertySVC(@Field("JSON_IN") String jsonIn);


    @FormUrlEncoded
    @POST("PostDisbursementSaldoSVC")
    Call<ResponeModel> PostDisbursementSaldoSVC(@Field("JSON_IN") String jsonIn);

    ////////
    @FormUrlEncoded
    @POST("POST_LoginSvc")
    Call<LoginRespone> POST_LoginSvc(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_MenuInformation")
    Call<MenuResponse> GET_MenuInformation(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ProfileLookup")
    Call<ProfileACResponse> GET_ProfileLookup(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_BankInformation")
    Call<BankInformationResponse> GET_BankInformation(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_StatusNUP")
    Call<List<StatusNup>> GET_StatusNUP(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListCountry")
    Call<List<CountryModel>> GET_ListCountry(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListProvince")
    Call<List<ProvinceModel>> GET_ListProvince(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListCity")
    Call<List<CityModel>> GET_ListCity(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ProfileInformation")
    Call<ProfileInfoResponse> GET_ProfileInformation(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_BankLookup")
    Call<List<BankListData>> GET_BankList(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListNUP")
    Call<List<ListNupDetail>> GET_ListNUP(@Field("JSON_IN") String jsonIn);


    @FormUrlEncoded
    @POST("POST_UpdateProfileInformation")
    Call<UpdateProfileInformationResponse> POST_UpdateProfileInformation(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("POST_UpdatePersonalBank")
    Call<BankInformationResponse> POST_UpdatePersonalBank(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListDownline")
    Call<List<DownlineList>> POST_DownlineList(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListResponseGroup")
    Call<List<ListResponseGroup>> GET_ResponseGroup(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListPersonalLead")
    Call<List<LeadList>> GET_LeadList(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListResponse")
    Call<List<ListResponse>> GET_ListResponse(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("POST_UpdateLeadResponse")
    Call<UpdateLeadResponse> POST_UpdateLead(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("POST_UpdateCallLead")
    Call<UpdateLeadResponse> POST_UpdateCall(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("POST_UpdateFCMToken")
    Call<ResponeModel> PostFCMTokenSVC(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListResponseHistory")
    Call<List<ResponseHistoryData>> GET_ResponseHistory(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListCallHistory")
    Call<List<CallHistoryData>> GET_CallHistory(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListPersonalLeadHistory")
    Call<List<PersonalLeadHistoryData>> GET_PersonalLeadHistory(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_LeadNoted")
    Call<NoteExpandResponse> GET_NoteExpandResponse(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("POST_UpdatePhotoProfile")
    Call<ProfileImageResponse> POST_ImageProfile(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_AppVersionSvc")
    Call<LaunchResponse> GET_LaunchResponse(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("POST_ContactUs")
    Call<ContactUsResponse> POST_ContactUs(@Field("JSON_IN") String jsonIn);

    @FormUrlEncoded
    @POST("GET_ListPersonalProject")
    Call<List<LeadProjectList>> GET_LeadProjectList(@Field("JSON_IN") String jsonIn);
}
