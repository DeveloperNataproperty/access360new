package id.nata.access360.network;



import java.io.IOException;
import java.util.concurrent.TimeUnit;

import id.nata.access360.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nata on 12/28/2016.
 */

public class XenditNetworkModule {
    private XenditNetworkService networkAPI;
    private OkHttpClient okHttpClient;

    public XenditNetworkModule( ) {
        okHttpClient = buildClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.XENDIT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        networkAPI = retrofit.create(XenditNetworkService.class);
    }

    /**
     * Method to return the API interface.
     *
     * @return
     */
    public XenditNetworkService getAPI() {
        return networkAPI;
    }


    /**
     * Method to build and return an OkHttpClient so we can set/get
     * headers quickly and efficiently.
     *
     * @return
     */
    public OkHttpClient buildClient() {

        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS);

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                // Do anything with response here
                //if we ant to grab a specific cookie or something..
                return response;
            }
        });

        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
//                        .addHeader("Authorization","Basic eG5kX2RldmVsb3BtZW50X05ZeURmTDBpMCtDdWtzNC9mckZOVERmR1lOSHg5WU42d1hXeitSeGkrMlBlK0wyaEN3RjBnUT09Og==")
//                        .addHeader("Authorization","Basic eG5kX2RldmVsb3BtZW50X1BZaUZLT290M2VENnhjRTZkckljRW1XVFpkYjFvZFY4eEhmaC9SaGw5V1BRK3JXbUNRZDE6") // napro punya
                        .addHeader("Authorization", BuildConfig.XENDIT_SECRET_KEY)
                        .build();
                return chain.proceed(request);
            }
        });

        return builder.build();
    }

}
