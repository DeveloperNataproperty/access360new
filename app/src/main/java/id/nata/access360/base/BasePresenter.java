package id.nata.access360.base;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:04.
 * Project : Napro
 */

public interface BasePresenter<T extends BaseInterface>{

    void setupView(T view);

    void clearView();
}
