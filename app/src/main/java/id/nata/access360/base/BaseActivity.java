package id.nata.access360.base;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import id.nata.access360.R;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:04.
 * Project : Napro
 */

public class BaseActivity extends AppCompatActivity {
    @Nullable
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    //connection
    @Nullable
    @BindView(R.id.layout_no_connection)
    protected View layoutNoConnection;
    @Nullable
    @BindView(R.id.txt_no_connection_title)
    protected TextView txtNoConnectionTitle;
    @Nullable
    @BindView(R.id.btn_retry)
    Button btnRetry;
    //data
    @Nullable
    @BindView(R.id.layout_no_data)
    protected View layoutNoData;
    @Nullable
    @BindView(R.id.txt_no_data_title)
    protected TextView txtNoDataTitle;
    //progress
    @Nullable
    @BindView(R.id.layout_progress)
    View layoutProgress;

    AlertDialog.Builder mDialogBuilder;
    AlertDialog mAlertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //loader
        mDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_progress, null);
        mDialogBuilder.setView(dialogView);
        mAlertDialog = mDialogBuilder.create();
        mAlertDialog.setCancelable(false);
    }

    protected void viewLoader() {
        mAlertDialog.show();
    }

    protected void hiddenLoader() {
        if (mAlertDialog != null && mAlertDialog.isShowing()) {
            mAlertDialog.dismiss();
        }
    }


    /**
     * untuk menampilkan pesan error koneksi
     *
     * @param activity
     */
    protected void viewErrorConnection(final Activity activity) {
        layoutNoConnection.setVisibility(View.VISIBLE);
        txtNoConnectionTitle.setText(getString(R.string.no_internet_connectivity));
        btnRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = activity.getIntent();
                activity.finish();
                activity.startActivity(intent);
            }
        });
    }
    /**
     * tampilan jika tidak ada data
     */
    protected void viewNoDataAvalable() {
        layoutNoData.setVisibility(View.VISIBLE);
        txtNoDataTitle.setText(getString(R.string.no_data_available));
    }

    /**
     * tampilan jika tidak ada data hilang
     */
    protected void hiddenNoDataAvalable() {
        layoutNoData.setVisibility(View.GONE);
    }

    /**
     * show progress bar di dalam layout
     */
    protected void showProgress (){
        layoutProgress.setVisibility(View.VISIBLE);
    }

    /**
     * dismis progress bar di dalam layout
     */
    protected void dismisProgress (){
        layoutProgress.setVisibility(View.GONE);
    }

    /**
     * setup toolbar, untuk menentukan title dan display back-nya
     *
     * @param paramStrTitle
     */
    protected void initToolbar(String paramStrTitle) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(paramStrTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

}
