package id.nata.access360.base;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:04.
 * Project : Napro
 */

public class BaseActivityProjectHeader extends AppCompatActivity {
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.txt_property_name)
    protected TextView txtPropertyName;
    @BindView(R.id.txt_price)
    protected TextView txtPrice;
    @BindView(R.id.img_guarantee)
    protected ImageView imgGuarantee;

    SessionManager mSessionManager;

    private String buyBackGuarantee, investmentPrice, propertyName, unitInfo;

    AlertDialog.Builder mDialogBuilder;
    AlertDialog mAlertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSessionManager = new SessionManager(this);

        buyBackGuarantee = mSessionManager.getStringFromSP(General.BUY_BACK_GUARANTEE);
        investmentPrice = mSessionManager.getStringFromSP(General.INVESTMENT_PRICE);
        propertyName = mSessionManager.getStringFromSP(General.PROPERTY_NAME);
        unitInfo = mSessionManager.getStringFromSP(General.UNIT_INFO);

        mDialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_buy_back, null);
        mDialogBuilder.setView(dialogView);
        mAlertDialog = mDialogBuilder.create();

    }

    protected void setHeader (){
        txtPropertyName.setText(propertyName+"\n"+unitInfo);
        txtPrice.setText("\n"+investmentPrice);
        if (buyBackGuarantee.equals("1")){
            imgGuarantee.setVisibility(View.VISIBLE);
            txtPropertyName.setGravity(Gravity.LEFT);
        } else {
            imgGuarantee.setVisibility(View.GONE);
            txtPropertyName.setGravity(Gravity.LEFT);
        }
    }

    /**
     * setup toolbar, untuk menentukan title dan display back-nya
     * @param paramStrTitle
     */
    protected void initToolbar(String paramStrTitle){
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(paramStrTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.img_guarantee)
    public void setImgGuarantee (){
        mAlertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }

}
