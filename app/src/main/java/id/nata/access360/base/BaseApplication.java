package id.nata.access360.base;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.multidex.MultiDex;
import android.util.Base64;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;

import id.nata.access360.BuildConfig;
import io.fabric.sdk.android.Fabric;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import id.nata.access360.dagger.ApplicationModule;
import id.nata.access360.dagger.DaggerDeps;
import id.nata.access360.dagger.Deps;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.XenditNetworkModule;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:04.
 * Project : Napro
 */

public class BaseApplication extends Application {
    private Deps deps;
    private XenditNetworkModule xenditNetworkModule;
    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
        deps = initDagger(this);

        MultiDex.install(this);
        Realm.init(this);
        RealmConfiguration realmConfiguration =
                new RealmConfiguration.Builder().schemaVersion(1)
                        .deleteRealmIfMigrationNeeded().name("napro.realm").build();
        Realm.setDefaultConfiguration(realmConfiguration);

        xenditNetworkModule = new XenditNetworkModule();

        FacebookSdk.sdkInitialize(getApplicationContext());

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "id.nata.access360",  // replace with your unique package name
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Utils.showLog("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                Utils.showLog("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    public Deps getDeps() {
        return deps;
    }

    protected Deps initDagger(BaseApplication application) {
        return DaggerDeps.builder()
                .applicationModule(new ApplicationModule(application))
                .build();
    }

    public XenditNetworkModule getXenditNetworkModule(){
        return xenditNetworkModule;
    }
}
