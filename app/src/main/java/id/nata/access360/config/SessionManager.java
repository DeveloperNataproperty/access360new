package id.nata.access360.config;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

import id.nata.access360.view.intro.SplashScreenActivity;

import static id.nata.access360.config.General.AREA_REF;
import static id.nata.access360.config.General.CATEGORY_TYPE;
import static id.nata.access360.config.General.EMAIL;
import static id.nata.access360.config.General.FILTER;
import static id.nata.access360.config.General.FULLNAME;
import static id.nata.access360.config.General.IS_LOGIN;
import static id.nata.access360.config.General.IS_NEED_VALIDASI;
import static id.nata.access360.config.General.KEYWORD;
import static id.nata.access360.config.General.MAX_PRICE;
import static id.nata.access360.config.General.MIN_PRICE;
import static id.nata.access360.config.General.PRICE_INVESTMEN;
import static id.nata.access360.config.General.PS_CODE;
import static id.nata.access360.config.General.PS_REF;
import static id.nata.access360.config.General.SUB_LOCATION_REF;
import static id.nata.access360.config.General.URL_VP;
import static id.nata.access360.config.General.USER_IMAGE;
import static id.nata.access360.config.General.USER_REF;


/**
 * Created by "dwist14"
 * on May 5/10/2017 14:45.
 * Project : l-commerce
 */
public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static SessionManager instance;

    // Constructor
    public SessionManager(Context context) {
        this.context = context;
        pref = this.context.getSharedPreferences(General.PREF_NATA, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static SessionManager getInstance(Context context) {
        if (instance == null) {
            instance = new SessionManager(context);
        }
        return instance;
    }

    public void createLoginSession(String paramStrPsRef, String paramStrEmail,
                                   String paramStrUserName, String paramStrPsCode) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(PS_REF, paramStrPsRef);
        editor.putString(EMAIL, paramStrEmail);
        editor.putString(FULLNAME, paramStrUserName);
        editor.putString(PS_CODE, paramStrPsCode);
        editor.commit();
    }

    public void urlViewProjectSession(String paramStrUrlVP) {
        editor.putString(URL_VP, paramStrUrlVP);
        editor.commit();
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        SharedPreferences pref = context.getSharedPreferences(General.PREF_NATA, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, SplashScreenActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(i);
    }

    public void setNeedValidasi(boolean isActive) {
        editor.putBoolean(IS_NEED_VALIDASI, isActive);
        editor.commit();
    }

    public void setFilter(String keyword, String categoryType, String areaRef, String subLocationRef,
                          String priceInvestmen, String minPrice, String maxPrice) {
        editor.putBoolean(FILTER, true);
        editor.putString(KEYWORD, keyword);
        editor.putString(CATEGORY_TYPE, categoryType);
        editor.putString(AREA_REF, areaRef);
        editor.putString(SUB_LOCATION_REF, subLocationRef);
        editor.putString(PRICE_INVESTMEN, priceInvestmen);
        editor.putString(MIN_PRICE, minPrice);
        editor.putString(MAX_PRICE, maxPrice);
        editor.commit();
    }

    public HashMap<String, String> getFilter() {
        HashMap<String, String> project = new HashMap<String, String>();
        project.put(KEYWORD, pref.getString(KEYWORD, ""));
        project.put(CATEGORY_TYPE, pref.getString(CATEGORY_TYPE, ""));
        project.put(AREA_REF, pref.getString(AREA_REF, ""));
        project.put(PRICE_INVESTMEN, pref.getString(PRICE_INVESTMEN, ""));
        project.put(MIN_PRICE, pref.getString(MIN_PRICE, "0"));
        project.put(MAX_PRICE, pref.getString(MAX_PRICE, "0"));
        return project;
    }

    public void resetFilter() {
        editor.putBoolean(FILTER, false);
        editor.remove(General.KEYWORD);
        editor.remove(General.CATEGORY_TYPE);
        editor.remove(General.AREA_REF);
        editor.remove(General.PRICE_INVESTMEN);
        editor.remove(General.MIN_PRICE);
        editor.remove(General.MAX_PRICE);
        editor.commit();

    }


    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public boolean is_Need_Validasi() {
        return pref.getBoolean(IS_NEED_VALIDASI, false);
    }

    public boolean is_Filter() {
        return pref.getBoolean(FILTER, false);
    }

    public String getStringFromSP(String valName) {
        return pref.getString(valName, "");
    }

    public void setStringToSP(String valName, String value) {
        editor.putString(valName, value);
        editor.commit();
    }

    public void setIntToSP(String valName, int value) {
        editor.putInt(valName, value);
        editor.commit();
    }

    public int getIntFromSP(String valName) {
        return pref.getInt(valName, 0);
    }

}
