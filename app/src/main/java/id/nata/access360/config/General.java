package id.nata.access360.config;

import id.nata.access360.BuildConfig;

/**
 * Created by "dwist14"
 * on May 5/9/2017 20:11.
 * Project : l-commerce
 */
public class General {
    //STATUS
    public static final int API_REQUEST_SUCCESS = 200;
    public static final int API_REQUEST_FAILED = 201;
    public static final int API_REQUEST_ACCOUNT_NOT_ACTIVE = 202; // 300
    public static final int API_REQUEST_ACCOUNT_NOT_EXIST = 203;

    public static final String VENDOR_XENDIT = "2";
    public static final String VENDOR_MIDTRANS= "3";

    //content
    public static final String TAG_ABOUT = "3";
    public static final String TAG_TERM_AND_CONDITION = "7";
    public static final String LINK_TERM_AND_CONDITION = "id.napro.nabungproperti.view.about.ui.TermAndConditionActivity";
    public static final String TAG_FAQ = "5";
    public static final String TAG_PRIVACY_POLICE = "6";
    public static final String TAG_USAGE_RULES = "7";
    public static final String TAG_BLOG = "8";
    public static final String TAG_PRESS = "9";

    //firebase
    public static final String SHARED_PREF = "shard_firebase";

    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static final String NOTIFICATION_LEAD = "1";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    //id KEY TOKEN
    public static final String FCM_TOKEN = "fcmToken";

    public static final String YOUTUBE_API_KEY = "AIzaSyAyZhmyEuSo2-flUF68xz0LXf0Qx9uJQg8";

    public static final String CLIENT_WEB = "AIzaSyBq6Hu_eLKYtnKMY1PDAWsKS9dQ2-BichU";

    public static final String IS_NEED_VALIDASI = "isNeedValidasi";
    public static final String NAPROID = "NaproID";

    public static final String ID_GOOGLE = "idGoogle";
    public static final String ID_FACEBOOK = "idFacebook";

    public static final String PHONE_WHATSHAPP = "+62811183381";
    public static final String PHONE_CALL = "02180821638";
    public static final String EMAIL_SUPPORT = "info@napro.id";
    public static final String EMAIL_SUBJECT = "Feedback dari Napro.id";
    public static final String PACKAGE_WHATSHAPP = "com.whatsapp";
    public static final String FACEBOOK = "NaPro.id";
    public static final String PACKAGE_PLAYSTORE = "id.napro.nabungproperti";
    public static final String WEBSITE = "https://www.napro.id/";
    public static final String INSTAGRAM = "napro.id";
    public static final String YOUTUBE = "UCAu1nwBspEaBl-8zmS_CCDg";

    //KEY PREF
    public static final String PREF_NATA = "NataPref";
    public static final String IS_LOGIN = "isLogin";
    public static final String PS_REF = "psRef";
    public static final String USER_REF = "2";
    public static final String USER_IMAGE = "userImage";
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String PS_CODE = "psCode";
    public static final String URL_VP = "urlVp";
    public static final String HP = "hp";
    public static final String SIMPLE_FORMAT_DATE = "dd/MM/yyyy";
    public static final String SIMPLE_FORMAT_DATE2 = "MM/yyyy";
    public static final String SIMPLE_FORMAT_DATE3 = "M";
    public static final String SIMPLE_FORMAT_DATE4 = "dd";
    public static final String UNIT_INFO = "unitInfo";
    public static final String REF = "Ref";
    public static final String LOT_SAHAM = "lotSaham";

    public static final String OPEN_INVEST = "2";
    public static final String FULYFUNDED = "3";

    public static final String SCHADULE_REF = "schadule_ref";
    public static final String SCHADULE_DATE = "schadule_date";
    public static final String CONTENT_REF = "contentRef";

    public static final String PROPERTY_REF = "propertyRef";
    public static final String DESCRIPTION = "description";
    public static final String SYNOPSIS = "synopsis";

    public static final String REGISTER_FROM_ANDROID = "1";

    public static final String IS_PUBLIC = "1"; //public
    public static final String IS_NOT_PUBLIC = "2"; //private

    public static final String IS_ON_PROPERTY_INVEST = "1";
    public static final String IS_NOT_ON_PROPERTY_INVEST = "0";

    public static final String LOGIN_SOURCE = "2"; // login from android
    public static final String POSISION = "position";
    public static final String IS_MOBILE = "1";
    public static final String SOURCE_TYPE = "2";
    public static final String BUY_BACK_GUARANTEE = "buyBackGuarantee";
    public static final String PROPERTY_NAME = "propertyName";
    public static final String INVESTMENT_PRICE_NOT_FORMAT = "investmentPriceNotFormat";
    public static final String UNIT = "unit";

    //FILTER
    public static final String FILTER = "filter";
    public static final String KEYWORD = "keyword";
    public static final String CATEGORY_TYPE = "categoryType";
    public static final String AREA_REF = "areaRef";
    public static final String PRICE_INVESTMEN = "priceInvestmen";
    public static final String MIN_PRICE = "minPrice";
    public static final String MAX_PRICE = "maxPrice";
    public static final String SUB_LOCATION_REF = "subLocation";

    public static final String MENU_TITLE = "menuTitle";
    public static final String PROPERTY_SHARE_REF = "propertyShareRef";

    //Key untuk permision result
    public static final int READ_PHONE_STATE_CONSTAT = 0;

    public static final String URL_MIDTRANS = "urlMidtrans";
    public static final String PAYMENT_CODE = "paymentCode";
    public static final String PAYMENT_REF = "paymentRef";

    public static final String INVESTMENT_PRICE = "investmentPrice";

    public static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

    public static final double RASIO_IMAGE_PROJECT = 1.777777777777778;
    public static final double RASIO_IMAGE_BANNER = 2;

    public static final String FOLDER_NAME = "Napro.id";
    public static final String IMAGE_NAME = "napro_image";

    public static final String SALDO_DEPOSIT = "saldoDeposite";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String URL_VIDEO = "urlVideo";

    public final static String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus",
            "September", "Oktober", "November", "Desember",};

    public final static String URL_INVESTMENT_SCHEMS = BuildConfig.BASEURL + "naproid/skema-investasi-apps";

    public static final String TITLE = "title";
    public static final String LINK_DETAIL = "linkDetail";
    public static final int XENDIT_CHARGE_TRANSFER = 4500;

}
