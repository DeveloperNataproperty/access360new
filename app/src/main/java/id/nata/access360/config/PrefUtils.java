package id.nata.access360.config;

import android.content.Context;

import id.nata.access360.view.register.model.UserInfoFacebookModel;


/**
 * Created by nata on 4/10/2017.
 */

public class PrefUtils {
    public static void setCurrentUser(UserInfoFacebookModel currentUser, Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.putObject("current_user_value", currentUser);
        complexPreferences.commit();
    }

    public static UserInfoFacebookModel getCurrentUser(Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        UserInfoFacebookModel currentUser = complexPreferences.getObject("current_user_value", UserInfoFacebookModel.class);
        return currentUser;
    }

    public static void clearCurrentUser( Context ctx){
        ComplexPreferences complexPreferences = ComplexPreferences.getComplexPreferences(ctx, "user_prefs", 0);
        complexPreferences.clearObject();
        complexPreferences.commit();
    }
}
