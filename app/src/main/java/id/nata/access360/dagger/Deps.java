package id.nata.access360.dagger;

import javax.inject.Singleton;

import dagger.Component;
import id.nata.access360.fcm.process.MyFirebaseInstanceIDServicePresenter;
import id.nata.access360.view.downline.process.DownlinePresenter;
import id.nata.access360.view.history.process.CallHistoryPresenter;
import id.nata.access360.view.history.process.PersonalLeadHistoryPresenter;
import id.nata.access360.view.history.process.ResponseHistoryPresenter;
import id.nata.access360.view.hubungiKami.process.HubungiKamiPresenter;
import id.nata.access360.view.intro.process.LaunchPresenter;
import id.nata.access360.view.intro.process.SplashScreenPresenter;
import id.nata.access360.view.lead.process.LeadListDetailPresenter;
import id.nata.access360.view.lead.process.LeadPresenter;
import id.nata.access360.view.lead.process.LeadProjectPresenter;
import id.nata.access360.view.login.process.ForgotPasswordPresenter;
import id.nata.access360.view.login.process.LoginPresenter;
import id.nata.access360.view.login.ui.LoginActivity;
import id.nata.access360.view.menuBank.Process.BankPresenter;
import id.nata.access360.view.menuBank.ui.InformasiBankActivity;
import id.nata.access360.view.menuMain.process.MenuMainPresenter;
import id.nata.access360.view.menuProfile.process.ProfileACPresenter;
import id.nata.access360.view.menuProfile.process.ProfileImagePresenter;
import id.nata.access360.view.note.process.NoteExpandPresenter;
import id.nata.access360.view.nup.process.ListNupDetailPresenter;
import id.nata.access360.view.nup.process.StatusNupPresenter;
import id.nata.access360.view.register.process.RegisterFormPresenter;
import id.nata.access360.view.register.process.RegisterOptionPresenter;
import id.nata.access360.view.register.process.RegisterValidasiPresenter;
import id.nata.access360.view.register.ui.RegisterFormActivity;
import id.nata.access360.view.register.ui.RegisterValidasiActivity;


@Singleton
@Component(modules = {ApplicationModule.class, NetworkModule.class,
        PresenterModule.class, SessionManagerModule.class})
public interface Deps {

    void inject(RegisterFormPresenter registerPresenter);

    void inject(RegisterFormActivity registerActivity);

    void inject(LoginPresenter loginPresenter);

    void inject(LoginActivity loginActivity);

    void inject(RegisterValidasiPresenter registerValidasiPresenter);

    void inject(RegisterValidasiActivity registerValidasi);

    void inject(ForgotPasswordPresenter forgotPasswordPresenter);

    void inject(MyFirebaseInstanceIDServicePresenter myFirebaseInstanceIDServicePresenter);

    void inject(SplashScreenPresenter chekUpdateAvaliabePlaystorePresenter);

    void inject(BankPresenter bankPresenter);

    void inject (RegisterOptionPresenter registerOptionPresenter);

    void inject(MenuMainPresenter menuMainPresenter);

    void inject(ProfileACPresenter profileACPresenter);

    void inject(InformasiBankActivity informasiBankActivity);

    void inject(StatusNupPresenter statusNupPresenter);

    void inject(ListNupDetailPresenter listNupDetailPresenter);

    void injet(LeadListDetailPresenter leadListDetailPresenter);

    void inject(DownlinePresenter downlinePresenter);

    void inject(LeadPresenter leadPresenter);

    void inject(ResponseHistoryPresenter responseHistoryPresenter);

    void inject(CallHistoryPresenter callHistoryPresenter);

    void inject(PersonalLeadHistoryPresenter personalLeadHistoryPresenter);

    void inject(NoteExpandPresenter noteExpandPresenter);

    void inject(ProfileImagePresenter profileImagePresenter);

    void inject(LaunchPresenter launchPresenter);

    void inject(HubungiKamiPresenter hubungiKamiPresenter);

    void inject(LeadProjectPresenter leadProjectPresenter);
}