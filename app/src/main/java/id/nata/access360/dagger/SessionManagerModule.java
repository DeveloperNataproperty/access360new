package id.nata.access360.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.nata.access360.config.SessionManager;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:04.
 * Project : Napro
 */
@Module
public class SessionManagerModule {
    @Provides
    @Singleton
    SessionManager providesSessionManager(Context context){
        return new SessionManager(context);
    }
}
