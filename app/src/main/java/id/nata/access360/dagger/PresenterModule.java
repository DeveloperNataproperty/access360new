package id.nata.access360.dagger;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.nata.access360.fcm.process.MyFirebaseInstanceIDServicePresenter;
import id.nata.access360.view.intro.process.SplashScreenPresenter;
import id.nata.access360.view.login.process.ForgotPasswordPresenter;
import id.nata.access360.view.login.process.LoginPresenter;
import id.nata.access360.view.menuBank.Process.BankPresenter;
import id.nata.access360.view.register.process.RegisterFormPresenter;
import id.nata.access360.view.register.process.RegisterOptionPresenter;
import id.nata.access360.view.register.process.RegisterValidasiPresenter;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:04.
 * Project : Napro
 */
@Module
public class PresenterModule {

    @Provides
    @Singleton
    RegisterFormPresenter registerPresenter(Context context) {
        return new RegisterFormPresenter(context);
    }

    @Provides
    @Singleton
    LoginPresenter loginPresenter(Context context) {
        return new LoginPresenter(context);
    }

    @Provides
    @Singleton
    RegisterValidasiPresenter registerValidasiPresenter(Context context) {
        return new RegisterValidasiPresenter(context);
    }


    @Provides
    @Singleton
    ForgotPasswordPresenter forgotPasswordPresenter(Context context) {
        return new ForgotPasswordPresenter(context);
    }

    @Provides
    @Singleton
    MyFirebaseInstanceIDServicePresenter myFirebaseInstanceIDServicePresenter(Context context) {
        return new MyFirebaseInstanceIDServicePresenter(context);
    }


    @Provides
    @Singleton
    SplashScreenPresenter chekUpdateAvaliabePlaystorePresenter(Context context){
        return  new SplashScreenPresenter(context);
    }

    @Provides
    @Singleton
    RegisterOptionPresenter registerOptionPresenter(Context context){
        return  new RegisterOptionPresenter(context);
    }

    @Provides
    @Singleton
    BankPresenter bankSpinnerPresenter(Context context){
        return  new BankPresenter(context);
    }

}
