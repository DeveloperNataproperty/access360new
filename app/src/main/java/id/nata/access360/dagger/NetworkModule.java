package id.nata.access360.dagger;


import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.nata.access360.BuildConfig;
import id.nata.access360.network.NetworkService;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by arifcebe
 * on Aug 8/24/17 16:25.
 * Project : MerchantApps
 */
@Module
public class NetworkModule {
    @Provides
    @Singleton
    Retrofit provideCall() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

//        Strategy strategy = new AnnotationStrategy();
//        Serializer serializer = new Persister(strategy);
//
//        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .addInterceptor(logging)
//                .connectTimeout(2, TimeUnit.MINUTES)
//                .writeTimeout(2, TimeUnit.MINUTES)
//                .readTimeout(2, TimeUnit.MINUTES)
//                .build();
//
//        return new Retrofit.Builder()
//                .baseUrl(BuildConfig.BASEURLLOCAL)
//                .addConverterFactory(SimpleXmlConverterFactory.create(serializer))
//                .client(okHttpClient)
//                .build();

        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Customize the request
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .removeHeader("Pragma")
                                .header("Cache-Control", String.format("max-age=%d", BuildConfig.CACHETIME))
                                .build();

                        Response response = chain.proceed(request);
                        response.cacheResponse();
                        // Customize or return the response
                        return response;
                    }
                });

        if (BuildConfig.DEBUG) {
            // tampilin log response, hanya ketika masih debug
            okHttpClient.addInterceptor(logging);
        }
        okHttpClient.readTimeout(60, TimeUnit.SECONDS);
        okHttpClient.connectTimeout(60, TimeUnit.SECONDS);

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.BASEURL + BuildConfig.URL)
                .client(okHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    public NetworkService providesNetworkService(
            Retrofit retrofit) {
        return retrofit.create(NetworkService.class);
    }

}
