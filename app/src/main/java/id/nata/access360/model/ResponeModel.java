package id.nata.access360.model;

/**
 * Created by "dwist14"
 * on Sep 9/14/2017 09:33.
 * Project : Napro
 */
public class ResponeModel {
    int status;
    String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
