package id.nata.access360.model;

/**
 * Created by "dwist14"
 * on Sep 9/27/2017 14:36.
 * Project : Napro
 */
public class FavoriteParam {
    String propertyRef, userRef;

    public String getPropertyRef() {
        return propertyRef;
    }

    public void setPropertyRef(String propertyRef) {
        this.propertyRef = propertyRef;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }
}
