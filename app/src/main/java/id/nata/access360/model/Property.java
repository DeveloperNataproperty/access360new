package id.nata.access360.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:58.
 * Project : Napro
 */
public class Property extends RealmObject {
    @PrimaryKey
    String propertyRef;
    String propertyName;
    String propertyDesc;
    String urlImage;
    String deadLineDate;
    String deadLineMonth;
    String deadLineYear;
    String deadLineMinute;
    String deadLineSecond;
    String totalInvestor;
    String investmentPrice;
    String progressInvesment;
    String buyBackGuarantee;
    String provinceName;
    String cityName;
    String countWishlist;
    String deadLineMilliseconds;
    String listingType;
    String propertyStatus;
    String clusterName;
    String floor;
    String unit;

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDeadLineMilliseconds() {
        return deadLineMilliseconds;
    }

    public void setDeadLineMilliseconds(String deadLineMilliseconds) {
        this.deadLineMilliseconds = deadLineMilliseconds;
    }

    public String getListingType() {
        return listingType;
    }

    public void setListingType(String listingType) {
        this.listingType = listingType;
    }

    public String getPropertyStatus() {
        return propertyStatus;
    }

    public void setPropertyStatus(String propertyStatus) {
        this.propertyStatus = propertyStatus;
    }

    public String getCountWishlist() {
        return countWishlist;
    }

    public void setCountWishlist(String countWishlist) {
        this.countWishlist = countWishlist;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getBuyBackGuarantee() {
        return buyBackGuarantee;
    }

    public void setBuyBackGuarantee(String buyBackGuarantee) {
        this.buyBackGuarantee = buyBackGuarantee;
    }

    public String getPropertyRef() {
        return propertyRef;
    }

    public void setPropertyRef(String propertyRef) {
        this.propertyRef = propertyRef;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyDesc() {
        return propertyDesc;
    }

    public void setPropertyDesc(String propertyDesc) {
        this.propertyDesc = propertyDesc;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getDeadLineDate() {
        return deadLineDate;
    }

    public void setDeadLineDate(String deadLineDate) {
        this.deadLineDate = deadLineDate;
    }

    public String getDeadLineMonth() {
        return deadLineMonth;
    }

    public void setDeadLineMonth(String deadLineMonth) {
        this.deadLineMonth = deadLineMonth;
    }

    public String getDeadLineYear() {
        return deadLineYear;
    }

    public void setDeadLineYear(String deadLineYear) {
        this.deadLineYear = deadLineYear;
    }

    public String getDeadLineMinute() {
        return deadLineMinute;
    }

    public void setDeadLineMinute(String deadLineMinute) {
        this.deadLineMinute = deadLineMinute;
    }

    public String getDeadLineSecond() {
        return deadLineSecond;
    }

    public void setDeadLineSecond(String deadLineSecond) {
        this.deadLineSecond = deadLineSecond;
    }

    public String getTotalInvestor() {
        return totalInvestor;
    }

    public void setTotalInvestor(String totalInvestor) {
        this.totalInvestor = totalInvestor;
    }

    public String getInvestmentPrice() {
        return investmentPrice;
    }

    public void setInvestmentPrice(String investmentPrice) {
        this.investmentPrice = investmentPrice;
    }

    public String getProgressInvesment() {
        return progressInvesment;
    }

    public void setProgressInvesment(String progressInvesment) {
        this.progressInvesment = progressInvesment;
    }
}
