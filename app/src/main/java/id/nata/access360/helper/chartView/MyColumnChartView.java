package id.nata.access360.helper.chartView;

import android.content.Context;
import android.util.AttributeSet;

import id.nata.access360.helper.Utils;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.listener.DummyColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SelectedValue;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.provider.ColumnChartDataProvider;
import lecho.lib.hellocharts.renderer.ColumnChartRenderer;

/**
 * Created by "dwist14"
 * on Jan 1/23/2018 17:40.
 * Project : Napro
 */
public class MyColumnChartView extends MyAbstractChartView implements ColumnChartDataProvider {
    private static final String TAG = "ColumnChartView";
    private ColumnChartData data;
    private ColumnChartOnValueSelectListener onValueTouchListener;

    public MyColumnChartView(Context context) {
        this(context, (AttributeSet)null, 0);
    }

    public MyColumnChartView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyColumnChartView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.onValueTouchListener = new DummyColumnChartOnValueSelectListener();
        this.setChartRenderer(new ColumnChartRenderer(context, this, this));
        this.setColumnChartData(ColumnChartData.generateDummyData());
    }

    public ColumnChartData getColumnChartData() {
        return this.data;
    }

    public void setColumnChartData(ColumnChartData data) {
        if(null == data) {
            this.data = ColumnChartData.generateDummyData();
        } else {
            this.data = data;
        }

        super.onChartDataChange();
    }

    public ColumnChartData getChartData() {
        return this.data;
    }

    public void callTouchListener() {
        SelectedValue selectedValue = this.chartRenderer.getSelectedValue();
        if(selectedValue.isSet()) {
            SubcolumnValue value = (SubcolumnValue)((Column)this.data.getColumns().get(selectedValue.getFirstIndex())).getValues().get(selectedValue.getSecondIndex());
            Utils.showLog("TAG", "grafik "+selectedValue.getFirstIndex()+" "+selectedValue.getSecondIndex()+" "+value);
            this.onValueTouchListener.onValueSelected(selectedValue.getFirstIndex(), selectedValue.getSecondIndex(), value);
        } else {
            this.onValueTouchListener.onValueDeselected();
        }

    }

    public ColumnChartOnValueSelectListener getOnValueTouchListener() {
        return this.onValueTouchListener;
    }

    public void setOnValueTouchListener(ColumnChartOnValueSelectListener touchListener) {
        if(null != touchListener) {
            this.onValueTouchListener = touchListener;
        }

    }
}
