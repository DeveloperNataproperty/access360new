package id.nata.access360.helper;

import android.webkit.WebView;

/**
 * Created by "dwist14"
 * on Jan 1/17/2018 14:11.
 * Project : Napro
 */
public class WebViewClient extends android.webkit.WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        // This line right here is what you're missing.
        // Use the url provided in the method.  It will match the member URL!
        view.loadUrl(url);
        return true;
    }
}
