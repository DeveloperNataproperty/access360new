package id.nata.access360.helper;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import id.nata.access360.BuildConfig;
import id.nata.access360.R;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.ValueShape;

import static id.nata.access360.config.General.READ_PHONE_STATE_CONSTAT;


/**
 * Created by arifcebe
 * on May 5/10/17 16:16.
 * Project : LCommerceApp
 */

public class Utils {
    private LineChartData lineChartData;
    private int numberOfLines = 1;
    private int maxNumberOfLines = 4;
    private int monthLenght = 12;
    private float[][] randomNumbersTab = new float[maxNumberOfLines][monthLenght];
    private float[] arrValOfPoint;
    private String[] arrMonthLable;

    private boolean hasLines = true;
    private boolean hasPoints = true;
    private ValueShape shape = ValueShape.CIRCLE;
    private boolean isFilled = false;
    private boolean hasLabels = false;
    private boolean isCubic = false;
    private boolean hasLabelForSelected = false;

    /**
     * method untuk conversi dari model ke dalam json object
     *
     * @param loginParam parameter yang akan diconversi, dalam bentuk class model
     * @return String Json.
     */
    public static String modelToJson(Object loginParam) {
        Gson gson = new Gson();
        String jsonIn = gson.toJson(loginParam);
        Log.i("utils", "json in " + jsonIn);
        return jsonIn;
    }

    /**
     * conversi dari dp to pixel
     *
     * @param context Context App
     * @param dp      parameter dalam bentuk DP
     * @return Integer Pixel
     */
    public static int dpToPx(Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    /**
     * format harga
     *
     * @param currency
     * @return
     */
    public static String currencyFormat(String currency) {
        if (currency == null) {
            currency = "0";
        }
        double currPrice = Double.valueOf(currency);
        DecimalFormat format = new DecimalFormat("#,###");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        symbols.setGroupingSeparator(',');
        format.setDecimalFormatSymbols(symbols);

        return format.format(currPrice);
    }

    public static void showSnackbar(View view, String pesan) {
        Snackbar.make(view, pesan, Snackbar.LENGTH_LONG)
                .show();
    }

    public static void showLog(String tag, String log) {
        if (BuildConfig.DEBUG) {
            if (log == null) {
                log = "";
            }

            short maxLogSize = 1000;
            if (log.length() >= maxLogSize) {
                for (int i = 0; i <= log.length() / maxLogSize; ++i) {
                    int start = i * maxLogSize;
                    int end = (i + 1) * maxLogSize;
                    end = end > log.length() ? log.length() : end;
                    Log.i(tag, log.substring(start, end));
                }
            } else {
                Log.i(tag, log);
            }
        }

    }

    public static boolean checkReadPhoneState(Context c) {
        String permission = Manifest.permission.READ_PHONE_STATE;
        int res = c.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public static void showDialogPermmisionReadPhoneState(Activity activity) {
        ActivityCompat.requestPermissions(activity, new String[]{
                Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_CONSTAT);
    }

    /**
     * Untuk set image sesuai rasio
     *
     * @param display ambil diplay activity
     * @param ratio   dalam bentuk double, hasil dari lebar / tinggi
     * @param view    image yang mau diset
     */
    public static void setRatioViewLayout(Display display, Double ratio, View view) {
        Point size = new Point();
        display.getSize(size);
        Integer width = size.x;
        Double result = width / ratio;
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = width;
        params.height = result.intValue();
        view.setLayoutParams(params);
        view.requestLayout();

    }

    public static String formatRp(String saldo) {

        return "Rp. " + saldo;
    }


    /*method search bar*/
    public static boolean isRtl(Resources resources) {
        return resources.getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
    }

    /*method search bar*/
    public static int getThemeColor(Context context, int id) {
        Resources.Theme theme = context.getTheme();
        TypedArray a = theme.obtainStyledAttributes(new int[]{id});
        int result = a.getColor(0, 0);
        a.recycle();
        return result;
    }

    /*hideSoftKeyboard*/
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(
                Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public static void showToast(Context context, String text) {
        final Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 1000);
    }

    public static void delayFinish(final Activity activity) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.finish();
            }
        }, 3000);
    }

    public static String stringFormatColon(Activity activity, String str) {
        Resources res = activity.getResources();
        String stringColon = String.format(res.getString(R.string.colon), str);
        return stringColon;
    }

    public static void shareImage(String url, final Context context) {
        Picasso.with(context).load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_STREAM, Utils.getLocalBitmapUri(bitmap, context));
                context.startActivity(Intent.createChooser(i, "Share Image"));
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    public static  Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static String toCSV(ArrayList<String> array) {
        String result = "";
        if (array.size() > 0) {
            StringBuilder sb = new StringBuilder();
            for (String s : array) {
                sb.append(s).append("\n");
            }
            result = sb.deleteCharAt(sb.length() - 1).toString();
        }
        return result;
    }

    public static String convertFromDoubleToString(double pDblValue) {
        DecimalFormat decimalFormat = new DecimalFormat("#");
        //return String.valueOf(pDblValue).replace(".0", "");
        return decimalFormat.format(pDblValue);
    }

    /* Untuk animasi text */
    public static void counterAnimate(int finalValue, final TextView textview) {
        int initialValue = 0;
        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(4000);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                textview.setText(NumberFormat.getInstance(Locale.US).format(valueAnimator.getAnimatedValue()));
            }
        });
        valueAnimator.start();
    }

}
