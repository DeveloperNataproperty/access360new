package id.nata.access360.helper;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arifcebe
 * on May 5/8/17 14:22.
 * Project : LCommerceApp
 */

public class PagerAdapterHelper extends FragmentPagerAdapter {

    private List<Fragment> fragmentList;
    private List<String> titleList;


    public PagerAdapterHelper(FragmentManager fm) {
        super(fm);
        fragmentList = new ArrayList<>();
        titleList = new ArrayList<>();
    }

    public void addContent(Fragment frg, String title){
        fragmentList.add(frg);
        titleList.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }
}
