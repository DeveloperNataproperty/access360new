package id.nata.access360.helper;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;

/**
 * Created by "dwist14"
 * on Sep 9/12/2017 13:53.
 * Project : Napro
 */
public class RecyclerviewListBaseActivity extends BaseActivity {
    @BindView(R.id.progress)
    protected ProgressBar progressBar;
    @BindView(R.id.recyclerView)
    protected RecyclerView rvListData;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_filter, menu);
        return false;
    }

    /**
     * method untuk menampilkan input method dan view yang digunakan untuk handle layout
     *
     * @param menuName nama menu yang akan ditampilkan
     */
    protected void renderView(String menuName) {
        setContentView(R.layout.recyclevie_list_base_activity);
        ButterKnife.bind(this);

        // set toolbar
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(menuName);

        // hide keyboard ketika pertama kali buka page
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;
    }
}
