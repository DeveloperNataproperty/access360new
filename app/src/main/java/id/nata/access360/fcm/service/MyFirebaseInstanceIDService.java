package id.nata.access360.fcm.service;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;

/**
 * Created by "dwist14"
 * on Dec 12/28/2017 09:42.
 * Project : Napro
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();
    SessionManager mSessionManager;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        mSessionManager = new SessionManager(this);

        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        Utils.showLog(TAG, "FCM_TOKEN " + refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(General.REGISTRATION_COMPLETE);
        registrationComplete.putExtra(General.FCM_TOKEN, refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

    }

    private void storeRegIdInPref(String token) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(General.SHARED_PREF, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(General.FCM_TOKEN, token);
        editor.commit();
    }

}
