package id.nata.access360.fcm.process;


import id.nata.access360.base.BaseInterface;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface MyFirebaseInstanceIDServiceViewInterface extends BaseInterface {

    void onSuccess(String paramStrSuccessrMessage);

    void onFailed(String paramStrErrorMessage);
}
