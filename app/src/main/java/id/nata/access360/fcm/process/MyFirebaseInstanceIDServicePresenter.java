package id.nata.access360.fcm.process;


import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.fcm.model.FCMTokenParam;
import id.nata.access360.helper.Utils;
import id.nata.access360.model.ResponeModel;
import id.nata.access360.network.NetworkService;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.nata.access360.config.General.USER_REF;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public class MyFirebaseInstanceIDServicePresenter implements BasePresenter<MyFirebaseInstanceIDServiceViewInterface> {
    @Inject
    NetworkService networkService;

    private MyFirebaseInstanceIDServiceViewInterface view;
    private Context mContext;
    private Realm mRealm;
    private SessionManager mSessionManager;

    public MyFirebaseInstanceIDServicePresenter(Context mContext) {
        super();
        ((BaseApplication) mContext.getApplicationContext()).getDeps().inject(this);
        this.mContext = mContext;
        mRealm = Realm.getDefaultInstance();
        mSessionManager = SessionManager.getInstance(mContext);
    }

    public void PostFCMTokenSVC(String fcmToken) {
        FCMTokenParam param = new FCMTokenParam();
        param.setToken(fcmToken);
        param.setUserRef(mSessionManager.getStringFromSP(USER_REF));
        param.setIsMobile(General.IS_MOBILE);

        Call<ResponeModel> callLogin = networkService.PostFCMTokenSVC(Utils.modelToJson(param));
        callLogin.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, Response<ResponeModel> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {
                        view.onSuccess(response.body().getMessage());
                    } else {
                        view.onFailed(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(MyFirebaseInstanceIDServiceViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
