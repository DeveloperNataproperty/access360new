package id.nata.access360.fcm.model;

/**
 * Created by "dwist14"
 * on Dec 12/28/2017 11:42.
 * Project : Napro
 */
public class FCMTokenParam {
    String psRef;
    String token;
    String userRef;
    String isMobile;
    String sourceType;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }
}
