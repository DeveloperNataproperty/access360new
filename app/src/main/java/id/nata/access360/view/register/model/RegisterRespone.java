package id.nata.access360.view.register.model;

/**
 * Created by "dwist14"
 * on Sep 9/25/2017 16:05.
 * Project : Napro
 */
public class RegisterRespone {
    int status;
    String message;
    RegisterData dataLogin;

    public RegisterData getData() {
        return dataLogin;
    }

    public void setData(RegisterData dataLogin) {
        this.dataLogin = dataLogin;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
