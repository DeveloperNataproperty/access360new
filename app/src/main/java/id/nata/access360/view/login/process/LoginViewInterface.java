package id.nata.access360.view.login.process;


import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.login.model.LoginData;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface LoginViewInterface extends BaseInterface {

    void onSuccessAccountNotActive(LoginData data);

    void onSuccess(LoginData data);

    void onFailed(String paramStrErrorMessage);
}
