package id.nata.access360.view.note.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.note.process.NoteExpandInterface;
import id.nata.access360.view.note.process.NoteExpandPresenter;

public class NoteExpandActivity extends BaseActivity implements NoteExpandInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.note_txt)
    TextView noteText;

    @Inject
    NoteExpandPresenter noteExpandPresenter;

    private SessionManager sessionManager;
    private String psRef, leadRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_expand);
        ButterKnife.bind(this);

        noteExpandPresenter = new NoteExpandPresenter(this);
        noteExpandPresenter.setupView(this);

        sessionManager = new SessionManager(this);
        psRef = sessionManager.getStringFromSP(General.PS_REF);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Note");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                leadRef = getIntent().getStringExtra("leadRef");
            }
        }

        getNoteExpand();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NoteExpandActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                break;
        }
        return true;
    }

    private void getNoteExpand() {
        noteExpandPresenter.getLeadNoteSVC(psRef, leadRef);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessGetNote(String noted, String message) {
        noteText.setText(noted);
    }

    @Override
    public void onFailedGetNote(String message) {
        Utils.showSnackbar(layout, message);
    }
}
