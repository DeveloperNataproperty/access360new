package id.nata.access360.view.register.model;

/**
 * Created by "dwist14"
 * on Sep 9/25/2017 15:42.
 * Project : Napro
 */
public class RegisterValidasiParam {
    String psRef, username, verificationCode;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
}
