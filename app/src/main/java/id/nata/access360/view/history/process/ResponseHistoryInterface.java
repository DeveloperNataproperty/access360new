package id.nata.access360.view.history.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.history.model.ResponseHistoryData;

public interface ResponseHistoryInterface extends BaseInterface {
    void onSuccessGetResponseHistory(List<ResponseHistoryData> responseHistoryList);

    void onFailedGetResponseHistory(String string);
}
