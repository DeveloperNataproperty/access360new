package id.nata.access360.view.login.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.helper.Utils;
import id.nata.access360.helper.UtilsSnackbar;
import id.nata.access360.view.intro.SplashScreenActivity;
import id.nata.access360.view.login.process.ForgotPasswordPresenter;
import id.nata.access360.view.login.process.ForgotPasswordViewInterface;

/**
 * Created by "dwist14"
 * on Nov 11/10/2017 14:23.
 * Project : Napro
 */
public class ForgotPasswordActivity extends BaseActivity implements ForgotPasswordViewInterface {
    @Inject
    ForgotPasswordPresenter forgotPasswordPresenter;

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_email_layout)
    TextInputLayout edtEmailLayout;
    @BindView(R.id.layout)
    LinearLayout layout;

    private String username;
    private AlertDialog.Builder alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_pactivity);
        ButterKnife.bind(this);
        forgotPasswordPresenter = new ForgotPasswordPresenter(this);
        forgotPasswordPresenter.setupView(this);
        initToolbar(getString(R.string.forgot_password));

        if (getIntent()!=null){
            if (getIntent().getExtras()!=null){
                username = getIntent().getStringExtra("email");
            }
        }
    }

    //click button submit
    @OnClick(R.id.btn_submit)
    public void setBtnSubmit() {
        Utils.hideSoftKeyboard(this);
        username = edtEmail.getText().toString().trim();

        if (validate()) {
            forgotPasswordPresenter.PostForgotPasswordSVC(username);
        }
    }

    private boolean validate() {
        boolean valid = true;
        if (username.equals("")) {
            edtEmailLayout.setError(getString(R.string.must_be_field));
            valid = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
            edtEmailLayout.setError(getString(R.string.email_not_valid));
            valid = false;
        } else {
            edtEmailLayout.setError(null);
        }
        return valid;
    }


    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        UtilsSnackbar.showSnakbarTypeThree(layout, this);
    }

    @Override
    public void onSuccess(String paramStrSuccessMessage) {
//        UtilsSnackbar.showSnakbarTypeOne(layout, paramStrSuccessMessage);
//        Utils.delayFinish(this);
        alertDialog = new AlertDialog.Builder(ForgotPasswordActivity.this);
        alertDialog.setMessage(paramStrSuccessMessage);
        alertDialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                Intent intent = new Intent(ForgotPasswordActivity.this, SplashScreenActivity.class);
                startActivity(intent);
            }
        });
        alertDialog.show();
    }

    @Override
    public void onFailed(String paramStrErrorMessage) {
        Utils.showSnackbar(layout, paramStrErrorMessage);
    }
}
