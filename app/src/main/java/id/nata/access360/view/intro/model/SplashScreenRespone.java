package id.nata.access360.view.intro.model;

/**
 * Created by "dwist14"
 * on Feb 2/6/2018 20:51.
 * Project : Napro
 */
public class SplashScreenRespone {
    int status;
    String versionCode;
    String versionName;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }
}
