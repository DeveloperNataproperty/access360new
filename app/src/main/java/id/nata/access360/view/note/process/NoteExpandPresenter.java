package id.nata.access360.view.note.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.note.model.NoteExpandParam;
import id.nata.access360.view.note.model.NoteExpandResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoteExpandPresenter implements BasePresenter<NoteExpandInterface> {

    @Inject
    NetworkService networkService;

    private NoteExpandInterface view;
    private Context context;

    public NoteExpandPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void getLeadNoteSVC(String psRef, String leadRef){
        view.showLoader();

        NoteExpandParam param = new NoteExpandParam();
        param.setPsRef(psRef);
        param.setLeadRef(leadRef);

        Call<NoteExpandResponse> callNoteResponseSVC = networkService.GET_NoteExpandResponse(Utils.modelToJson(param));
        callNoteResponseSVC.enqueue(new Callback<NoteExpandResponse>() {
            @Override
            public void onResponse(Call<NoteExpandResponse> call, Response<NoteExpandResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.onSuccessGetNote(response.body().getNoted() ,response.body().getMessage());
                    } else if (response.body().getStatus()== General.API_REQUEST_FAILED){
                        view.onFailedGetNote(response.body().getMessage());
                    }
                }else{
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<NoteExpandResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(NoteExpandInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
