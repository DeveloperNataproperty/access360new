package id.nata.access360.view.history.model;

public class PersonalLeadHistoryData {
    private String leadRef;
    private String leadName;
    private String leadEmail;
    private String leadPhone;
    private String leadCity;
    private String responseRef;
    private String responseGroupRef;
    private String responseName;
    private String noted;

    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public String getLeadEmail() {
        return leadEmail;
    }

    public void setLeadEmail(String leadEmail) {
        this.leadEmail = leadEmail;
    }

    public String getLeadPhone() {
        return leadPhone;
    }

    public void setLeadPhone(String leadPhone) {
        this.leadPhone = leadPhone;
    }

    public String getLeadCity() {
        return leadCity;
    }

    public void setLeadCity(String leadCity) {
        this.leadCity = leadCity;
    }

    public String getResponseRef() {
        return responseRef;
    }

    public void setResponseRef(String responseRef) {
        this.responseRef = responseRef;
    }

    public String getResponseGroupRef() {
        return responseGroupRef;
    }

    public void setResponseGroupRef(String responseGroupRef) {
        this.responseGroupRef = responseGroupRef;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public String getNoted() {
        return noted;
    }

    public void setNoted(String noted) {
        this.noted = noted;
    }
}
