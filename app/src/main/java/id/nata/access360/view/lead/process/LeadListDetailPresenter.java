package id.nata.access360.view.lead.process;

import android.content.Context;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.lead.model.ListResponse;
import id.nata.access360.view.lead.model.ListResponseParam;
import id.nata.access360.view.lead.model.ListResponseGroup;
import id.nata.access360.view.lead.model.UpdateCallParam;
import id.nata.access360.view.lead.model.UpdateLeadResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadListDetailPresenter implements BasePresenter<LeadListDetailInterface> {

    @Inject
    NetworkService networkService;

    private LeadListDetailInterface view;
    private Context context;

    public LeadListDetailPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().injet(this);
        this.context = context;
    }

    public void getResponseGroup(String jsonIn){
        view.showLoader();

        Call<List<ListResponseGroup>> callResponseGroup = networkService.GET_ResponseGroup(Utils.modelToJson(jsonIn));
        callResponseGroup.enqueue(new Callback<List<ListResponseGroup>>() {
            @Override
            public void onResponse(Call<List<ListResponseGroup>> call, Response<List<ListResponseGroup>> response) {
//                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetResponseGroup(response.body());
                    } else {
                        view.onFailedGetResponseGroup(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<ListResponseGroup>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    public void getListResponse(String psRef, String username, String responseGroupRef){
//        view.showLoader();

        ListResponseParam param = new ListResponseParam();
        param.setPsRef(psRef);
        param.setUsername(username);
        param.setResponseGroupRef(responseGroupRef);

        Call<List<ListResponse>> callListResponse = networkService.GET_ListResponse(Utils.modelToJson(param));
        callListResponse.enqueue(new Callback<List<ListResponse>>() {
            @Override
            public void onResponse(Call<List<ListResponse>> call, Response<List<ListResponse>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetListResponse(response.body());
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<ListResponse>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    public void postUpdateLeadResponseLanjut(String psRef, String username, String leadRef, String responseGroupRef, String responseRef, String isFinish, String noted){
        view.showLoader();

        PostUpdateLeadParam param = new PostUpdateLeadParam();
        param.setPsRef(psRef);
        param.setUsername(username);
        param.setLeadRef(leadRef);
        param.setResponseGroupRef(responseGroupRef);
        param.setResponseRef(responseRef);
        param.setIsFinish(isFinish);
        param.setNoted(noted);

        Call<UpdateLeadResponse> callUpdateLeadResponse = networkService.POST_UpdateLead(Utils.modelToJson(param));
        callUpdateLeadResponse.enqueue(new Callback<UpdateLeadResponse>() {
            @Override
            public void onResponse(Call<UpdateLeadResponse> call, Response<UpdateLeadResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccessUpdateLeadLanjut(response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED){
                        view.onFailedUpdateLeadLanjut(response.body().getMessage());
                    } else if (response.body().getStatus() == 202){
                        view.onFailedUpdateLeadLanjutPhone(response.body().getMessage());
                    } else {
                        view.onFailedUpdateLeadFromAndro(context.getString(R.string.update_failed));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateLeadResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    public void postUpdateLeadResponseSelesai(String psRef, String username, String leadRef, String responseGroupRef, String responseRef, String isFinish, String noted){
        view.showLoader();

        PostUpdateLeadParam param = new PostUpdateLeadParam();
        param.setPsRef(psRef);
        param.setUsername(username);
        param.setLeadRef(leadRef);
        param.setResponseGroupRef(responseGroupRef);
        param.setResponseRef(responseRef);
        param.setIsFinish(isFinish);
        param.setNoted(noted);

        Call<UpdateLeadResponse> callUpdateLeadResponse = networkService.POST_UpdateLead(Utils.modelToJson(param));
        callUpdateLeadResponse.enqueue(new Callback<UpdateLeadResponse>() {
            @Override
            public void onResponse(Call<UpdateLeadResponse> call, Response<UpdateLeadResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccessUpdateLeadSelesai(response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED){
                        view.onFailedUpdateLeadSelesai(response.body().getMessage());
                    } else if (response.body().getStatus() == 202){
                        view.onFailedUpdateLeadSelesaiPhone(response.body().getMessage());
                    } else {
                        view.onFailedUpdateLeadFromAndro(context.getString(R.string.update_failed));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateLeadResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    public void postUpdateCallLeadWa(String psRef, String leadRef, String phone, String typeCall){
        UpdateCallParam param = new UpdateCallParam();
        param.setPsRef(psRef);
        param.setLeadRef(leadRef);
        param.setPhone(phone);
        param.setCallType(typeCall);

        Call<UpdateLeadResponse> callUpdateCall = networkService.POST_UpdateCall(Utils.modelToJson(param));
        callUpdateCall.enqueue(new Callback<UpdateLeadResponse>() {
            @Override
            public void onResponse(Call<UpdateLeadResponse> call, Response<UpdateLeadResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccessUpdateCallLeadWa(response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED){
                        view.onFailedUpdateCallLeadWa(response.body().getMessage());
                    }  else {
                        view.onFailedUpdateLeadFromAndro(context.getString(R.string.update_failed));
                    }
                }  else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateLeadResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    public void postUpdateCallLeadPhone(String psRef, String leadRef, String phone, String typeCall){
        UpdateCallParam param = new UpdateCallParam();
        param.setPsRef(psRef);
        param.setLeadRef(leadRef);
        param.setPhone(phone);
        param.setCallType(typeCall);

        Call<UpdateLeadResponse> callUpdateCall = networkService.POST_UpdateCall(Utils.modelToJson(param));
        callUpdateCall.enqueue(new Callback<UpdateLeadResponse>() {
            @Override
            public void onResponse(Call<UpdateLeadResponse> call, Response<UpdateLeadResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccessUpdateCallLeadPhone(response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED){
                        view.onFailedUpdateCallLeadPhone(response.body().getMessage());
                    }  else {
                        view.onFailedUpdateLeadFromAndro(context.getString(R.string.update_failed));
                    }
                }  else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateLeadResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(LeadListDetailInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
