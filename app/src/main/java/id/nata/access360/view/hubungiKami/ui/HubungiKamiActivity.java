package id.nata.access360.view.hubungiKami.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.hubungiKami.process.HubungiKamiInterface;
import id.nata.access360.view.hubungiKami.process.HubungiKamiPresenter;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.menuProfile.model.DataPersonal;

public class HubungiKamiActivity extends BaseActivity implements HubungiKamiInterface {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.txt_nama)
    TextView namaTxt;
    @BindView(R.id.txt_hp)
    TextView hpTxt;
    @BindView(R.id.txt_email)
    TextView emailTxt;
    @BindView(R.id.txt_pesan)
    TextView pesanTxt;
    @BindView(R.id.nama_edt)
    EditText namaEdt;
    @BindView(R.id.hp_edt)
    EditText hpEdt;
    @BindView(R.id.email_edt)
    EditText emailEdt;
    @BindView(R.id.pesan_edt)
    EditText pesanEdt;
    @BindView(R.id.kirim_pesan_btn)
    Button kirimPesanBtn;

    @Inject
    HubungiKamiPresenter hubungiKamiPresenter;

    SessionManager sessionManager;
    private String psRef, username, nama, hp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hubungi_kami);
        ButterKnife.bind(this);
        hubungiKamiPresenter = new HubungiKamiPresenter(this);
        hubungiKamiPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());

        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Hubungi Kami");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initTextViewColor();

        getProfileInfo();
    }

    private void getProfileInfo() {
        hubungiKamiPresenter.GETProfileInfoSvc(psRef, username);
    }

    private void initTextViewColor() {
        String namaTextViewStr = "<font>Nama</font> <font color=#ff4242>*</font>";
        String hpTextViewStr = "<font>No. Hp</font> <font color=#ff4242>*</font>";
        String emailTextViewStr = "<font>Email</font> <font color=#ff4242>*</font>";
        String pesanTextViewStr = "<font>Pesan</font>";

        namaTxt.setText(Html.fromHtml(namaTextViewStr));
        hpTxt.setText(Html.fromHtml(hpTextViewStr));
        emailTxt.setText(Html.fromHtml(emailTextViewStr));
        pesanTxt.setText(Html.fromHtml(pesanTextViewStr));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(HubungiKamiActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccesProfileInfo(DataPersonal dataPersonal) {
        nama = dataPersonal.getPsName();
        hp = dataPersonal.getHandphone();

        namaEdt.setText(nama);
        hpEdt.setText(hp);
        emailEdt.setText(username);
    }

    @Override
    public void onFailed(String string) {
        Utils.showSnackbar(layout, string);
    }

    @OnClick(R.id.kirim_pesan_btn)
    public void btnKirimPesan(){
        hubungiKamiPresenter.POST_ContactUs(psRef, pesanEdt.getText().toString().trim());
    }

    @Override
    public void onSuccessPostMessage(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onFailedPostMessage(String message) {
        Utils.showSnackbar(layout, message);
    }
}
