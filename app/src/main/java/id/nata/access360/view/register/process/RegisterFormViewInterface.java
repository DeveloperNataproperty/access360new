package id.nata.access360.view.register.process;


import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.register.model.RegisterData;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface RegisterFormViewInterface extends BaseInterface {

    void onSuccesss(RegisterData registerData, String message);

    void onFailedFromService(String message);

    void onAlreadyExist(String message);

    void onFailed(String paramStrErrorMessage);
}
