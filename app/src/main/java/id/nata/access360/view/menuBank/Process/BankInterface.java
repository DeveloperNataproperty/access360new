package id.nata.access360.view.menuBank.Process;

import org.json.JSONArray;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.menuBank.Model.BankInformationData;
import id.nata.access360.view.menuBank.Model.BankListData;

/**
 * Created by NATA on 2/12/19.
 */

public interface BankInterface extends BaseInterface {
    void onSuccessBankInfo(BankInformationData bankInformationData, String message);
    void onFailedBankInfo(String message);
    void onSuccess(List<BankListData> bankListData);
    void onFailed(String string);

}
