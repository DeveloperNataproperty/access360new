package id.nata.access360.view.lead.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.fcm.service.MyFirebaseMessagingService;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.history.ui.PersonalLeadHistory;
import id.nata.access360.view.lead.adapter.LeadListAdapter;
import id.nata.access360.view.lead.model.LeadList;
import id.nata.access360.view.lead.process.LeadInterface;
import id.nata.access360.view.lead.process.LeadPresenter;

public class LeadActivity extends BaseActivity implements LeadInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_lead)
    ListView listLead;

    @Inject
    LeadPresenter leadPresenter;

    private SessionManager sessionManager;
    private String psRef, username, projectRef, projectName;

    private LeadListAdapter leadListAdapter;

    List<LeadList> leadLists = new ArrayList<LeadList>();

    private String leadRef, leadName, leadEmail, leadPhone, leadCity, statusColor, responseGroupRef, responseRef, noted, statusColorMessage, linkPk, linkPriceList, linkImage;

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead);
        ButterKnife.bind(this);
        leadPresenter = new LeadPresenter(this);
        leadPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                projectRef = getIntent().getStringExtra("projectRef");
                projectName = getIntent().getStringExtra("projectName");
            }
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(projectName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        getLeadList();

        listLead.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                leadRef = leadLists.get(i).getLeadRef();
                leadName = leadLists.get(i).getLeadName();
                leadEmail = leadLists.get(i).getLeadEmail();
                leadPhone = leadLists.get(i).getLeadPhone();
                leadCity = leadLists.get(i).getLeadCity();
                statusColor = leadLists.get(i).getStatusColor();
                responseGroupRef = leadLists.get(i).getResponseGroupRef();
                responseRef = leadLists.get(i).getResponseRef();
                noted = leadLists.get(i).getNoted();
                statusColorMessage = leadLists.get(i).getStatusColorMessage();
                linkPk = leadLists.get(i).getLinkPK();
                linkPriceList = leadLists.get(i).getLinkPriceList();
                linkImage = leadLists.get(i).getLinkImage();

                Intent intent = new Intent(LeadActivity.this, LeadDetailActivity.class);
                intent.putExtra("leadRef", leadRef);
                intent.putExtra("leadName", leadName);
                intent.putExtra("leadEmail", leadEmail);
                intent.putExtra("leadPhone", leadPhone);
                intent.putExtra("leadCity", leadCity);
                intent.putExtra("statusColor", statusColor);
                intent.putExtra("responseGroupRef", responseGroupRef);
                intent.putExtra("responseRef", responseRef);
                intent.putExtra("noted", noted);
                intent.putExtra("statusColorMessage", statusColorMessage);
                intent.putExtra("projectName", projectName);
                intent.putExtra("linkPk", linkPk);
                intent.putExtra("linkPriceList", linkPriceList);
                intent.putExtra("linkImage", linkImage);
                startActivity(intent);


            }
        });

    }

    private void getLeadList() {
        leadPresenter.getLeadListSVC(psRef, username, projectRef);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessGetLeadList(List<LeadList> leadList) {
        leadListAdapter = new LeadListAdapter(this, leadList);
        listLead.setAdapter(leadListAdapter);
        leadLists = leadList;
    }

    @Override
    public void onFailedGetLeadList(String string) {
        Utils.showSnackbar(layout, string);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.history:
                Intent i = new Intent(LeadActivity.this, PersonalLeadHistory.class);
                i.putExtra("projectRef", projectRef);
                i.putExtra("projectName", projectName);
                startActivity(i);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLeadList();
    }
}
