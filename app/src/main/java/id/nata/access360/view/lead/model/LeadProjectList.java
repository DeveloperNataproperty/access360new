package id.nata.access360.view.lead.model;

public class LeadProjectList {
    private String projectRef;
    private String projectName;
    private String totalLead;

    public String getProjectRef() {
        return projectRef;
    }

    public void setProjectRef(String projectRef) {
        this.projectRef = projectRef;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTotalLead() {
        return totalLead;
    }

    public void setTotalLead(String totalLead) {
        this.totalLead = totalLead;
    }
}
