package id.nata.access360.view.lead.model;

public class ListResponseParam {
    private String psRef;
    private String username;
    private String responseGroupRef;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getResponseGroupRef() {
        return responseGroupRef;
    }

    public void setResponseGroupRef(String responseGroupRef) {
        this.responseGroupRef = responseGroupRef;
    }
}
