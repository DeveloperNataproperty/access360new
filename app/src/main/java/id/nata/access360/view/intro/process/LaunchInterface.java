package id.nata.access360.view.intro.process;

import id.nata.access360.base.BaseInterface;

public interface LaunchInterface extends BaseInterface {
    void onSuccessGetVersionCode(String versionCode, String versionName);

    void onFailedGetVersionCOde(String message);
}
