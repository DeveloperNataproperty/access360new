package id.nata.access360.view.register.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.PrefUtils;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.login.model.LoginData;
import id.nata.access360.view.login.ui.LoginActivity;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.register.model.UserInfoFacebookModel;
import id.nata.access360.view.register.process.RegisterOptionPresenter;
import id.nata.access360.view.register.process.RegisterOptionViewInterface;

import static id.nata.access360.config.General.EMAIL;
import static id.nata.access360.config.General.FULLNAME;

/**
 * Created by "dwist14"
 * on Sep 9/18/2017 20:49.
 * Project : Napro
 */
public class RegisterOptionActivity extends BaseActivity implements GoogleApiClient.OnConnectionFailedListener,
        RegisterOptionViewInterface {
    @Inject
    RegisterOptionPresenter registerOptionPresenter;

    GoogleApiClient mGoogleApiClient;
    private static final String TAG = RegisterOptionActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;
    UserInfoFacebookModel user;
    private CallbackManager callbackManager;

    public FirebaseAuth firebaseAuth;

    String strPsRef, strFullname, strEmail, strPsCode, strIdFacebook, strIdGoogle;

    private SessionManager mSessionManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_option_activity);
        ButterKnife.bind(this);

        registerOptionPresenter = new RegisterOptionPresenter(this);
        registerOptionPresenter.setupView(this);

        //google
//        firebaseAuth = FirebaseAuth.getInstance();

        mSessionManager = new SessionManager(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //With Facebook
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, mCallBack);
    }

    //textview di klik akan buka loginActivity
    @OnClick(R.id.txt_login)
    public void btnLoginOnClicked() {
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.layout_email)
    public void signUpEmail() {
        Intent i = new Intent(this, RegisterFormActivity.class);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.layout_google)
    public void signUpGoogleOnClick() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick(R.id.layout_facebook)
    public void signInFacebookOnClick() {
        Utils.showLog("login", "fb button on click");
        if (AccessToken.getCurrentAccessToken() != null) {
            //  LoginManager.getInstance().logOut();
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
        } else {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //facebook
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        Utils.showLog(TAG, "result requsetCode " + requestCode + " resultCode " + resultCode);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//        }
        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (googleSignInResult.isSuccess()) {

                GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();

//                FirebaseUserAuth(googleSignInAccount);
            }

        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void handleSignInResult(GoogleSignInResult result) {
        Utils.showLog(TAG, "handleSignInResult " + result.isSuccess());
        Utils.showLog(TAG, "handleSignInResult " + result.getStatus().getStatusCode());
        Utils.showLog(TAG, "handleSignInResult " + result.getStatus().getStatusMessage());

        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String personName = acct.getDisplayName();
            String email = acct.getEmail();
            String googleId = acct.getId();
            Log.e(TAG, "Name: " + personName + ", email: " + email + ", " + googleId);
            googleRespone(true, email, personName, googleId);
        } else {
            // Signed out, show unauthenticated UI.
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            googleRespone(false, "", "", "");
                        }
                    });
        }
    }

    public void FirebaseUserAuth(GoogleSignInAccount googleSignInAccount) {

//        AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
//
////        Toast.makeText(RegisterOptionActivity.this,""+ authCredential.getProvider(),Toast.LENGTH_LONG).show();
//
//        firebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(RegisterOptionActivity.this,
//                new OnCompleteListener() {
//                    @Override
//                    public void onComplete(@NonNull Task AuthResultTask) {
//
//                        if (AuthResultTask.isSuccessful()) {
//
//                            // Getting Current Login user details.
//                            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
//
//                            String personName = firebaseUser.getDisplayName();
//                            String email = firebaseUser.getEmail();
//                            String googleId = firebaseUser.getUid();
//
//                            Log.e(TAG, "Name: " + personName + ", email: " + email + ", " + googleId);
//                            googleRespone(true, email, personName, googleId);
//
//                        } else {
//                            Toast.makeText(RegisterOptionActivity.this, getString(R.string.something_error), Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
    }

    private void googleRespone(boolean isSignedIn, String email, String name, String idGoogle) {
        if (isSignedIn) {
            strEmail = email;
            strIdGoogle = idGoogle;
            strFullname = name;

            registerOptionPresenter.PostLoginGoogleSVC(email, idGoogle);

        } else {

        }
    }

    //FB
    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("callbackFB", "success");
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Log.e("response: ", response + "");
                            String emailId = null;
                            try {
                                user = new UserInfoFacebookModel();
                                user.facebookID = object.getString("id").toString();
                                user.name = object.getString("name").toString();
                                user.gender = object.getString("gender").toString();
                                user.urlPhotoProfile = "http://graph.facebook.com/" + user.facebookID + "/picture?type=large";
                                user.email = object.getString("email").toString();
                                PrefUtils.setCurrentUser(user, RegisterOptionActivity.this);

                                Log.d("callbackFB", object.getString("name").toString() + " " + object.getString("email").toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("callbackFB", e.toString());
                            }

                            ByFacebook();

                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            Log.d("callbackFB", "cancel");
        }

        @Override
        public void onError(FacebookException e) {
            Log.d("callbackFB", e.getMessage().toString());
        }
    };

    private void ByFacebook() {
        if (PrefUtils.getCurrentUser(this) != null) {
            facebookRespone(true, user.email, user.name, user.facebookID);
        }
    }

    private void facebookRespone(boolean isSignedFacebook, String email, String name, String idFacebook) {
        if (isSignedFacebook) {
            if (email == null || email.equals("")) {
                LoginManager.getInstance().logInWithReadPermissions(
                        this,
                        Arrays.asList("email"));
            } else {
                strEmail = email;
                strFullname = name;
                strIdFacebook = idFacebook;

                registerOptionPresenter.PostLoginUserSVC(email, "", idFacebook);
            }

        } else {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mGoogleApiClient.clearDefaultAccountAndReconnect();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Toast.makeText(getApplicationContext(), appErrorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailed(String paramStrErrorMessage) {
        Toast.makeText(getApplicationContext(), paramStrErrorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessAccountNotActive(LoginData data) {
//        mSessionManager.setStringToSP(General.USER_REF, data.getUserRef());
        mSessionManager.setStringToSP(General.PS_REF, data.getPsRef());
        mSessionManager.setNeedValidasi(true);
        Intent i = new Intent(getApplicationContext(), RegisterValidasiActivity.class);
        startActivity(i);
    }

    @Override
    public void onSuccessAccountNotRegister(LoginData data) {
        Intent i = new Intent(this, RegisterFormActivity.class);
        i.putExtra(FULLNAME, strFullname);
        i.putExtra(EMAIL, strEmail);
        i.putExtra(General.ID_FACEBOOK, strIdFacebook);
        i.putExtra(General.ID_GOOGLE, strIdGoogle);
        startActivity(i);
    }

    @Override
    public void onSuccessLoginUser(LoginData data) {
        strPsRef = data.getPsRef();
        strEmail = data.getUsername();
        strFullname = data.getPsName();
        strPsCode = data.getPsCode();

        mSessionManager.createLoginSession(strPsRef, strEmail, strFullname, strPsCode);

        Intent i = new Intent(getApplicationContext(), ProjectMenu2Activity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }
}
