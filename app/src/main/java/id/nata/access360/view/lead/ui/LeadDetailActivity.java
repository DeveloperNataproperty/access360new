package id.nata.access360.view.lead.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.MyListView;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.history.ui.CallHistoryActivity;
import id.nata.access360.view.history.ui.ResponseHistoryActivity;
import id.nata.access360.view.lead.adapter.ListResponseAdapter;
import id.nata.access360.view.lead.adapter.ResponseGroupAdapter;
import id.nata.access360.view.lead.model.ListResponse;
import id.nata.access360.view.lead.model.ListResponseGroup;
import id.nata.access360.view.lead.process.LeadListDetailInterface;
import id.nata.access360.view.lead.process.LeadListDetailPresenter;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.note.ui.NoteExpandActivity;

public class LeadDetailActivity extends BaseActivity implements LeadListDetailInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img_status)
    ImageView statusImage;
    @BindView(R.id.txt_name)
    TextView namaText;
    @BindView(R.id.txt_email)
    TextView emailText;
    @BindView(R.id.txt_no_telp)
    TextView noTelpText;
    @BindView(R.id.txt_kota)
    TextView kotaText;
    @BindView(R.id.txt_note)
    TextView notedText;
    @BindView(R.id.txt_note_click)
    TextView noteTextClick;
    @BindView(R.id.spinner_status)
    Spinner statusSpinner;
    @BindView(R.id.btn_update_lanjut_fu)
    Button UpdateDanLanjutBtn;
    @BindView(R.id.btn_update_selesai_fu)
    Button UpdateDanSelesaiBtn;
    @BindView(R.id.list_respone)
    MyListView responseList;
    @BindView(R.id.btn_wa)
    ImageButton btnWa;
    @BindView(R.id.btn_wa2)
    ImageButton btnWa2;
    @BindView(R.id.btn_wa3)
    ImageButton btnWa3;
    @BindView(R.id.btn_wa4)
    ImageButton btnWa4;
    @BindView(R.id.btn_call)
    ImageButton btnCall;
    @BindView(R.id.edt_note)
    EditText edtNote;

    @Inject
    LeadListDetailPresenter leadListDetailPresenter;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private ResponseGroupAdapter responseGroupAdapter;
    private ListResponseAdapter listResponseAdapter;
    private List<ListResponseGroup> listResponseGroups = new ArrayList<ListResponseGroup>();
    private List<ListResponse> listResponses = new ArrayList<ListResponse>();

    private String leadRef, leadName, leadEmail, leadPhone, leadCity, statusColor, statusColorMessage, linkPk, linkPriceList, linkImage;
    private String psRef, username, projectName, responseGroupName;
    private String responseGroupRefIntent = "", responseGroupRefSpinner = "", responseGroupRef = "", responseRefGone = "";
    private String responseRef, noted, typeCall="";
    private String isFinish;

    private Integer clicked = 0, flagUpdate = 0, flagResponeRef = 0;
    private Boolean firstLoad = true;

    private AlertDialog.Builder alertDialog;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_detail);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);

        leadListDetailPresenter = new LeadListDetailPresenter(this);
        leadListDetailPresenter.setupView(this);

        sessionManager = new SessionManager(this);
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                leadRef = getIntent().getStringExtra("leadRef");
                leadName = getIntent().getStringExtra("leadName");
                leadEmail = getIntent().getStringExtra("leadEmail");
                leadPhone = getIntent().getStringExtra("leadPhone");
                leadCity = getIntent().getStringExtra("leadCity");
                statusColor = getIntent().getStringExtra("statusColor");
                responseGroupRefIntent = getIntent().getStringExtra("responseGroupRef");
                responseRef = getIntent().getStringExtra("responseRef");
                noted = getIntent().getStringExtra("noted");
                statusColorMessage = getIntent().getStringExtra("statusColorMessage");
                projectName = getIntent().getStringExtra("projectName");

                linkPk = getIntent().getStringExtra("linkPk");
                linkPriceList = getIntent().getStringExtra("linkPriceList");
                linkImage = getIntent().getStringExtra("linkImage");

                namaText.setText(leadName);
                emailText.setText(leadEmail);
                noTelpText.setText(leadPhone);
                kotaText.setText(leadCity);

                //view more Log
                if (noted.length()<14){
                    notedText.setText(noted);
                } else {
                    String notedExpand = noted.substring(0, 15);
                    notedText.setText(notedExpand);
                }
//                Drawable background = statusImage.getBackground();
//                ((GradientDrawable) background).setColor(Color.parseColor(statusColor));
                if (statusColor.equals("#ffffff")){
                    statusImage.setVisibility(View.INVISIBLE);
                } else {
                    statusImage.setColorFilter(Color.parseColor(statusColor));
                }

            }
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(projectName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getGroupResponse();

        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                responseGroupRefSpinner = listResponseGroups.get(i).getResponseGroupRef();
                responseGroupName = listResponseGroups.get(i).getResponseGroupName();

                if (!firstLoad) {
                    clicked = 1;
                } else {
                    firstLoad = false;
                }
                if (flagUpdate == 1) {
                    clicked = 2;
                    flagUpdate = 2;
                }
                leadListDetailPresenter.getListResponse(psRef, username, responseGroupRefSpinner);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        responseList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                listResponseAdapter.setSelectedItem();
                flagResponeRef = 2;
                responseRef = listResponses.get(i).getResponseRef();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_lead_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(LeadDetailActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                break;
            case R.id.response_history:
                Intent intentResponse = new Intent(LeadDetailActivity.this, ResponseHistoryActivity.class);
                intentResponse.putExtra("leadRef", leadRef);
                intentResponse.putExtra("projectName", projectName);
                startActivity(intentResponse);
                break;
            case R.id.call_history:
                Intent intentCall = new Intent(LeadDetailActivity.this, CallHistoryActivity.class);
                intentCall.putExtra("leadRef", leadRef);
                intentCall.putExtra("projectName", projectName);
                startActivity(intentCall);
                break;
        }
        return true;
    }

    private void getGroupResponse() {
        leadListDetailPresenter.getResponseGroup("");
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @OnClick(R.id.btn_update_lanjut_fu)
    public void onClickBtnUpdateLanjut() {
        isFinish = "0";
        if (clicked == 0) {
            responseGroupRef = responseGroupRefIntent;
        } else {
            responseGroupRef = responseGroupRefSpinner;
        }

        if (flagResponeRef == 1) {
            responseRef = responseRefGone;
        }

        leadListDetailPresenter.postUpdateLeadResponseLanjut(psRef, username, leadRef, responseGroupRef, responseRef, isFinish, edtNote.getText().toString().trim());
    }

    @OnClick(R.id.btn_update_selesai_fu)
    public void onClickBtnUpdateSelesai() {
        isFinish = "1";
        if (clicked == 0) {
            responseGroupRef = responseGroupRefIntent;
        } else {
            responseGroupRef = responseGroupRefSpinner;
        }
        leadListDetailPresenter.postUpdateLeadResponseSelesai(psRef, username, leadRef, responseGroupRef, responseRef, isFinish, edtNote.getText().toString().trim());
    }

    @OnClick(R.id.btn_call)
    public void onCLickButtonCall() {
        typeCall = "1";
        leadListDetailPresenter.postUpdateCallLeadPhone(psRef, leadRef, leadPhone, typeCall);
    }

    @OnClick(R.id.btn_wa)
    public void onClickButtonWa() {
        call_whatsapp();
    }

    @OnClick(R.id.btn_wa2)
    public void onClickButtonWa2(){
        call_whatsapp2();
    }

    @OnClick(R.id.btn_wa3)
    public void onClickButtonWa3(){
        call_whatsapp3();
    }

    @OnClick(R.id.btn_wa4)
    public void onCLickButtonWa4(){
        call_whatsapp4();
    }

    @OnClick(R.id.img_status)
    public void onCLickStatusImage(){
        alertDialog = new AlertDialog.Builder(LeadDetailActivity.this);
        alertDialog.setMessage(statusColorMessage);
        alertDialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

            }
        });
        alertDialog.show();
    }

    @OnClick(R.id.txt_note_click)
    public void onClickNoteViewMore(){
        Intent i = new Intent(LeadDetailActivity.this, NoteExpandActivity.class);
        i.putExtra("leadRef", leadRef);
        startActivity(i);
    }

    @Override
    public void onSuccessGetResponseGroup(List<ListResponseGroup> listResponseGroup) {
        responseGroupAdapter = new ResponseGroupAdapter(this, listResponseGroup);
        statusSpinner.setAdapter(responseGroupAdapter);

        if (clicked == 0) {
            for (int i = 0; i < listResponseGroup.size(); i++) {
                if (listResponseGroup.get(i).getResponseGroupRef().equals(responseGroupRefIntent)) {
                    statusSpinner.setSelection(i);
                    break;
                }
            }
        } else {
            for (int i = 0; i < listResponseGroup.size(); i++) {
                if (listResponseGroup.get(i).getResponseGroupRef().equals(responseGroupRefSpinner)) {
                    statusSpinner.setSelection(i);
                    break;
                }
            }
        }



        listResponseGroups = listResponseGroup;


    }

    @Override
    public void onFailedGetResponseGroup(String string) {
        Utils.showSnackbar(layout, string);
    }

    @Override
    public void onSuccessGetListResponse(List<ListResponse> listResponse) {
        listResponseAdapter = new ListResponseAdapter(this, listResponse);
        responseList.setAdapter(listResponseAdapter);


        for (int i = 0; i < listResponse.size(); i++) {
            if (listResponse.get(i).getResponseRef().equals("1") || listResponse.get(i).getResponseRef().equals("2") || listResponse.get(i).getResponseRef().equals("12") ||
                    listResponse.get(i).getResponseRef().equals("13") || listResponse.get(i).getResponseRef().equals("14")) {
                responseList.setVisibility(View.GONE);
//                responseRef = listResponse.get(i).getResponseRef();
                responseRefGone = listResponse.get(i).getResponseRef();
                flagResponeRef = 1;
            }
            else {
                responseList.setVisibility(View.VISIBLE);
            }
        }

        listResponses = listResponse;

        responseList.setExpanded(true);
        responseList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        // ini agar pas milih yg ada respon listnya kaga ke highlight
        listResponseAdapter.setSelectedItem(0, Integer.parseInt(responseGroupRefIntent), Integer.parseInt(responseGroupRefSpinner), clicked);

        for (int h = 0; h < listResponse.size(); h++) {
            if (listResponse.get(h).getResponseRef().equals(responseRef)) {
                responseRef = listResponse.get(h).getResponseRef();
                listResponseAdapter.setSelectedItem(h, Integer.parseInt(responseGroupRefIntent), Integer.parseInt(responseGroupRefSpinner), clicked);

            }
        }


        if (clicked == 2) {
            for (int j = 0; j < listResponse.size(); j++) {
                if (listResponse.get(j).getResponseRef().equals(responseRef)) {
                    responseRef = listResponse.get(j).getResponseRef();
//                    listResponseAdapter.setSelectedItemUpdate(j);
                }
            }
            clicked = 1;
            flagUpdate = 0;
            responseGroupRefIntent = responseGroupRef;
        }



        //////
        if (flagResponeRef.equals("1")){

            // ini agar pas milih yg ada respon listnya kaga ke highlight
            listResponseAdapter.setSelectedItem(0, Integer.parseInt(responseGroupRefIntent), Integer.parseInt(responseGroupRefSpinner), clicked);

            for (int h = 0; h < listResponse.size(); h++) {
                if (listResponse.get(h).getResponseRef().equals(responseRef)) {
                    responseRef = listResponse.get(h).getResponseRef();
                    listResponseAdapter.setSelectedItem(h, Integer.parseInt(responseGroupRefIntent), Integer.parseInt(responseGroupRefSpinner), clicked);

                }
            }


            if (clicked == 2) {
                for (int j = 0; j < listResponse.size(); j++) {
                    if (listResponse.get(j).getResponseRef().equals(responseRef)) {
                        responseRef = listResponse.get(j).getResponseRef();
//                        listResponseAdapter.setSelectedItemUpdate(j);
                    }
                }
                clicked = 1;
                flagUpdate = 0;
                responseGroupRefIntent = responseGroupRef;
            }

        }
        /////




    }

    @Override
    public void onSuccessUpdateLeadLanjut(String message) {
        Utils.showSnackbar(layout, message);
        flagUpdate = 1;
        getGroupResponse();
    }

    @Override
    public void onFailedUpdateLeadLanjut(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onFailedUpdateLeadLanjutPhone(String message) {
//        Utils.showSnackbar(layout, message);

        alertDialog = new AlertDialog.Builder(LeadDetailActivity.this);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

            }
        });
        alertDialog.show();
    }

    @Override
    public void onFailedUpdateLeadSelesaiPhone(String message) {
//        Utils.showSnackbar(layout, message);

        alertDialog = new AlertDialog.Builder(LeadDetailActivity.this);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

            }
        });
        alertDialog.show();
    }

    @Override
    public void onSuccessUpdateLeadSelesai(String message) {
//        Intent returnIntent = new Intent();
//        setResult(Activity.RESULT_OK, returnIntent);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onFailedUpdateLeadSelesai(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onFailedUpdateLeadFromAndro(String string) {
        Utils.showSnackbar(layout, string);
    }

    @Override
    public void onSuccessUpdateCallLeadWa(String message) {
//        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onFailedUpdateCallLeadWa(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onSuccessUpdateCallLeadPhone(String message) {
//        Utils.showSnackbar(layout, message);
        if (isPermissionGranted()){
            call_action();
        }
    }

    @Override
    public void onFailedUpdateCallLeadPhone(String message) {
        Utils.showSnackbar(layout, message);
    }


    @Override
    public void onBackPressed() {
//        Intent returnIntent = new Intent();
//        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    private void call_whatsapp() {
        try {
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName(General.PACKAGE_WHATSHAPP, "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(leadPhone) + "@s.whatsapp.net");
            startActivity(sendIntent);
            typeCall = "2";
            leadListDetailPresenter.postUpdateCallLeadWa(psRef, leadRef, leadPhone, typeCall);
        } catch (Exception e) {
            Utils.showSnackbar(layout, getString(R.string.whatshapp_not_instal));
        }
    }

    public void call_action() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + "+"+PhoneNumberUtils.stripSeparators(leadPhone)));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("TAG", "Permission is granted");
                return true;
            } else {

                Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Log.v("TAG", "Permission is granted");
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    call_action();
                } else {
                    Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void call_whatsapp2() {
        try {
            Context context = this;
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            //data dummy
            String phone = leadPhone;
            String urlKP = linkPk;
            String url = "https://api.whatsapp.com/send?phone="+ phone +"&text=" + URLEncoder.encode("Hi, berikut isi link pk/brosur: \n" + urlKP, "UTF-8");
            //
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
                typeCall = "3";
                leadListDetailPresenter.postUpdateCallLeadWa(psRef, leadRef, leadPhone, typeCall);
            } else {
                Utils.showSnackbar(layout, getString(R.string.whatshapp_not_instal));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void call_whatsapp3(){
        try{
            Context context = this;
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String phone = leadPhone;
            String urlImage = linkImage;
            String url = "https://api.whatsapp.com/send?phone="+ phone +"&text=" + URLEncoder.encode("Hi, berikut isi link image gallery: \n" + urlImage, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
                typeCall = "5";
                leadListDetailPresenter.postUpdateCallLeadWa(psRef, leadRef, leadPhone, typeCall);
            } else {
                Utils.showSnackbar(layout, getString(R.string.whatshapp_not_instal));
            }
        } catch (Exception e){
            Utils.showSnackbar(layout, getString(R.string.whatshapp_not_instal));
        }
    }

    private void call_whatsapp4(){
        try{
            Context context = this;
            PackageManager packageManager = context.getPackageManager();
            Intent i = new Intent(Intent.ACTION_VIEW);
            String phone = leadPhone;
            String urlPriceList = linkPriceList;
            String url = "https://api.whatsapp.com/send?phone="+ phone +"&text=" + URLEncoder.encode("Hi, berikut isi link price list: \n" + urlPriceList, "UTF-8");
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                context.startActivity(i);
                typeCall = "4";
                leadListDetailPresenter.postUpdateCallLeadWa(psRef, leadRef, leadPhone, typeCall);
            } else {
                Utils.showSnackbar(layout, getString(R.string.whatshapp_not_instal));
            }
        } catch (Exception e){
            Utils.showSnackbar(layout, getString(R.string.whatshapp_not_instal));
        }
    }
}
