package id.nata.access360.view.downline.model;


import java.util.List;

public class DownlineList {

    private String level;
    private String psName;
    private String handphone;
    private String email;
    private List<ChildList> childList;

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<ChildList> getChildList() {
        return childList;
    }

    public void setChildList(List<ChildList> childList) {
        this.childList = childList;
    }
}
