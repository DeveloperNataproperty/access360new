package id.nata.access360.view.intro.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.intro.model.LaunchResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LaunchPresenter implements BasePresenter<LaunchInterface> {

    @Inject
    NetworkService networkService;

    private LaunchInterface view;
    private Context mContext;

    public LaunchPresenter(Context mContext) {
        super();
        ((BaseApplication) mContext.getApplicationContext()).getDeps().inject(this);
        this.mContext = mContext;
    }

    public void GETVersionApp(){
        view.showLoader();

        Call<LaunchResponse> callLaunch = networkService.GET_LaunchResponse("");
        callLaunch.enqueue(new Callback<LaunchResponse>() {
            @Override
            public void onResponse(Call<LaunchResponse> call, Response<LaunchResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccessGetVersionCode(response.body().getVersionCode(), response.body().getVersionName());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED) {
                        view.onFailedGetVersionCOde(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<LaunchResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(LaunchInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
