package id.nata.access360.view.menuProfile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.menuProfile.model.DataSex;

public class SpinnerDataSexAdapter extends BaseAdapter {
    private Context context;
    private List<DataSex> list;
    private ListCountryHolder holder;

    public SpinnerDataSexAdapter(Context context, List<DataSex> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.spinner_list_item,null);
            holder = new ListCountryHolder();
            holder.countryName = (TextView) convertView.findViewById(R.id.name);

            convertView.setTag(holder);
        }else{
            holder = (ListCountryHolder) convertView.getTag();
        }
        DataSex dataSex = list.get(position);
        holder.countryName.setText(dataSex.getSexName());

        return convertView;
    }

    private class ListCountryHolder {
        TextView countryName;
    }
}
