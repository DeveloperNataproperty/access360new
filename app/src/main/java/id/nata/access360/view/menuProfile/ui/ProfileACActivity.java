package id.nata.access360.view.menuProfile.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.menuBank.ui.InformasiBankActivity;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.menuProfile.adapter.CityAdapter;
import id.nata.access360.view.menuProfile.adapter.CountryAdapter;
import id.nata.access360.view.menuProfile.adapter.ProvinceAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataEducationAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataIncomeAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataJobTitleAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataMartialStatusAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataNationAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataOccupationAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataReligionAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerDataSexAdapter;
import id.nata.access360.view.menuProfile.adapter.SpinnerTipeDataAdapter;
import id.nata.access360.view.menuProfile.model.CityModel;
import id.nata.access360.view.menuProfile.model.CountryModel;
import id.nata.access360.view.menuProfile.model.DataEducation;
import id.nata.access360.view.menuProfile.model.DataIncome;
import id.nata.access360.view.menuProfile.model.DataJobTitle;
import id.nata.access360.view.menuProfile.model.DataMaritalStatus;
import id.nata.access360.view.menuProfile.model.DataNation;
import id.nata.access360.view.menuProfile.model.DataOccupation;
import id.nata.access360.view.menuProfile.model.DataPersonal;
import id.nata.access360.view.menuProfile.model.DataReligion;
import id.nata.access360.view.menuProfile.model.DataSex;
import id.nata.access360.view.menuProfile.model.DataType;
import id.nata.access360.view.menuProfile.model.ProvinceModel;
import id.nata.access360.view.menuProfile.process.ProfileACPresenter;
import id.nata.access360.view.menuProfile.process.ProfileACViewInterface;
import id.nata.access360.view.register.ui.RegisterValidasiActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ProfileACActivity extends BaseActivity implements ProfileACViewInterface {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.kd_member_tv)
    TextView txtKdMember;

    @BindView(R.id.txt_nama)
    TextView txtNama;
    @BindView(R.id.txt_username)
    TextView txtUsername;
    @BindView(R.id.txt_tempat_lahir)
    TextView txtTempatLahir;
    @BindView(R.id.txt_kd_member)
    TextView txtViewKdMember;
    @BindView(R.id.txt_hp)
    TextView txtHp;
    @BindView(R.id.txt_tgl_lahir)
    TextView txtTglLahir;

    @BindView(R.id.nama_edt)
    EditText edtNama;
    @BindView(R.id.username_edt)
    EditText edtUsername;
    @BindView(R.id.tempat_lahir_edt)
    EditText edtTempatLahir;
    @BindView(R.id.kd_member_edt)
    EditText edtKdMember;
    @BindView(R.id.hp_edt)
    EditText edtHp;
    @BindView(R.id.tgl_lahir_edt)
    EditText edtTglLahir;

    @BindView(R.id.negara_spinner)
    Spinner spnNegara;
    @BindView(R.id.propinsi_spinner)
    Spinner spnPropinsi;
    @BindView(R.id.kota_spinner)
    Spinner spnKota;
    @BindView(R.id.kd_pos_edt)
    EditText edtKdPos;
    @BindView(R.id.alamat_edt)
    EditText edtAlamat;

    @BindView(R.id.tipe_spinner)
    Spinner spnTipeData;
    @BindView(R.id.jenis_kelamin_spinner)
    Spinner spnJk;
    @BindView(R.id.agama_spinner)
    Spinner spnAgama;
    @BindView(R.id.status_spinner)
    Spinner spnStatus;
    @BindView(R.id.pekerjaan_spinner)
    Spinner spnPekerjaan;
    @BindView(R.id.jabatan_spinner)
    Spinner spnJabatan;
    @BindView(R.id.pendidikan_spinner)
    Spinner spnPendidikan;
    @BindView(R.id.pendapatan_spinner)
    Spinner spnPendapatan;
    @BindView(R.id.kewarganegaraan_spinner)
    Spinner spnKewarganegaraan;

    @BindView(R.id.kd_sponsor_edt)
    EditText edtKdSponsor;
    @BindView(R.id.upline_layout)
    LinearLayout uplineLayout;
    @BindView(R.id.upline_name_edt)
    EditText edtUplineName;
    @BindView(R.id.upline_email_edt)
    EditText edtUplineEmail;
    @BindView(R.id.upline_hp_edt)
    EditText edtUplineHp;

    @Inject
    ProfileACPresenter profileACPresenter;

    SessionManager sessionManager;

    String psRef, username;
    String psName, email, kodeMember, handphone, birthPlace, birthDate;
    String countryCode, provinceCode, cityCode, postCode, address;
    String personalType, sexRef, religionRef, maritalStatus, occupationRef, jobTitleRef, educationRef, incomeRef, nationRef;
    String kodeMemberUpline, uplineName, uplineEmail, uplineHP;

    private List<CountryModel> listCounty = new ArrayList<CountryModel>();
    private CountryAdapter adapterCountry;

    private List<ProvinceModel> listProvince = new ArrayList<ProvinceModel>();
    private ProvinceAdapter adapterProvince;

    private List<CityModel> listCity = new ArrayList<CityModel>();
    private CityAdapter adapterCity;

    private String idCountryCode = "", idProvinceCode = "", idCityCode = "";

    List<DataType> listTypeData = new ArrayList<DataType>();
    SpinnerTipeDataAdapter spinnerTipeDataAdapter;

    List<DataSex> listDataSex = new ArrayList<DataSex>();
    SpinnerDataSexAdapter spinnerDataSexAdapter;

    List<DataReligion> listDataReligion = new ArrayList<DataReligion>();
    SpinnerDataReligionAdapter spinnerDataReligionAdapter;

    List<DataMaritalStatus> listDataMaritalStatus = new ArrayList<DataMaritalStatus>();
    SpinnerDataMartialStatusAdapter spinnerDataMartialStatusAdapter;

    List<DataOccupation> listDataOccupation = new ArrayList<DataOccupation>();
    SpinnerDataOccupationAdapter spinnerDataOccupationAdapter;

    List<DataJobTitle> listDataJobTitle = new ArrayList<DataJobTitle>();
    SpinnerDataJobTitleAdapter spinnerDataJobTitleAdapter;

    List<DataEducation> listDataEducation = new ArrayList<DataEducation>();
    SpinnerDataEducationAdapter spinnerDataEducationAdapter;

    List<DataIncome> listDataIncome = new ArrayList<DataIncome>();
    SpinnerDataIncomeAdapter spinnerDataIncomeAdapter;

    List<DataNation> listDataNation = new ArrayList<DataNation>();
    SpinnerDataNationAdapter spinnerDataNationAdapter;

    private String idTypeData, idSexData, idReligionData, idMaritalStatus, idOccupationData;
    private String idJobTitleData, idEducationData, idIncomeData, idNationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile2);
        ButterKnife.bind(this);
        profileACPresenter = new ProfileACPresenter(this);
        profileACPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.profile));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        uplineLayout.setVisibility(View.GONE);

        initTextColor();

        getProfileInfo();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        spnNegara.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idCountryCode = listCounty.get(position).getCountryCode();
                listProvince.clear();
                requestProvince(idCountryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnPropinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idProvinceCode = listProvince.get(position).getProvinceCode();
                listCity.clear();
                requestCity(idCountryCode, idProvinceCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnKota.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idCityCode = listCity.get(position).getCityCode();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnTipeData.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idTypeData = listTypeData.get(position).getPersonalType();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnJk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idSexData = listDataSex.get(position).getSexRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnAgama.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idReligionData = listDataReligion.get(position).getReligionRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idMaritalStatus = listDataMaritalStatus.get(position).getMaritalStatus();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnPekerjaan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idOccupationData = listDataOccupation.get(position).getOccupationRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnJabatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idJobTitleData = listDataJobTitle.get(position).getJobTitleRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnPendidikan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idEducationData = listDataEducation.get(position).getEducationRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnPendapatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idIncomeData = listDataIncome.get(position).getIncomeRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnKewarganegaraan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idNationData = listDataNation.get(position).getNationRef();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void initTextColor() {
        String namaTextViewStr = "<font>Nama</font> <font color=#ff4242>*</font>";
        String usernameTextViewStr = "<font>Username (Email)</font> <font color=#ff4242>*</font>";
        String tmpLahirTextViewStr = "<font>Tempat Lahir</font> <font color=#ff4242>*</font>";
        String kdMemberTextViewStr = "<font>Kode Member</font> <font color=#ff4242>*</font>";
        String noHpTextViewStr = "<font>No. Hp</font> <font color=#ff4242>*</font>";
        String tglLahirTextViewStr = "<font>Tanggal Lahir</font> <font color=#ff4242>*</font>";

        txtNama.setText(Html.fromHtml(namaTextViewStr));
        txtUsername.setText(Html.fromHtml(usernameTextViewStr));
        txtTempatLahir.setText(Html.fromHtml(tmpLahirTextViewStr));
        txtViewKdMember.setText(Html.fromHtml(kdMemberTextViewStr));
        txtHp.setText(Html.fromHtml(noHpTextViewStr));
        txtTglLahir.setText(Html.fromHtml(tglLahirTextViewStr));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ProfileACActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.simpan_btn)
    public void btnSimpanProfileClicked() {
//        Intent i = new Intent(getApplicationContext(), InformasiBankActivity.class);
//        i.putExtra("idTypeData", idTypeData);
//        i.putExtra("idSexData", idSexData);
//        i.putExtra("idReligionData", idReligionData);
//        startActivity(i);

        if (edtUsername.getText().toString().equals("") || edtNama.getText().toString().equals("") || edtKdMember.getText().toString().equals("") || edtHp.getText().toString().equals("") ||
        edtTempatLahir.getText().toString().equals("") || edtTglLahir.getText().toString().equals("")){
            Toast.makeText(this, "Wajib isi field yang bertanda *", Toast.LENGTH_SHORT).show();
        } else {
            profileACPresenter.POSTUpdateProfileInfoSvc(psRef, edtUsername.getText().toString(), edtNama.getText().toString(), edtKdMember.getText().toString(),
                    edtHp.getText().toString(), edtTempatLahir.getText().toString(), edtTglLahir.getText().toString(), idCountryCode, idProvinceCode, idCityCode,
                    edtKdPos.getText().toString(), edtAlamat.getText().toString(), idTypeData, idSexData, idReligionData, idMaritalStatus, idOccupationData,
                    idJobTitleData, idEducationData, idIncomeData, idNationData, edtKdSponsor.getText().toString());
        }

    }

    private void getProfileInfo() {
        profileACPresenter.GETProfileInfoSvc(psRef, username);
    }

    private void getListCountry() {
        profileACPresenter.GETListCountrySvc("");
    }

    private void requestProvince(final String countryCode) {
        profileACPresenter.GETListProvinceSvc(countryCode);
    }

    private void requestCity(final String countryCode, final String provinceCode) {
        profileACPresenter.GETListCitySvc(countryCode, provinceCode);
    }

    private void getProfileLookup() {
        profileACPresenter.GETProfileLookupSvc("");
    }

    @Override
    public void onSuccesProfileInfo(DataPersonal dataPersonal) {
        psName = dataPersonal.getPsName();
        email = dataPersonal.getUsername();
        kodeMember = dataPersonal.getKodeMember();
        handphone = dataPersonal.getHandphone();
        birthPlace = dataPersonal.getBirthPlace();
        birthDate = dataPersonal.getBirthDate();

        countryCode = dataPersonal.getCountryCode();
        provinceCode = dataPersonal.getProvinceCode();
        cityCode = dataPersonal.getCityCode();
        postCode = dataPersonal.getPostCode();
        address = dataPersonal.getAddress();

        personalType = dataPersonal.getPersonalType();
        sexRef = dataPersonal.getSexRef();
        religionRef = dataPersonal.getReligionRef();
        maritalStatus = dataPersonal.getMaritalStatus();
        occupationRef = dataPersonal.getOccupationRef();
        jobTitleRef = dataPersonal.getJobTitleRef();
        educationRef = dataPersonal.getEducationRef();
        incomeRef = dataPersonal.getIncomeRef();
        nationRef = dataPersonal.getNationRef();

        kodeMemberUpline = dataPersonal.getKodeMemberUpline();
        uplineEmail = dataPersonal.getUplineEmail();
        uplineName = dataPersonal.getUplineName();
        uplineHP = dataPersonal.getUplineHP();

        if(!psName.isEmpty()) {
            edtNama.setText(psName);
        }

        if(!email.isEmpty()) {
            edtUsername.setText(email);
        }

        if(!kodeMember.isEmpty()) {
            txtKdMember.setText(kodeMember);
            edtKdMember.setText(kodeMember);
        }

        if(!handphone.isEmpty()) {
            edtHp.setText(handphone);
        }

        if(!birthPlace.isEmpty()) {
            edtTempatLahir.setText(birthPlace);
        }

        if(!birthDate.isEmpty()) {
            edtTglLahir.setText(birthDate);
        }

        if(!postCode.isEmpty()) {
            edtKdPos.setText(postCode);
        }

        if(!address.isEmpty()) {
            edtAlamat.setText(address);
        }

        if(!kodeMemberUpline.isEmpty()) {
            edtKdSponsor.setText(kodeMemberUpline);
            edtKdSponsor.setEnabled(false);
            uplineLayout.setVisibility(View.VISIBLE);
            edtUplineName.setText(uplineName);
            edtUplineName.setEnabled(false);
            edtUplineEmail.setText(uplineEmail);
            edtUplineEmail.setEnabled(false);
            edtUplineHp.setText(uplineHP);
            edtUplineHp.setEnabled(false);
        }

        edtUsername.setEnabled(false);
        edtKdMember.setEnabled(false);

        getListCountry();
        getProfileLookup();

    }

    @Override
    public void onSuccesCountry(List<CountryModel> countryModels) {

        adapterCountry = new CountryAdapter(this, countryModels);
        spnNegara.setAdapter(adapterCountry);

        for(int i=0; i < countryModels.size(); i++) {
            if(countryModels.get(i).getCountryCode().equals(countryCode)){
                spnNegara.setSelection(i);
                break;
            }
        }

        listCounty = countryModels;
//        requestProvince(countryCode);
    }


    @Override
    public void onSuccesProvince(List<ProvinceModel> provinceModels) {

        adapterProvince = new ProvinceAdapter(this, provinceModels);
        spnPropinsi.setAdapter(adapterProvince);

        for(int i=0; i < provinceModels.size(); i++) {
            if(provinceModels.get(i).getProvinceCode().equals(provinceCode)){
                spnPropinsi.setSelection(i);
                break;
            }
        }

        listProvince = provinceModels;
//        requestCity(countryCode, provinceCode);

    }

    @Override
    public void onSuccesCity(List<CityModel> cityModels) {

        adapterCity = new CityAdapter(this, cityModels);
        spnKota.setAdapter(adapterCity);

        for(int i=0; i < cityModels.size(); i++) {
            if(cityModels.get(i).getCityCode().equals(cityCode)){
                spnKota.setSelection(i);
                break;
            }
        }

        listCity = cityModels;
    }

    @Override
    public void onSuccess(List<DataType> dataType, List<DataSex> dataSex, List<DataReligion> dataReligion, List<DataMaritalStatus> dataMaritalStatus,
                          List<DataOccupation> dataOccupation, List<DataJobTitle> dataJobTitle, List<DataEducation> dataEducation,
                          List<DataIncome> dataIncome, List<DataNation> dataNation) {

        spinnerTipeDataAdapter = new SpinnerTipeDataAdapter(this, dataType);
        spnTipeData.setAdapter(spinnerTipeDataAdapter);

        for(int i=0; i < dataType.size(); i++) {
            if(dataType.get(i).getPersonalType().equals(personalType)){
                spnTipeData.setSelection(i);
                break;
            }
        }
        listTypeData = dataType;

        spinnerDataSexAdapter = new SpinnerDataSexAdapter(this, dataSex);
        spnJk.setAdapter(spinnerDataSexAdapter);
        for(int i=0; i < dataSex.size(); i++) {
            if(dataSex.get(i).getSexRef().equals(sexRef)){
                spnJk.setSelection(i);
                break;
            }
        }
        listDataSex = dataSex;

        spinnerDataReligionAdapter = new SpinnerDataReligionAdapter(this, dataReligion);
        spnAgama.setAdapter(spinnerDataReligionAdapter);
        for(int i=0; i < dataReligion.size(); i++) {
            if(dataReligion.get(i).getReligionRef().equals(religionRef)){
                spnAgama.setSelection(i);
                break;
            }
        }
        listDataReligion = dataReligion;

        spinnerDataMartialStatusAdapter = new SpinnerDataMartialStatusAdapter(this, dataMaritalStatus);
        spnStatus.setAdapter(spinnerDataMartialStatusAdapter);
        for(int i=0; i < dataMaritalStatus.size(); i++) {
            if(dataMaritalStatus.get(i).getMaritalStatus().equals(maritalStatus)){
                spnStatus.setSelection(i);
                break;
            }
        }
        listDataMaritalStatus = dataMaritalStatus;

        spinnerDataOccupationAdapter = new SpinnerDataOccupationAdapter(this, dataOccupation);
        spnPekerjaan.setAdapter(spinnerDataOccupationAdapter);
        spnPekerjaan.setSelection(3);
        for(int i=0; i < dataOccupation.size(); i++) {
            if(dataOccupation.get(i).getOccupationRef().equals(occupationRef)){
                spnPekerjaan.setSelection(i);
                break;
            }
        }
        listDataOccupation = dataOccupation;

        spinnerDataJobTitleAdapter = new SpinnerDataJobTitleAdapter(this, dataJobTitle);
        spnJabatan.setAdapter(spinnerDataJobTitleAdapter);
        for(int i=0; i < dataJobTitle.size(); i++) {
            if(dataJobTitle.get(i).getJobTitleRef().equals(jobTitleRef)){
                spnJabatan.setSelection(i);
                break;
            }
        }
        listDataJobTitle = dataJobTitle;

        spinnerDataEducationAdapter = new SpinnerDataEducationAdapter(this, dataEducation);
        spnPendidikan.setAdapter(spinnerDataEducationAdapter);
        for(int i=0; i < dataEducation.size(); i++) {
            if(dataEducation.get(i).getEducationRef().equals(educationRef)){
                spnPendidikan.setSelection(i);
                break;
            }
        }
        listDataEducation = dataEducation;

        spinnerDataIncomeAdapter = new SpinnerDataIncomeAdapter(this, dataIncome);
        spnPendapatan.setAdapter(spinnerDataIncomeAdapter);
        for(int i=0; i < dataIncome.size(); i++) {
            if(dataIncome.get(i).getIncomeRef().equals(incomeRef)){
                spnPendapatan.setSelection(i);
                break;
            }
        }
        listDataIncome = dataIncome;

        spinnerDataNationAdapter = new SpinnerDataNationAdapter(this, dataNation);
        spnKewarganegaraan.setAdapter(spinnerDataNationAdapter);
        for(int i=0; i < dataNation.size(); i++) {
            if(dataNation.get(i).getNationRef().equals(nationRef)){
                spnKewarganegaraan.setSelection(i);
                break;
            }
        }
        listDataNation = dataNation;

    }

    @Override
    public void onFailed(String paramStrErrorMessage) {
        Utils.showSnackbar(layout, paramStrErrorMessage);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {

    }
}
