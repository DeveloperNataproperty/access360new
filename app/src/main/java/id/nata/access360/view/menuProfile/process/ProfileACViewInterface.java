package id.nata.access360.view.menuProfile.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.menuProfile.model.CityModel;
import id.nata.access360.view.menuProfile.model.CountryModel;
import id.nata.access360.view.menuProfile.model.DataEducation;
import id.nata.access360.view.menuProfile.model.DataIncome;
import id.nata.access360.view.menuProfile.model.DataJobTitle;
import id.nata.access360.view.menuProfile.model.DataMaritalStatus;
import id.nata.access360.view.menuProfile.model.DataNation;
import id.nata.access360.view.menuProfile.model.DataOccupation;
import id.nata.access360.view.menuProfile.model.DataPersonal;
import id.nata.access360.view.menuProfile.model.DataReligion;
import id.nata.access360.view.menuProfile.model.DataSex;
import id.nata.access360.view.menuProfile.model.DataType;
import id.nata.access360.view.menuProfile.model.ProvinceModel;

public interface ProfileACViewInterface extends BaseInterface {

    void onSuccesProfileInfo(DataPersonal dataPersonal);

    void onSuccesCountry(List<CountryModel> countryList);

    void onSuccesProvince(List<ProvinceModel> provinceList);

    void onSuccesCity(List<CityModel> cityList);

    void onSuccess(List<DataType> dataType, List<DataSex> dataSex, List<DataReligion> dataReligion, List<DataMaritalStatus> dataMaritalStatus,
                   List<DataOccupation> dataOccupation, List<DataJobTitle> dataJobTitle, List<DataEducation> dataEducation,
                   List<DataIncome> dataIncome, List<DataNation> dataNation);

    void onFailed(String paramStrErrorMessage);
}
