package id.nata.access360.view.login.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.model.ResponeModel;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.login.model.ForgotPasswordParam;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.nata.access360.config.General.IS_MOBILE;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public class ForgotPasswordPresenter implements BasePresenter<ForgotPasswordViewInterface> {

    @Inject
    NetworkService networkService;

    private ForgotPasswordViewInterface view;
    private Context context;
    private Realm mRealm;

    public ForgotPasswordPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
        mRealm = Realm.getDefaultInstance();
    }


    public void PostForgotPasswordSVC(String username) {
        view.showLoader();

        ForgotPasswordParam passwordParam = new ForgotPasswordParam();
        passwordParam.setUsername(username);

        Call<ResponeModel> callLogin = networkService.PostForgotPasswordSVC(Utils.modelToJson(passwordParam));
        callLogin.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, final Response<ResponeModel> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccess(response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED){
                        view.onSuccess(response.body().getMessage());
                    } else {
                        view.onFailed(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(ForgotPasswordViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
