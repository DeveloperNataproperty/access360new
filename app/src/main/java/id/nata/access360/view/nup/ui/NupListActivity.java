package id.nata.access360.view.nup.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.nup.adapter.ListNupDetailAdapter;
import id.nata.access360.view.nup.model.ListNupDetail;
import id.nata.access360.view.nup.process.ListNupDetailInterface;
import id.nata.access360.view.nup.process.ListNupDetailPresenter;

public class NupListActivity extends BaseActivity implements ListNupDetailInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_nup_detail)
    ListView statusNupList;

    @Inject
    ListNupDetailPresenter listNupDetailPresenter;

    private ListNupDetailAdapter listNupDetailAdapter;
    private SessionManager sessionManager;
    private String psRef, username, statusNup, urlNup;
    private List<ListNupDetail> listNupDetailsNew = new ArrayList<ListNupDetail>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup_list);
        ButterKnife.bind(this);
        listNupDetailPresenter = new ListNupDetailPresenter(this);
        listNupDetailPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);
        Intent intent = getIntent();
        statusNup = intent.getStringExtra("psRefKey");

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.status_nup));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getListNupDetail();

        statusNupList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                urlNup = listNupDetailsNew.get(i).getUrlNUP();

                Uri uri = Uri.parse(urlNup);
                Intent intentNup = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intentNup);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupListActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                break;
        }
        return true;
    }

    private void getListNupDetail(){

        listNupDetailPresenter.getListNupDetail(psRef, username, statusNup);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessGetNupDetail(List<ListNupDetail> listNupDetails) {
        listNupDetailAdapter = new ListNupDetailAdapter(this, listNupDetails);
        statusNupList.setAdapter(listNupDetailAdapter);
        listNupDetailsNew = listNupDetails;
    }

    @Override
    public void onFailedGetNupDetail(String string) {
        Utils.showSnackbar(layout, string);
    }
}
