package id.nata.access360.view.register.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.model.ResponeModel;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.login.model.LoginRespone;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.nata.access360.config.General.API_REQUEST_FAILED;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public class RegisterValidasiPresenter implements BasePresenter<RegisterValidasiViewInterface> {

    @Inject
    NetworkService networkService;

    private RegisterValidasiViewInterface view;
    private Context context;

    public RegisterValidasiPresenter(Context context) {
        super();
        ((BaseApplication) context).getDeps().inject(this);
        this.context = context;
    }

    public void PostRegistrationCodeSVC(String jsonIn) {
        view.showLoader();
        Call<LoginRespone> callLogin = networkService.PostRegistrationCodeSVC(jsonIn);
        callLogin.enqueue(new Callback<LoginRespone>() {
            @Override
            public void onResponse(Call<LoginRespone> call, Response<LoginRespone> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccess(response.body().getDataLogin(), response.body().getMessage());
                    }  else if (response.body().getStatus() == API_REQUEST_FAILED) {
                        view.onCodeFailed(response.body().getMessage());
                    } else {
                        view.onFailed(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<LoginRespone> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    public void PostResendingRegistrationCodeSVC(String jsonIn) {
        view.showLoaderDialog(context.getString(R.string.please_wait));
        Call<ResponeModel> callLogin = networkService.PostResendingRegistrationCodeSVC(jsonIn);
        callLogin.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, Response<ResponeModel> response) {
                view.dismissLoaderDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccessReSendCode(response.body().getMessage());
                    } else {
                        view.onFailedReSendCode(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.dismissLoaderDialog();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(RegisterValidasiViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
