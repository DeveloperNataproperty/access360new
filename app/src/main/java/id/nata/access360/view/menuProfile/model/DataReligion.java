package id.nata.access360.view.menuProfile.model;

public class DataReligion {
    String religionRef, religionName;

    public String getReligionRef() {
        return religionRef;
    }

    public void setReligionRef(String religionRef) {
        this.religionRef = religionRef;
    }

    public String getReligionName() {
        return religionName;
    }

    public void setReligionName(String religionName) {
        this.religionName = religionName;
    }
}
