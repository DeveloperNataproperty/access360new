package id.nata.access360.view.menuBank.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.menuBank.Model.BankListData;
import id.nata.access360.view.menuBank.ui.InformasiBankActivity;

/**
 * Created by NATA on 2/12/19.
 */

public class BankListAdapter extends BaseAdapter {
    private Context context;
    private List<BankListData> list;
    private ListBankDataList holder;
    private InformasiBankActivity informasiBankActivity;


    public BankListAdapter(Context context, List<BankListData> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(R.layout.item_bank_list,null);
            holder = new ListBankDataList();
            holder.bankRef = (TextView) view.findViewById(R.id.bank_ref);
            holder.bankName = (TextView) view.findViewById(R.id.bank_name);

            view.setTag(holder);
        }else{
            holder = (ListBankDataList) view.getTag();
        }
        BankListData bankListData = list.get(i);
        holder.bankRef.setText(bankListData.getBankRef());
        holder.bankName.setText(bankListData.getBankName());

        return view;
    }

    private class ListBankDataList {
        TextView bankRef, bankName;
    }
}
