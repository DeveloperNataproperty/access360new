package id.nata.access360.view.lead.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.lead.model.LeadList;

public class LeadListAdapter extends BaseAdapter {

    private Context context;
    private List<LeadList> list;
    private LeadListHolder holder;

    public LeadListAdapter(Context context, List<LeadList> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_lead,null);
            holder = new LeadListHolder();
            holder.nameText = (TextView) convertView.findViewById(R.id.txt_name);
            holder.hpText = (TextView) convertView.findViewById(R.id.txt_hp);
            holder.emailText = (TextView) convertView.findViewById(R.id.txt_email);
            holder.kotaText = (TextView) convertView.findViewById(R.id.txt_kota);
            holder.statusImage = (ImageView) convertView.findViewById(R.id.img_status);
            holder.txtResponse = (TextView) convertView.findViewById(R.id.txt_response);

            convertView.setTag(holder);
        }else{
            holder = (LeadListHolder) convertView.getTag();
        }

        LeadList leadList = list.get(pos);
        holder.nameText.setText(leadList.getLeadName());
        holder.hpText.setText(leadList.getLeadPhone());
        holder.emailText.setText(leadList.getLeadEmail());
        holder.kotaText.setText(leadList.getLeadCity());
        holder.txtResponse.setText(leadList.getResponseName());
        if (leadList.getStatusColor().equals("#ffffff")){
            holder.statusImage.setVisibility(View.INVISIBLE);
        } else {
            Drawable background = holder.statusImage.getBackground();
            ((GradientDrawable) background).setColor(Color.parseColor(leadList.getStatusColor()));
        }

        return convertView;
    }

    private class LeadListHolder {
        TextView nameText, hpText, emailText, kotaText, txtResponse;
        ImageView statusImage;
    }
}
