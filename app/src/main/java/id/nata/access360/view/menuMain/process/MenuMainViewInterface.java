package id.nata.access360.view.menuMain.process;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.menuMain.model.DataMenu;

public interface MenuMainViewInterface extends BaseInterface {

    void onSuccess(DataMenu dataMenu);

    void onSuccessRegisterFCM(String paramStrSuccessrMessage);

    void onFailed(String paramStrErrorMessage);
}
