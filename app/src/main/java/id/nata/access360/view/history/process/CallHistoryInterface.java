package id.nata.access360.view.history.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.history.model.CallHistoryData;

public interface CallHistoryInterface extends BaseInterface {
    void onSuccessGetCallHistory(List<CallHistoryData> callHistoryData);

    void onFailedGetCallHistory(String string);
}
