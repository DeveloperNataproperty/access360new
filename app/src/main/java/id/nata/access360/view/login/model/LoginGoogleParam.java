package id.nata.access360.view.login.model;

/**
 * Created by "dwist14"
 * on Sep 9/14/2017 10:58.
 * Project : Napro
 */
public class LoginGoogleParam {
    String email, loginSource, isMobile, idGoogle;

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }

    public String getIdGoogle() {
        return idGoogle;
    }

    public void setIdGoogle(String idGoogle) {
        this.idGoogle = idGoogle;
    }
}
