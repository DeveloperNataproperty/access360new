package id.nata.access360.view.lead.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.lead.model.LeadParam;
import id.nata.access360.view.lead.model.LeadProjectList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadProjectPresenter implements BasePresenter<LeadProjectInterface> {

    @Inject
    NetworkService networkService;

    private LeadProjectInterface view;
    private Context context;

    public LeadProjectPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void getLeadProjectListSVC(String psRef, String username){
        view.showLoader();

        LeadParam param = new LeadParam();
        param.setPsRef(psRef);
        param.setUsername(username);

        Call<List<LeadProjectList>> callLeadProjectList = networkService.GET_LeadProjectList(Utils.modelToJson(param));
        callLeadProjectList.enqueue(new Callback<List<LeadProjectList>>() {
            @Override
            public void onResponse(Call<List<LeadProjectList>> call, Response<List<LeadProjectList>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetLeadProjectList(response.body());
                    } else {
                        view.onFailedGetLeadProjectList(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<LeadProjectList>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(LeadProjectInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
