package id.nata.access360.view.login.model;

/**
 * Created by "dwist14"
 * on Nov 11/10/2017 14:59.
 * Project : Napro
 */
public class ForgotPasswordParam {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
