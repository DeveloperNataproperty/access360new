package id.nata.access360.view.lead.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.lead.model.LeadList;

public interface LeadInterface extends BaseInterface {

    void onSuccessGetLeadList(List<LeadList> leadList);

    void onFailedGetLeadList(String string);

}
