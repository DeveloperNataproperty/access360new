package id.nata.access360.view.history.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.history.model.CallHistoryData;
import id.nata.access360.view.history.model.HistoryParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallHistoryPresenter implements BasePresenter<CallHistoryInterface> {

    @Inject
    NetworkService networkService;

    private CallHistoryInterface view;
    private Context context;

    public CallHistoryPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void getListCallHistory(String psRef, String leadRef){
        view.showLoader();

        HistoryParam param = new HistoryParam();
        param.setPsRef(psRef);
        param.setLeadRef(leadRef);

        Call<List<CallHistoryData>> callHistoryDataSVC = networkService.GET_CallHistory(Utils.modelToJson(param));
        callHistoryDataSVC.enqueue(new Callback<List<CallHistoryData>>() {
            @Override
            public void onResponse(Call<List<CallHistoryData>> call, Response<List<CallHistoryData>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetCallHistory(response.body());
                    } else {
                        view.onFailedGetCallHistory(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<CallHistoryData>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(CallHistoryInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
