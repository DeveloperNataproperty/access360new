package id.nata.access360.view.menuProfile.model;

public class DataIncome {
    String incomeRef, incomeName;

    public String getIncomeRef() {
        return incomeRef;
    }

    public void setIncomeRef(String incomeRef) {
        this.incomeRef = incomeRef;
    }

    public String getIncomeName() {
        return incomeName;
    }

    public void setIncomeName(String incomeName) {
        this.incomeName = incomeName;
    }
}
