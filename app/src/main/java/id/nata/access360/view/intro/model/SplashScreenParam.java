package id.nata.access360.view.intro.model;

/**
 * Created by "dwist14"
 * on Feb 2/6/2018 23:40.
 * Project : Napro
 */
public class SplashScreenParam {
    String loginSource;
    String isMobile;

    public String getLoginSource() {
        return loginSource;
    }

    public void setLoginSource(String loginSource) {
        this.loginSource = loginSource;
    }

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }
}
