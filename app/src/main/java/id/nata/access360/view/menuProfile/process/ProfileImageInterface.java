package id.nata.access360.view.menuProfile.process;

import id.nata.access360.base.BaseInterface;

public interface ProfileImageInterface extends BaseInterface {
    void onSuccessUpload(String message);

    void onFailedUpload(String message);
}
