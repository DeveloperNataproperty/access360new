package id.nata.access360.view.menuProfile.model;

public class DataNation {
    String nationRef, nationName;

    public String getNationRef() {
        return nationRef;
    }

    public void setNationRef(String nationRef) {
        this.nationRef = nationRef;
    }

    public String getNationName() {
        return nationName;
    }

    public void setNationName(String nationName) {
        this.nationName = nationName;
    }
}
