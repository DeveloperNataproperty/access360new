package id.nata.access360.view.register.model;

/**
 * Created by nata on 4/10/2017.
 */

public class UserInfoFacebookModel {
    public String name;

    public String email;

    public String facebookID;

    public String gender;

    public String urlPhotoProfile;

    public String birthday;
}
