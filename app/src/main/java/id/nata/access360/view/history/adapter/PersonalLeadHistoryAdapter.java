package id.nata.access360.view.history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.history.model.PersonalLeadHistoryData;

public class PersonalLeadHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<PersonalLeadHistoryData> list;
    private PersonalLeadHistoryHolder holder;

    public PersonalLeadHistoryAdapter(Context context, List<PersonalLeadHistoryData> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_personal_lead_history,null);
            holder = new PersonalLeadHistoryHolder();
            holder.nameText = (TextView) convertView.findViewById(R.id.txt_name);
            holder.emailText = (TextView) convertView.findViewById(R.id.txt_email);
            holder.hpText = (TextView) convertView.findViewById(R.id.txt_hp);
            holder.kotaText = (TextView) convertView.findViewById(R.id.txt_kota);
            holder.responText = (TextView) convertView.findViewById(R.id.txt_respon);
            holder.noteText = (TextView) convertView.findViewById(R.id.txt_note);

            convertView.setTag(holder);
        }else{
            holder = (PersonalLeadHistoryHolder) convertView.getTag();
        }

        PersonalLeadHistoryData personalLeadHistoryData = list.get(pos);
        holder.nameText.setText(personalLeadHistoryData.getLeadName());
        holder.emailText.setText(personalLeadHistoryData.getLeadEmail());
        holder.hpText.setText(personalLeadHistoryData.getLeadPhone());
        holder.kotaText.setText(personalLeadHistoryData.getLeadCity());
        holder.responText.setText(personalLeadHistoryData.getResponseName());
        holder.noteText.setText(personalLeadHistoryData.getNoted());

        return convertView;
    }

    private class PersonalLeadHistoryHolder {
        TextView nameText, emailText, hpText, kotaText, responText, noteText;
    }
}
