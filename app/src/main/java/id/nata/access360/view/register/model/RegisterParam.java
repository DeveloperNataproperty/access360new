package id.nata.access360.view.register.model;

/**
 * Created by "dwist14"
 * on Sep 9/14/2017 09:29.
 * Project : Napro
 */
public class RegisterParam {
    String personalType, personalName, personalHp, birthDate, username,password, kodeSponsor;

    public String getPersonalType() {
        return personalType;
    }

    public void setPersonalType(String personalType) {
        this.personalType = personalType;
    }

    public String getPersonalName() {
        return personalName;
    }

    public void setPersonalName(String personalName) {
        this.personalName = personalName;
    }

    public String getPersonalHp() {
        return personalHp;
    }

    public void setPersonalHp(String personalHp) {
        this.personalHp = personalHp;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getKodeSponsor() {
        return kodeSponsor;
    }

    public void setKodeSponsor(String kodeSponsor) {
        this.kodeSponsor = kodeSponsor;
    }
}
