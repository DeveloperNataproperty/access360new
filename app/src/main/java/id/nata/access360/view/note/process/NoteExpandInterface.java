package id.nata.access360.view.note.process;

import id.nata.access360.base.BaseInterface;

public interface NoteExpandInterface extends BaseInterface {
    void onSuccessGetNote(String noted, String message);

    void onFailedGetNote(String message);
}
