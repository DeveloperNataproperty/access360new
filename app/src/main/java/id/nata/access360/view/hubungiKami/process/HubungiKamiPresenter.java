package id.nata.access360.view.hubungiKami.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.hubungiKami.model.ContactUsParam;
import id.nata.access360.view.hubungiKami.model.ContactUsResponse;
import id.nata.access360.view.menuProfile.model.DataPersonal;
import id.nata.access360.view.menuProfile.model.ProfileInfoResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HubungiKamiPresenter implements BasePresenter<HubungiKamiInterface> {

    @Inject
    NetworkService networkService;

    private HubungiKamiInterface view;
    private Context mContext;

    public HubungiKamiPresenter(Context mContext) {
        super();
        ((BaseApplication) mContext.getApplicationContext()).getDeps().inject(this);
        this.mContext = mContext;
    }

    public void GETProfileInfoSvc(String strPsRef, String strUsername) {
        view.showLoader();

        DataPersonal param = new DataPersonal();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);

        Call<ProfileInfoResponse> callInfo = networkService.GET_ProfileInformation(Utils.modelToJson(param));
        callInfo.enqueue(new Callback<ProfileInfoResponse>() {
            @Override
            public void onResponse(Call<ProfileInfoResponse> call, Response<ProfileInfoResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccesProfileInfo(response.body().getDataPersonal());
                    } else {
                        view.onFailed(mContext.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ProfileInfoResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void POST_ContactUs(String psRef, String message){
        view.showLoader();

        ContactUsParam param = new ContactUsParam();
        param.setPsRef(psRef);
        param.setMessage(message);

        Call<ContactUsResponse> postContactUs = networkService.POST_ContactUs(Utils.modelToJson(param));
        postContactUs.enqueue(new Callback<ContactUsResponse>() {
            @Override
            public void onResponse(Call<ContactUsResponse> call, Response<ContactUsResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus()==General.API_REQUEST_SUCCESS){
                        view.onSuccessPostMessage(response.body().getMessage());
                    } else if (response.body().getStatus()==General.API_REQUEST_FAILED){
                        view.onFailedPostMessage(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(HubungiKamiInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
