package id.nata.access360.view.downline.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.NonScrollExpandableListView;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.downline.adapter.DownlineAdapter;
import id.nata.access360.view.downline.model.DownlineList;
import id.nata.access360.view.downline.process.DownlineInterface;
import id.nata.access360.view.downline.process.DownlinePresenter;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;

public class DownlineActivity extends AppCompatActivity implements DownlineInterface {

    private SessionManager sessionManager;
    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_task)
    NonScrollExpandableListView expandableListView;
    @BindView(R.id.txt_no_data)
    TextView noDataText;

    @Inject
    DownlinePresenter downlinePresenter;

    private DownlineAdapter downlineAdapter;

    private String psRef, username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downline);
        ButterKnife.bind(this);
        downlinePresenter = new DownlinePresenter(this);
        downlinePresenter.setupView(this);

        sessionManager = new SessionManager(this);
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Downline");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getDownline();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(DownlineActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                break;
        }
        return true;
    }

    private void getDownline() {
        downlinePresenter.getDownlineSVC(psRef, username);
    }

    @Override
    public void showLoader() {}

    @Override
    public void dismissLoader() {}

    @Override
    public void onSuccesGetDownline(List<DownlineList> downlineList) {
        downlineAdapter = new DownlineAdapter(this, downlineList);
        expandableListView.setAdapter(downlineAdapter);
        for (int i = 0; i < downlineAdapter.getGroupCount(); i++) {
            expandableListView.expandGroup(i);
        }

        noDataText.setVisibility(View.GONE);
    }

    @Override
    public void onEmptyGetDownline(String string) {
        Utils.showSnackbar(layout, string);
        noDataText.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }
}
