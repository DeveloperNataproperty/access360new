package id.nata.access360.view.register.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.login.model.LoginData;
import id.nata.access360.view.login.model.ReSendCodeParam;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.register.model.RegisterValidasiParam;
import id.nata.access360.view.register.process.RegisterValidasiPresenter;
import id.nata.access360.view.register.process.RegisterValidasiViewInterface;
import me.philio.pinentry.PinEntryView;

/**
 * Created by "dwist14"
 * on Sep 9/25/2017 15:38.
 * Project : Napro
 */
public class RegisterValidasiActivity extends AppCompatActivity implements RegisterValidasiViewInterface {
    @Inject
    RegisterValidasiPresenter registerValidasiPresenter;

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.layout)
    RelativeLayout layout;
    @BindView(R.id.txt_re_send_code)
    TextView txtResendCode;
    @BindView(R.id.pin_entry_border)
    PinEntryView pinEntryBorder;

    SessionManager sessionManager;

    String registrationCode;

//    String strUserRef, strFullname, strEmail, strLinkProfile;
    String strPsRef, strUsername, strPsName, strPsCode;
    String psRef, username;

    ProgressDialog mProgressDialog;
    


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_validasi);
        ((BaseApplication) getApplication()).getDeps().inject(this);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        psRef = intent.getStringExtra("psRef");
        username = intent.getStringExtra("username");

        registerValidasiPresenter.setupView(this);
        sessionManager = new SessionManager(this);
        mProgressDialog = new ProgressDialog(this);
    }

    //klik submit
    @OnClick(R.id.btn_submit)
    public void btnSubmitOnClick() {
        Utils.hideSoftKeyboard(this);

//        Intent i = new Intent(getApplicationContext(), ProjectMenu2Activity.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(i);

        registrationCode = pinEntryBorder.getText().toString();
        if (!registrationCode.equals("")) {
            RegisterValidasiParam param = new RegisterValidasiParam();
            param.setPsRef(psRef);
            param.setUsername(username);
            param.setVerificationCode(registrationCode);
            registerValidasiPresenter.PostRegistrationCodeSVC(Utils.modelToJson(param));
        } else {
            Utils.showSnackbar(layout, getString(R.string.must_be_field));
        }

    }

    @Override
    public void showLoader() {
        btnSubmit.setText("");
        btnSubmit.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoader() {
        btnSubmit.setText(getString(R.string.submit));
        btnSubmit.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onSuccess(LoginData data, String message) {
        Utils.showSnackbar(layout, message);
        strPsRef = data.getPsRef();
        strUsername = data.getUsername();
        strPsName = data.getPsName();
        strPsCode = data.getPsCode();

        sessionManager = new SessionManager(getApplicationContext());
        sessionManager.createLoginSession(strPsRef, strUsername, strPsName, strPsCode);

        Intent i = new Intent(getApplicationContext(), ProjectMenu2Activity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    //Failed general
    @Override
    public void onFailed(String paramStrErrorMessage) {
        Utils.showSnackbar(layout, paramStrErrorMessage);
    }

    //jika kode salah
    @Override
    public void onCodeFailed(String message) {
        Utils.showSnackbar(layout, message);

    }

    //resendcode
    @OnClick(R.id.txt_re_send_code)
    public void txtReSendCode() {
        ReSendCodeParam param = new ReSendCodeParam();
        param.setUserRef(sessionManager.getStringFromSP(General.USER_REF));
        param.setIsMobile(General.IS_MOBILE);
        registerValidasiPresenter.PostResendingRegistrationCodeSVC(Utils.modelToJson(param));
    }

    @Override
    public void showLoaderDialog(String message) {
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setInverseBackgroundForced(false);
        mProgressDialog.show();
    }

    @Override
    public void dismissLoaderDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessReSendCode(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onFailedReSendCode(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onSuccessExDate(String message) {
        Utils.showSnackbar(layout, getString(R.string.re_send_code_desc));
    }

}
