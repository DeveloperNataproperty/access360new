package id.nata.access360.view.nup.model;

/**
 * Created by NATA on 2/13/19.
 */

public class StatusNup {
    private String statusRef;
    private String statusName;
    private String totalNUP;

    public String getStatusRef() {
        return statusRef;
    }

    public void setStatusRef(String statusRef) {
        this.statusRef = statusRef;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTotalNUP() {
        return totalNUP;
    }

    public void setTotalNUP(String totalNUP) {
        this.totalNUP = totalNUP;
    }
}
