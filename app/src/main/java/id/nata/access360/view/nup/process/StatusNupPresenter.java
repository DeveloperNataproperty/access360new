package id.nata.access360.view.nup.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.nup.model.StatusNup;
import id.nata.access360.view.nup.model.StatusNupParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NATA on 2/13/19.
 */

public class StatusNupPresenter implements BasePresenter<StatusNupInterface> {

    @Inject
    NetworkService networkService;

    private StatusNupInterface view;
    private Context mContext;

    public StatusNupPresenter(Context context) {
        super();
        //((BaseApplication) context).getDeps().inject(this);
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.mContext = context;
    }

    public void getStatusNupSVC(String strPsRef, String strUsername){
        view.showLoader();

        StatusNupParam param = new StatusNupParam();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);

        Call<List<StatusNup>> callStatusNup = networkService.GET_StatusNUP(Utils.modelToJson(param));
        callStatusNup.enqueue(new Callback<List<StatusNup>>() {
            @Override
            public void onResponse(Call<List<StatusNup>> call, Response<List<StatusNup>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetNup(response.body());
                    } else {
                        view.onFailedGetNup(mContext.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<StatusNup>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(StatusNupInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
