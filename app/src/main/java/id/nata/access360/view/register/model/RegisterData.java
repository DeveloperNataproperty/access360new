package id.nata.access360.view.register.model;

/**
 * Created by "dwist14"
 * on Sep 9/25/2017 16:06.
 * Project : Napro
 */
public class RegisterData {
    private String psRef;
    private String username;
    private String psName;
    private String psCode;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getPsCode() {
        return psCode;
    }

    public void setPsCode(String psCode) {
        this.psCode = psCode;
    }
}
