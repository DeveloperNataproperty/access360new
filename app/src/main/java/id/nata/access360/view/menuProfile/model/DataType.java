package id.nata.access360.view.menuProfile.model;

public class DataType {
    String personalType, personalTypeName;

    public String getPersonalType() {
        return personalType;
    }

    public void setPersonalType(String personalType) {
        this.personalType = personalType;
    }

    public String getPersonalTypeName() {
        return personalTypeName;
    }

    public void setPersonalTypeName(String personalTypeName) {
        this.personalTypeName = personalTypeName;
    }
}
