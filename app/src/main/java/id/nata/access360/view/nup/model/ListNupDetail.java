package id.nata.access360.view.nup.model;

/**
 * Created by NATA on 2/14/19.
 */

public class ListNupDetail {
    private String projectName;
    private String nupOrderCode;
    private String jumlah;
    private String harga;
    private String totalHarga;
    private String customerName;
    private String urlNUP;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getNupOrderCode() {
        return nupOrderCode;
    }

    public void setNupOrderCode(String nupOrderCode) {
        this.nupOrderCode = nupOrderCode;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getTotalHarga() {
        return totalHarga;
    }

    public void setTotalHarga(String totalHarga) {
        this.totalHarga = totalHarga;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getUrlNUP() {
        return urlNUP;
    }

    public void setUrlNUP(String urlNUP) {
        this.urlNUP = urlNUP;
    }
}
