package id.nata.access360.view.intro.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.intro.model.SplashScreenRespone;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public class SplashScreenPresenter implements BasePresenter<SplashScreenViewInterface> {

    @Inject
    NetworkService networkService;

    private SplashScreenViewInterface view;
    private Context context;
    private Realm mRealm;

    public SplashScreenPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
        mRealm = Realm.getDefaultInstance();
    }

    public void GetAppsInfoSvc(String jsonIn) {
        view.showLoader();
        networkService.GetVersionApps(jsonIn)
                .enqueue(new Callback<SplashScreenRespone>() {
                    @Override
                    public void onResponse(Call<SplashScreenRespone> call, final Response<SplashScreenRespone> response) {
                        view.dismissLoader();
                        if (response.isSuccessful()){
                            view.onSuccess(response.body().getVersionCode(), response.body().getVersionName());
                        } else {
                            view.onNetworkError(context.getResources().getString(R.string.network_error));
                        }
                    }

                    @Override
                    public void onFailure(Call<SplashScreenRespone> call, Throwable t) {
                        view.dismissLoader();
                        view.onNetworkError(context.getResources().getString(R.string.network_error));
                    }
                });

    }

    @Override
    public void setupView(SplashScreenViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
