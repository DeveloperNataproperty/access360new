package id.nata.access360.view.lead.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.lead.model.LeadProjectList;

public interface LeadProjectInterface extends BaseInterface {

    void onSuccessGetLeadProjectList(List<LeadProjectList> leadProjectList);

    void onFailedGetLeadProjectList(String string);
}
