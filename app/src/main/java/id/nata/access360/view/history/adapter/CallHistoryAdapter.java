package id.nata.access360.view.history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.history.model.CallHistoryData;

public class CallHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<CallHistoryData> list;
    private CallHistoryHolder holder;

    public CallHistoryAdapter(Context context, List<CallHistoryData> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_call_history,null);
            holder = new CallHistoryHolder();
            holder.nameText = (TextView) convertView.findViewById(R.id.txt_name);
            holder.tglText = (TextView) convertView.findViewById(R.id.txt_tgl);
            holder.callTypeText = (TextView) convertView.findViewById(R.id.txt_type_call);

            convertView.setTag(holder);
        }else{
            holder = (CallHistoryHolder) convertView.getTag();
        }

        CallHistoryData callHistoryData = list.get(pos);
        holder.nameText.setText(callHistoryData.getMemberName());
        holder.tglText.setText(callHistoryData.getInputTime());
        holder.callTypeText.setText(callHistoryData.getTypeCall());

        return convertView;
    }

    private class CallHistoryHolder {
        TextView nameText, tglText, callTypeText;
    }
}
