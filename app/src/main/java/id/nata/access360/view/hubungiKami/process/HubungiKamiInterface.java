package id.nata.access360.view.hubungiKami.process;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.menuProfile.model.DataPersonal;

public interface HubungiKamiInterface extends BaseInterface {
    void onSuccesProfileInfo(DataPersonal dataPersonal);

    void onFailed(String string);

    void onSuccessPostMessage(String message);

    void onFailedPostMessage(String message);
}
