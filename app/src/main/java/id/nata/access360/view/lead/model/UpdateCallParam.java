package id.nata.access360.view.lead.model;

public class UpdateCallParam {
    private String psRef;
    private String leadRef;
    private String phone;
    private String typeCall;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCallType() {
        return typeCall;
    }

    public void setCallType(String callType) {
        this.typeCall = callType;
    }
}
