package id.nata.access360.view.lead.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.lead.model.LeadList;
import id.nata.access360.view.lead.model.LeadParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeadPresenter implements BasePresenter<LeadInterface> {

    @Inject
    NetworkService networkService;

    private LeadInterface view;
    private Context context;

    public LeadPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void getLeadListSVC(String psRef, String usernanme, String projectRef){
        view.showLoader();

        LeadParam param = new LeadParam();
        param.setPsRef(psRef);
        param.setUsername(usernanme);
        param.setProjectRef(projectRef);

        Call<List<LeadList>> callLeadList = networkService.GET_LeadList(Utils.modelToJson(param));
        callLeadList.enqueue(new Callback<List<LeadList>>() {
            @Override
            public void onResponse(Call<List<LeadList>> call, Response<List<LeadList>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetLeadList(response.body());
                    } else {
                        view.onFailedGetLeadList(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<LeadList>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });


    }

    @Override
    public void setupView(LeadInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
