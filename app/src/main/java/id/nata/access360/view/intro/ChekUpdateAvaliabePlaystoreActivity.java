package id.nata.access360.view.intro;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;

/**
 * Created by "dwist14"
 * on Feb 2/6/2018 20:34.
 * Project : Napro
 */
public class ChekUpdateAvaliabePlaystoreActivity extends AppCompatActivity {
    @BindView(R.id.btn_update)
    Button btnUpdate;
    @BindView(R.id.txt_version)
    TextView txtVersion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chek_update_available_playstore_activiy);
        ButterKnife.bind(this);

        txtVersion.setText(getIntent().getStringExtra("version"));

    }

    //button update click
    @OnClick(R.id.btn_update)
    public void setBtnUpdate(){
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }



}
