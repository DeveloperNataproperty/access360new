package id.nata.access360.view.nup.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.nup.model.ListNupDetail;
import id.nata.access360.view.nup.model.NupDetailListParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NATA on 2/14/19.
 */

public class ListNupDetailPresenter implements BasePresenter<ListNupDetailInterface> {

    @Inject
    NetworkService networkService;

    private ListNupDetailInterface view;
    private Context mContext;

    public ListNupDetailPresenter(Context mContext) {
        super();
        //((BaseApplication) context).getDeps().inject(this);
        ((BaseApplication) mContext.getApplicationContext()).getDeps().inject(this);
        this.mContext = mContext;
    }

    public void getListNupDetail(String strPsRef, String strUsername, String strStatusNup){
        view.showLoader();

        NupDetailListParam param = new NupDetailListParam();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);
        param.setStatusNup(strStatusNup);

        Call<List<ListNupDetail>> callListNupDetail = networkService.GET_ListNUP(Utils.modelToJson(param));
        callListNupDetail.enqueue(new Callback<List<ListNupDetail>>() {
            @Override
            public void onResponse(Call<List<ListNupDetail>> call, Response<List<ListNupDetail>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetNupDetail(response.body());
                    } else {
                        view.onFailedGetNupDetail(mContext.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<ListNupDetail>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(ListNupDetailInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
