package id.nata.access360.view.menuMain.model;

public class DataMenu {
    private String psRef;
    private String username;
    private String urlViewProject;
    private String urlPhoto;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUrlViewProject() {
        return urlViewProject;
    }

    public void setUrlViewProject(String urlViewProject) {
        this.urlViewProject = urlViewProject;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }
}
