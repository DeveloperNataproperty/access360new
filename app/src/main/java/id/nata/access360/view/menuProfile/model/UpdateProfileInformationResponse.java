package id.nata.access360.view.menuProfile.model;

public class UpdateProfileInformationResponse {
    int status;
    String message;
    DataPersonal dataPersonal;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataPersonal getDataPersonal() {
        return dataPersonal;
    }

    public void setDataPersonal(DataPersonal dataPersonal) {
        this.dataPersonal = dataPersonal;
    }
}
