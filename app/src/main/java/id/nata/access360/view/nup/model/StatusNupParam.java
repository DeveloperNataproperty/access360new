package id.nata.access360.view.nup.model;

/**
 * Created by NATA on 2/13/19.
 */

public class StatusNupParam {
    String psRef, username;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
