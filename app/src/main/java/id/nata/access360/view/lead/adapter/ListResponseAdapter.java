package id.nata.access360.view.lead.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.lead.model.ListResponse;

public class ListResponseAdapter extends BaseAdapter {

    private Context context;
    private List<ListResponse> list;
    private ListResponseHolder holder;

    public ListResponseAdapter(Context context, List<ListResponse> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_response_list,null);
            holder = new ListResponseHolder();
            holder.responseName = (TextView) convertView.findViewById(R.id.txt_response_name);
            holder.imgCeklis = (ImageView) convertView.findViewById(R.id.img_ceklis);

            convertView.setTag(holder);
        }else{
            holder = (ListResponseHolder) convertView.getTag();
        }

        ListResponse listResponse = list.get(i);
        holder.responseName.setText(listResponse.getResponseName());

        // set selected item
        LinearLayout ActiveItem = (LinearLayout) convertView;

        if (clicked == 0) {
            if (i == selectedItem){
                if (groupRefItem == groupRefClicked) {
//                    ActiveItem.setBackgroundResource(R.color.dot_dark_screen);
//                    holder.responseName.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                    holder.imgCeklis.setVisibility(View.VISIBLE);

                    // for focus on it
                    int top = ActiveItem.getTop();
                    ((ListView) viewGroup).setSelection(i);
                }
            } else {
                holder.imgCeklis.setVisibility(View.GONE);
            }
        }

        if (clicked == 1) {
            if (i == selectedItem){
                if (groupRefItem == groupRefClicked) {
//                    ActiveItem.setBackgroundResource(R.color.dot_dark_screen);
//                    holder.responseName.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                    holder.imgCeklis.setVisibility(View.VISIBLE);

                    // for focus on it
                    int top = ActiveItem.getTop();
                    ((ListView) viewGroup).setSelection(i);
                }
            } else {
                holder.imgCeklis.setVisibility(View.GONE);
            }
        }

        if (clicked == 2) {
            if (i == selectedItem){
//                    ActiveItem.setBackgroundResource(R.color.dot_dark_screen);
//                    holder.responseName.setTextColor(ContextCompat.getColor(context, R.color.colorWhite));
                    holder.imgCeklis.setVisibility(View.VISIBLE);

                    // for focus on it
                    int top = ActiveItem.getTop();
                    ((ListView) viewGroup).setSelection(i);

            } else {
                holder.imgCeklis.setVisibility(View.GONE);
            }
        }

        return convertView;
    }

    private int selectedItem, groupRefItem, groupRefClicked, clicked;

    public void setSelectedItem(int position, int groupRef, int groupRefClick, int click) {
        groupRefItem = groupRef;
        selectedItem = position;
        groupRefClicked = groupRefClick;
        clicked = click;
    }

    public void setSelectedItemUpdate(int position) {
        selectedItem = position;
    }


    private class ListResponseHolder {
        TextView responseName;
        ImageView imgCeklis;
    }


}
