package id.nata.access360.view.menuProfile.model;

public class DataJobTitle {
    String jobTitleRef, jobTitleName;

    public String getJobTitleRef() {
        return jobTitleRef;
    }

    public void setJobTitleRef(String jobTitleRef) {
        this.jobTitleRef = jobTitleRef;
    }

    public String getJobTitleName() {
        return jobTitleName;
    }

    public void setJobTitleName(String jobTitleName) {
        this.jobTitleName = jobTitleName;
    }
}
