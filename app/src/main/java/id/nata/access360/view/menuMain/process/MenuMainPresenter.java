package id.nata.access360.view.menuMain.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.fcm.model.FCMTokenParam;
import id.nata.access360.helper.Utils;
import id.nata.access360.model.ResponeModel;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.menuMain.model.MenuRequest;
import id.nata.access360.view.menuMain.model.MenuResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.nata.access360.config.General.PS_REF;
import static id.nata.access360.config.General.USER_REF;

public class MenuMainPresenter implements BasePresenter<MenuMainViewInterface> {

    @Inject
    NetworkService networkService;

    private MenuMainViewInterface view;
    private Context mContext;

    private SessionManager mSessionManager;

    public MenuMainPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.mContext = context;
        mSessionManager = SessionManager.getInstance(mContext);
    }

    public void GET_MenuInformation(String strPsRef, String strUsername) {
        view.showLoader();

        MenuRequest param = new MenuRequest();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);

        Call<MenuResponse> callMenu = networkService.GET_MenuInformation(Utils.modelToJson(param));
        callMenu.enqueue(new Callback<MenuResponse>() {
            @Override
            public void onResponse(Call<MenuResponse> call, Response<MenuResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccess(response.body().getDataMenu());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED) {
                        view.onFailed(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<MenuResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void PostFCMTokenSVC(String strPsRef, String fcmToken) {

        FCMTokenParam param = new FCMTokenParam();
        param.setPsRef(strPsRef);
        param.setToken(fcmToken);

        Utils.showLog("TAG", "FCM_TOKEN " + fcmToken +" psRef "+mSessionManager.getStringFromSP(PS_REF));

        Call<ResponeModel> callLogin = networkService.PostFCMTokenSVC(Utils.modelToJson(param));
        callLogin.enqueue(new Callback<ResponeModel>() {
            @Override
            public void onResponse(Call<ResponeModel> call, Response<ResponeModel> response) {
                if (response.isSuccessful()) {
                    if (response.isSuccessful()) {
                        view.onSuccessRegisterFCM(response.body().getMessage());
                    } else {
                        view.onFailed(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ResponeModel> call, Throwable t) {
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(MenuMainViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
