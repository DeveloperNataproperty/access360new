package id.nata.access360.view.intro;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.intro.model.SplashScreenParam;
import id.nata.access360.view.intro.process.LaunchInterface;
import id.nata.access360.view.intro.process.LaunchPresenter;
import id.nata.access360.view.intro.process.SplashScreenPresenter;
import id.nata.access360.view.intro.process.SplashScreenViewInterface;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 13:30.
 * Project : Napro
 */
public class SplashScreenActivity extends BaseActivity implements LaunchInterface {
    @Inject
    LaunchPresenter launchPresenter;

    public static final String TAG = SplashScreenActivity.class.getSimpleName();
    private static final int SLEEP_TIME = 1000;
    SessionManager sessionManager;
    private Context mContext;

    String versionName = "";
    int versionCodeHp = -1;

    AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.splashscreen_activity);

        launchPresenter = new LaunchPresenter(this);
        launchPresenter.setupView(this);

        getVersionInfo();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashScreenParam param = new SplashScreenParam();
                param.setLoginSource(General.LOGIN_SOURCE);
                param.setIsMobile(General.IS_MOBILE);
                launchPresenter.GETVersionApp();
            }
        }, SLEEP_TIME);

        //panggil ke service untuk cek update aplication


        mContext = SplashScreenActivity.this;
        sessionManager = new SessionManager(this);
//        changeStatusBarColor();
    }


    private void getVersionInfo() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCodeHp = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        Utils.showLog(TAG, versionName + " " + versionCodeHp);
    }

    /**
     * Making notification bar transparent
     */
//    private void changeStatusBarColor() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = getWindow();
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.setStatusBarColor(Color.TRANSPARENT);
//        }
//    }

    /**
     * check session login,
     * jika posisinya session login true, maka langsung redirect ke halaman home,
     * jika false, tampilin dulu halaman login
     */

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Toast.makeText(mContext, appErrorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSuccessGetVersionCode(String versionCode, String versionName) {
        if (versionCodeHp<Integer.parseInt(versionCode)){
            if (!isFinishing()) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SplashScreenActivity.this);
                alertDialogBuilder
                        .setMessage("Update Available v" + versionName)
                        .setCancelable(false)
                        .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=id.nata.access360")));
                            }
                        })
                        /*.setNegativeButton("No",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        })*/;
                alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        } else {
            checkSessionLogin();
        }
    }

    @Override
    public void onFailedGetVersionCOde(String message) {

    }

    private void checkSessionLogin() {
        Class classActivity;
        if (sessionManager.isLoggedIn()) {
            classActivity = ProjectMenu2Activity.class;
        } else {
            classActivity = WelcomeActivity.class;
        }
        Intent i = new Intent(mContext, classActivity);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(i);
        finish();
    }
}
