package id.nata.access360.view.nup.model;

/**
 * Created by NATA on 2/14/19.
 */

public class NupDetailListParam {
    private String psRef;
    private String username;
    private String statusNup;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatusNup() {
        return statusNup;
    }

    public void setStatusNup(String statusNup) {
        this.statusNup = statusNup;
    }
}
