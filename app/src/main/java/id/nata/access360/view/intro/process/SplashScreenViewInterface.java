package id.nata.access360.view.intro.process;


import id.nata.access360.base.BaseInterface;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface SplashScreenViewInterface extends BaseInterface {

    void onSuccess(String versionCode, String versionName);

    void onFailed();
}
