package id.nata.access360.view.menuBank.Model;

/**
 * Created by NATA on 2/12/19.
 */

public class BankListData {
    private String bankRef;
    private String bankName;

    public String getBankRef() {
        return bankRef;
    }

    public void setBankRef(String bankRef) {
        this.bankRef = bankRef;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }
}
