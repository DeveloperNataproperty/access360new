package id.nata.access360.view.login.model;

/**
 * Created by "dwist14"
 * on Sep 9/26/2017 11:19.
 * Project : Napro
 */
public class ReSendCodeParam {
    String userRef, isMobile;

    public String getIsMobile() {
        return isMobile;
    }

    public void setIsMobile(String isMobile) {
        this.isMobile = isMobile;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }
}
