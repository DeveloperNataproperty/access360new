package id.nata.access360.view.login.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.login.model.LoginGoogleParam;
import id.nata.access360.view.login.model.LoginParam;
import id.nata.access360.view.login.model.LoginRequest;
import id.nata.access360.view.login.model.LoginRespone;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.nata.access360.config.General.IS_MOBILE;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public class LoginPresenter implements BasePresenter<LoginViewInterface> {

    @Inject
    NetworkService networkService;

    private LoginViewInterface view;
    private Context mContext;

    public LoginPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.mContext = context;
    }

    public void PostLoginUserSVC(String strEmail, String strPassword, String strIdFacebook) {
        view.showLoader();

        LoginParam param = new LoginParam();
        param.setEmail(strEmail);
        param.setPassword(strPassword);
        param.setLoginSource(General.LOGIN_SOURCE);
        param.setIsMobile(IS_MOBILE);
        param.setIdFacebook(strIdFacebook);

        Call<LoginRespone> callLogin = networkService.PostLoginUserSVC(Utils.modelToJson(param));
        callLogin.enqueue(new Callback<LoginRespone>() {
            @Override
            public void onResponse(Call<LoginRespone> call, Response<LoginRespone> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccess(response.body().getDataLogin());
                    } else if (response.body().getStatus() == General.API_REQUEST_ACCOUNT_NOT_ACTIVE) {
                        view.onSuccessAccountNotActive(response.body().getDataLogin());
                    } else {
                        view.onFailed("Email dan/atau Password tidak cocok, silakan coba lagi");
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<LoginRespone> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void POST_LoginSvc(String strEmail, String strPassword) {
        view.showLoader();

        LoginRequest param = new LoginRequest();
        param.setUsername(strEmail);
        param.setPassword(strPassword);

        String aa = Utils.modelToJson(param);
        System.out.println(aa);

        Call<LoginRespone> callLogin = networkService.POST_LoginSvc(Utils.modelToJson(param));
        callLogin.enqueue(new Callback<LoginRespone>() {
            @Override
            public void onResponse(Call<LoginRespone> call, Response<LoginRespone> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccess(response.body().getDataLogin());
                    } else if (response.body().getStatus() == General.API_REQUEST_ACCOUNT_NOT_ACTIVE) {
                        view.onSuccessAccountNotActive(response.body().getDataLogin());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED) {
                        view.onFailed(response.body().getMessage());
                    } else {
                        view.onFailed("Email dan/atau Password tidak cocok, silakan coba lagi");
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<LoginRespone> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });

    }


    public void PostLoginGoogleSVC(String strEmail, String strIdGoole) {
        view.showLoader();

        LoginGoogleParam param = new LoginGoogleParam();
        param.setEmail(strEmail);
        param.setLoginSource(General.LOGIN_SOURCE);
        param.setIsMobile(IS_MOBILE);
        param.setIdGoogle(strIdGoole);

        Call<LoginRespone> callLogin = networkService.PostLoginGoogleSVC(Utils.modelToJson(param));
        callLogin.enqueue(new Callback<LoginRespone>() {
            @Override
            public void onResponse(Call<LoginRespone> call, Response<LoginRespone> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccess(response.body().getDataLogin());
                    } else if (response.body().getStatus() == General.API_REQUEST_ACCOUNT_NOT_ACTIVE) {
                        view.onSuccessAccountNotActive(response.body().getDataLogin());
                    } else {
                        view.onFailed(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<LoginRespone> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(LoginViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
