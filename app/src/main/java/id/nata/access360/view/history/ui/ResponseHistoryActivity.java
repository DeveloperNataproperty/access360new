package id.nata.access360.view.history.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.history.adapter.ResponseHistoryAdapter;
import id.nata.access360.view.history.model.ResponseHistoryData;
import id.nata.access360.view.history.process.ResponseHistoryInterface;
import id.nata.access360.view.history.process.ResponseHistoryPresenter;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;

public class ResponseHistoryActivity extends BaseActivity implements ResponseHistoryInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_response_history)
    ListView listResponse;

    @Inject
    ResponseHistoryPresenter responseHistoryPresenter;

    private SessionManager sessionManager;
    private String psRef, leadRef, projectName;

    private ResponseHistoryAdapter responseHistoryAdapter;
    private List<ResponseHistoryData> responseHistoryDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_response_history);
        ButterKnife.bind(this);

        responseHistoryPresenter = new ResponseHistoryPresenter(this);
        responseHistoryPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                leadRef = getIntent().getStringExtra("leadRef");
                projectName = getIntent().getStringExtra("projectName");
            }
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(projectName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        reqResponseHistory();
    }

    private void reqResponseHistory() {
        responseHistoryPresenter.getListResponseHistorySVC(psRef, leadRef);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessGetResponseHistory(List<ResponseHistoryData> responseHistoryList) {
        responseHistoryAdapter = new ResponseHistoryAdapter(this, responseHistoryList);
        listResponse.setAdapter(responseHistoryAdapter);

        responseHistoryDataList = responseHistoryList;
    }

    @Override
    public void onFailedGetResponseHistory(String string) {
        Utils.showSnackbar(layout, string);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(ResponseHistoryActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
