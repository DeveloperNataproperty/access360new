package id.nata.access360.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.nup.model.ListNupDetail;
import id.nata.access360.view.nup.ui.NupListActivity;

/**
 * Created by NATA on 2/14/19.
 */

public class ListNupDetailAdapter extends BaseAdapter{

    private Context context;
    private List<ListNupDetail> list;
    private ListNupDetailHolder holder;

    public ListNupDetailAdapter(Context context, List<ListNupDetail> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.custom_list_nup_detail,null);
            holder = new ListNupDetailHolder();
            holder.textNameProject = (TextView) convertView.findViewById(R.id.txt_name_project);
            holder.textNupOrder = (TextView) convertView.findViewById(R.id.txt_nup_order_code);
            holder.textJumlah = (TextView) convertView.findViewById(R.id.txt_jumlah);
            holder.textHarga = (TextView) convertView.findViewById(R.id.txt_harga);
            holder.textTotalHarga = (TextView) convertView.findViewById(R.id.txt_total_harga);
            holder.textNamaCust = (TextView) convertView.findViewById(R.id.txt_nama_cust);

            convertView.setTag(holder);
        }else{
            holder = (ListNupDetailHolder) convertView.getTag();
        }

        ListNupDetail statusNup = list.get(i);
        holder.textNameProject.setText(statusNup.getProjectName());
        holder.textNupOrder.setText(statusNup.getNupOrderCode());
        holder.textJumlah.setText(statusNup.getJumlah());
        holder.textHarga.setText(statusNup.getHarga());
        holder.textTotalHarga.setText(statusNup.getTotalHarga());
        holder.textNamaCust.setText(statusNup.getCustomerName());

        return convertView;
    }

    private class ListNupDetailHolder {
        TextView textNameProject, textNupOrder, textJumlah, textHarga, textTotalHarga, textNamaCust;
    }
}
