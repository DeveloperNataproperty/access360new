package id.nata.access360.view.menuProfile.process;

import android.content.ComponentName;
import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.menuProfile.model.ProfileImageParam;
import id.nata.access360.view.menuProfile.model.ProfileImageResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileImagePresenter implements BasePresenter<ProfileImageInterface> {

    @Inject
    NetworkService networkService;

    private ProfileImageInterface view;
    private Context context;

    public ProfileImagePresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void POST_ImageProfileSVC(String psRef, String image){
        view.showLoader();

        ProfileImageParam param = new ProfileImageParam();
        param.setPsRef(psRef);
        param.setImage(image);

        Call<ProfileImageResponse> postImageResponse = networkService.POST_ImageProfile(Utils.modelToJson(param));
        postImageResponse.enqueue(new Callback<ProfileImageResponse>() {
            @Override
            public void onResponse(Call<ProfileImageResponse> call, Response<ProfileImageResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccessUpload(response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED){
                        view.onFailedUpload(response.body().getMessage());
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ProfileImageResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }


    @Override
    public void setupView(ProfileImageInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
