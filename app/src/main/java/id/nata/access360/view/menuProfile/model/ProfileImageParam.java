package id.nata.access360.view.menuProfile.model;

public class ProfileImageParam {
    private String psRef;
    private String image;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
