package id.nata.access360.view.history.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.history.adapter.PersonalLeadHistoryAdapter;
import id.nata.access360.view.history.model.PersonalLeadHistoryData;
import id.nata.access360.view.history.process.PersonalLeadHistoryInterface;
import id.nata.access360.view.history.process.PersonalLeadHistoryPresenter;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;

public class PersonalLeadHistory extends BaseActivity implements PersonalLeadHistoryInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_personallead_history)
    ListView personalLeadHistoryList;

    @Inject
    PersonalLeadHistoryPresenter personalLeadHistoryPresenter;

    private SessionManager sessionManager;
    private String psRef, username, projectRef, projectName;

    private PersonalLeadHistoryAdapter personalLeadHistoryAdapter;
    private List<PersonalLeadHistoryData> personalLeadHistoryDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_lead_history);
        ButterKnife.bind(this);

        personalLeadHistoryPresenter = new PersonalLeadHistoryPresenter(this);
        personalLeadHistoryPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                projectRef = getIntent().getStringExtra("projectRef");
                projectName = getIntent().getStringExtra("projectName");
            }
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(projectName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getPersonalLead();
    }

    private void getPersonalLead() {
        personalLeadHistoryPresenter.getPersonalLeadHistorySVC(psRef, username, projectRef);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccesGetPersonalLeadList(List<PersonalLeadHistoryData> callPersonalLeadHistory) {
        personalLeadHistoryAdapter = new PersonalLeadHistoryAdapter(this, callPersonalLeadHistory);
        personalLeadHistoryList.setAdapter(personalLeadHistoryAdapter);
    }

    @Override
    public void onFailedGetPersonalLeadList(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(PersonalLeadHistory.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
