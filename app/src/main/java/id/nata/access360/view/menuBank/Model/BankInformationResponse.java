package id.nata.access360.view.menuBank.Model;

/**
 * Created by NATA on 2/13/19.
 */

public class BankInformationResponse {
    int status;
    String message;
    BankInformationData dataBank;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BankInformationData getDataBank() {
        return dataBank;
    }

    public void setDataBank(BankInformationData dataBank) {
        this.dataBank = dataBank;
    }
}
