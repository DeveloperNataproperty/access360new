package id.nata.access360.view.lead.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.lead.model.ListResponseGroup;

public class ResponseGroupAdapter extends BaseAdapter {
    private Context context;
    private List<ListResponseGroup> list;
    private ResponseGroupHolder holder;

    public ResponseGroupAdapter(Context context, List<ListResponseGroup> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null){
            view = LayoutInflater.from(context)
                    .inflate(R.layout.item_spinner_response_group,null);
            holder = new ResponseGroupHolder();
            holder.groupNameResponseText = (TextView) view.findViewById(R.id.txt_response_group_name);

            view.setTag(holder);
        }else{
            holder = (ResponseGroupHolder) view.getTag();
        }

        ListResponseGroup listResponseGroup = list.get(i);
        holder.groupNameResponseText.setText(listResponseGroup.getResponseGroupName());

        return view;
    }

    private class ResponseGroupHolder {
        TextView groupNameResponseText;
    }
}
