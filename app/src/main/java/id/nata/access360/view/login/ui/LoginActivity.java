package id.nata.access360.view.login.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONObject;

import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.config.PrefUtils;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.helper.UtilsSnackbar;
import id.nata.access360.view.login.model.LoginData;
import id.nata.access360.view.login.process.LoginPresenter;
import id.nata.access360.view.login.process.LoginViewInterface;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.register.model.UserInfoFacebookModel;
import id.nata.access360.view.register.ui.RegisterOptionActivity;
import id.nata.access360.view.register.ui.RegisterValidasiActivity;

import static id.nata.access360.config.General.EMAIL;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:04.
 * Project : Napro
 */
public class LoginActivity extends AppCompatActivity
        implements LoginViewInterface,
        GoogleApiClient.OnConnectionFailedListener {
    GoogleApiClient mGoogleApiClient;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 007;

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.txt_forgot_password)
    TextView txtForgotPassword;
    @BindView(R.id.edt_email_layout)
    TextInputLayout edtEmailLayout;
    @BindView(R.id.edt_password_layout)
    TextInputLayout edtPasswordLayout;

    @Inject
    LoginPresenter loginPresenter;
    private SessionManager mSessionManager;
    UserInfoFacebookModel user;
    private CallbackManager callbackManager;
    String email, password;
    String strPsRef, strFullname, strEmail, strPsCode;
    private AccessToken accessToken;

    public FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        loginPresenter = new LoginPresenter(this);
        loginPresenter.setupView(this);

//        firebaseAuth = FirebaseAuth.getInstance();

        accessToken = AccessToken.getCurrentAccessToken();
        mSessionManager = new SessionManager(getApplicationContext());

        init();
    }

    private void init() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        //With Facebook
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, mCallBack);
    }

    //click login button
    @OnClick(R.id.btn_login)
    public void btnLoginOnClick() {
        Utils.hideSoftKeyboard(this);

//        Intent i = new Intent(getApplicationContext(), ProjectMenu2Activity.class);
//        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        startActivity(i);

        email = edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();

        if (validate()) {
//            loginPresenter.PostLoginUserSVC(email, password, "");
            loginPresenter.POST_LoginSvc(email, password);
        }
    }

    //click google
//    @OnClick(R.id.layout_google)
//    public void signInGoogle() {
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }
//
//    //click facebook
//    @OnClick(R.id.layout_facebook)
//    public void signInFacebookOnClick() {
//        Utils.showLog("login", "fb button on click");
//        if (AccessToken.getCurrentAccessToken() != null) {
//            //  LoginManager.getInstance().logOut();
//            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
//        } else {
//            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_friends"));
//        }
//    }

    //click forgot pass
    @OnClick(R.id.txt_forgot_password)
    public void setTxtForgotPassword() {
        startActivity(new Intent(this, ForgotPasswordActivity.class).putExtra(EMAIL, strEmail));
    }

    private boolean validate() {
        boolean valid = true;
        if (email.equals("")) {
            edtEmailLayout.setError(getString(R.string.must_be_field));
            valid = false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            edtEmailLayout.setError(getString(R.string.email_not_valid));
            valid = false;
        } else {
            edtEmailLayout.setError(null);
        }
        if (password.equals("")) {
            edtPasswordLayout.setError(getString(R.string.must_be_field));
            valid = false;
        } else {
            edtPasswordLayout.setError(null);
        }
        return valid;
    }


    //akan buka halaman Register
    @OnClick(R.id.txt_register)
    public void btnRegisterOnClicked() {
        Intent i = new Intent(this, RegisterOptionActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void showLoader() {
        btnLogin.setText("");
        btnLogin.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoader() {
        btnLogin.setText(getString(R.string.Login));
        btnLogin.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        UtilsSnackbar.showSnakbarTypeThree(layout, this);
    }

    //succes login
    @Override
    public void onSuccess(LoginData data) {
        strPsRef = data.getPsRef();
        strEmail = data.getUsername();
        strFullname = data.getPsName();
        strPsCode = data.getPsCode();

        mSessionManager.createLoginSession(strPsRef, strEmail, strFullname, strPsCode);

        Intent i = new Intent(getApplicationContext(), ProjectMenu2Activity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
        finish();
    }

    //succes tapi account belum active
    @Override
    public void onSuccessAccountNotActive(LoginData data) {
        Intent i = new Intent(getApplicationContext(), RegisterValidasiActivity.class);
        i.putExtra("psRef", data.getPsRef());
        i.putExtra("username", data.getUsername());
        startActivity(i);
    }

    @Override
    public void onFailed(String paramStrErrorMessage) {
        UtilsSnackbar.showSnakbarTypeOne(layout, paramStrErrorMessage);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//        }

        if (requestCode == RC_SIGN_IN) {

            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);

            if (googleSignInResult.isSuccess()) {

                GoogleSignInAccount googleSignInAccount = googleSignInResult.getSignInAccount();

//                FirebaseUserAuth(googleSignInAccount);
            }

        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            String googleId = acct.getId();
            String personName = acct.getDisplayName();
            String email = acct.getEmail();

            googleRespone(true, email, personName, googleId);
        } else {
            // Signed out, show unauthenticated UI.
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            googleRespone(false, "", "", "");
                        }
                    });
        }
//    }
//
//    public void FirebaseUserAuth(GoogleSignInAccount googleSignInAccount) {
//
//        AuthCredential authCredential = GoogleAuthProvider.getCredential(googleSignInAccount.getIdToken(), null);
//
////        Toast.makeText(RegisterOptionActivity.this,""+ authCredential.getProvider(),Toast.LENGTH_LONG).show();
//
////        firebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(this,
//                new OnCompleteListener() {
//                    @Override
//                    public void onComplete(@NonNull Task AuthResultTask) {
//
//                        if (AuthResultTask.isSuccessful()) {
//
//                            // Getting Current Login user details.
////                            FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
////
////                            String personName = firebaseUser.getDisplayName();
////                            String email = firebaseUser.getEmail();
////                            String googleId = firebaseUser.getUid();
////
////                            Log.e(TAG, "Name: " + personName + ", email: " + email + ", " + googleId);
////                            googleRespone(true, email, personName, googleId);
//
//                        } else {
//                            Toast.makeText(getApplicationContext(), getString(R.string.something_error), Toast.LENGTH_LONG).show();
//                        }
//                    }
//                });
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    //FB
    private FacebookCallback<LoginResult> mCallBack = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Utils.showLog("callbackFB", "callbackFB success");
            GraphRequest request = GraphRequest.newMeRequest(
                    loginResult.getAccessToken(),
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            Utils.showLog("response: ", response + "");
                            String emailId = null;
                            try {
                                user = new UserInfoFacebookModel();
                                user.facebookID = object.getString("id").toString();
                                user.name = object.getString("name").toString();
                                user.gender = object.getString("gender").toString();
                                user.urlPhotoProfile = "http://graph.facebook.com/" + user.facebookID + "/picture?type=large";
                                user.email = object.getString("email").toString();
                                PrefUtils.setCurrentUser(user, LoginActivity.this);
//                                Utils.showLog("callbackFB", object.getString("name").toString()
//                                        + " " + object.getString("email").toString());

                                ByFacebook(object.getString("id").toString(), object.getString("email").toString());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }

                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender");
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            Utils.showLog("callbackFB", "callbackFB cancel");
        }

        @Override
        public void onError(FacebookException e) {
            Utils.showLog("callbackFB", "callbackFB " + e.getMessage().toString());
        }
    };

    private void ByFacebook(String idFacebook, String email) {
        if (PrefUtils.getCurrentUser(this) != null) {
            if (email == null || email.equals("")) {
                LoginManager.getInstance().logInWithReadPermissions(
                        this,
                        Arrays.asList("email"));
            } else {
                loginPresenter.PostLoginUserSVC(email, "", idFacebook);
            }
        }
    }

    private void googleRespone(boolean isSignedIn, String email, String name, String strIdGoogle) {
        if (isSignedIn) {
            loginPresenter.PostLoginGoogleSVC(email, strIdGoogle);
        } else {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mGoogleApiClient.clearDefaultAccountAndReconnect();
        mGoogleApiClient.disconnect();

    }
}
