package id.nata.access360.view.menuBank.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.menuBank.Adapter.BankListAdapter;
import id.nata.access360.view.menuBank.Model.BankInformationData;
import id.nata.access360.view.menuBank.Model.BankInformationParam;
import id.nata.access360.view.menuBank.Model.BankListData;
import id.nata.access360.view.menuBank.Process.BankInterface;
import id.nata.access360.view.menuBank.Process.BankPresenter;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class InformasiBankActivity extends BaseActivity implements BankInterface{

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bank_spinner)
    Spinner bankSpinner;
    @BindView(R.id.nama_akun_edt)
    EditText accNameEditText;
    @BindView(R.id.nomor_akun_edt)
    EditText accNoEditText;
    @BindView(R.id.cabang_edt)
    EditText branchEditText;

    @Inject
    BankPresenter bankPresenter;


    private List<BankListData> bankListData2 = new ArrayList<BankListData>();
    private BankListAdapter bankListAdapter;

    private SessionManager sessionManager;
    private String psRef, username;
    private String psRefInfo, bankRefInfo, bankAccNoInfo, bankAccNameInfo, bankBranchInfo;
    private String bankRefSelected, bankNameSelected;
    
    String idBankRef, idBankName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi_bank);
        ((BaseApplication) getApplication()).getDeps().inject(this);
        ButterKnife.bind(this);
        bankPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.bankk));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getBankInfo();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        bankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idBankRef = bankListData2.get(position).getBankRef();
                idBankName = bankListData2.get(position).getBankName();
//                Toast.makeText(InformasiBankActivity.this, "Bank Ref : "+ idBankRef, Toast.LENGTH_SHORT).show();
//                Toast.makeText(InformasiBankActivity.this, "Bank Nama : "+ idBankName, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void getBankInfo() {
        bankPresenter.getBankInformationSVC(psRef, username);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(InformasiBankActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                break;
        }
        return true;
    }

    @OnClick(R.id.simpan_btn)
    public void btnSimpanPersonalBankClicked() {
        bankPresenter.POSTUpdatePersonalBank(psRef, username, idBankRef, accNoEditText.getText().toString(),
                accNameEditText.getText().toString(), branchEditText.getText().toString());
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessBankInfo(BankInformationData bankInformationData, String message) {
//        Utils.showSnackbar(layout, message);

        psRefInfo = bankInformationData.getPsRef();
        bankRefInfo = bankInformationData.getBankRef();
        bankAccNoInfo = bankInformationData.getBankAccNo();
        bankAccNameInfo = bankInformationData.getBankAccName();
        bankBranchInfo = bankInformationData.getBankBranch();

        accNameEditText.setText(bankAccNameInfo);
        accNoEditText.setText(bankAccNoInfo);
        branchEditText.setText(bankBranchInfo);

        //requestBankLookUp
        bankPresenter.getBankListSVC("");
    }

    @Override
    public void onFailedBankInfo(String message) {
        Utils.showSnackbar(layout, message);
    }

    @Override
    public void onSuccess(List<BankListData> bankListData) {
        bankListAdapter = new BankListAdapter(this, bankListData);
        bankSpinner.setAdapter(bankListAdapter);
        for (int i=0; i<bankListData.size(); i++){
            if (bankListData.get(i).getBankRef().equals(bankRefInfo)){
                bankSpinner.setSelection(i);
                break;
            }
        }
        bankListData2 = bankListData;
    }

    @Override
    public void onFailed(String string) {
        Utils.showSnackbar(layout, "Gagal load data");
    }


}
