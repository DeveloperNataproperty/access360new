package id.nata.access360.view.lead.model;

public class LeadList {

    private String leadRef;
    private String leadName;
    private String leadEmail;
    private String leadPhone;
    private String leadCity;
    private String statusColor;
    private String responseGroupRef;
    private String responseRef;
    private String responseName;
    private String noted;
    private String statusColorMessage;
    private String linkPK;
    private String linkPriceList;
    private String linkImage;

    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public String getLeadEmail() {
        return leadEmail;
    }

    public void setLeadEmail(String leadEmail) {
        this.leadEmail = leadEmail;
    }

    public String getLeadPhone() {
        return leadPhone;
    }

    public void setLeadPhone(String leadPhone) {
        this.leadPhone = leadPhone;
    }

    public String getLeadCity() {
        return leadCity;
    }

    public void setLeadCity(String leadCity) {
        this.leadCity = leadCity;
    }

    public String getStatusColor() {
        return statusColor;
    }

    public void setStatusColor(String statusColor) {
        this.statusColor = statusColor;
    }

    public String getResponseGroupRef() {
        return responseGroupRef;
    }

    public void setResponseGroupRef(String responseGroupRef) {
        this.responseGroupRef = responseGroupRef;
    }

    public String getResponseRef() {
        return responseRef;
    }

    public void setResponseRef(String responseRef) {
        this.responseRef = responseRef;
    }

    public String getResponseName() {
        return responseName;
    }

    public void setResponseName(String responseName) {
        this.responseName = responseName;
    }

    public String getNoted() {
        return noted;
    }

    public void setNoted(String noted) {
        this.noted = noted;
    }

    public String getStatusColorMessage() {
        return statusColorMessage;
    }

    public void setStatusColorMessage(String statusColorMessage) {
        this.statusColorMessage = statusColorMessage;
    }

    public String getLinkPK() {
        return linkPK;
    }

    public void setLinkPK(String linkPK) {
        this.linkPK = linkPK;
    }

    public String getLinkPriceList() {
        return linkPriceList;
    }

    public void setLinkPriceList(String linkPriceList) {
        this.linkPriceList = linkPriceList;
    }

    public String getLinkImage() {
        return linkImage;
    }

    public void setLinkImage(String linkImage) {
        this.linkImage = linkImage;
    }
}
