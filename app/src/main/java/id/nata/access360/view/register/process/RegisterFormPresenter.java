package id.nata.access360.view.register.process;

import android.content.Context;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.register.model.RegisterRespone;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public class RegisterFormPresenter implements BasePresenter<RegisterFormViewInterface> {

    @Inject
    NetworkService networkService;

    private RegisterFormViewInterface view;
    private Context context;

    public RegisterFormPresenter(Context context) {
        super();
        ((BaseApplication) context).getDeps().inject(this);
        this.context = context;
    }

    public void PostRegisterUserSVC(String jsonIn){
        view.showLoader();
        Call<RegisterRespone> callLogin = networkService.PostRegisterUserSVC(jsonIn);
        callLogin.enqueue(new Callback<RegisterRespone>() {
            @Override
            public void onResponse(Call<RegisterRespone> call, Response<RegisterRespone> response) {
                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.onSuccesss(response.body().getData() ,response.body().getMessage());
                    } else if (response.body().getStatus()== General.API_REQUEST_FAILED){
                        view.onFailedFromService(response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_ACCOUNT_NOT_ACTIVE){
                        view.onAlreadyExist(response.body().getMessage());
                    }
                    else {
                        view.onFailed(response.body().getMessage());
                    }
                }else{
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<RegisterRespone> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(RegisterFormViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
