package id.nata.access360.view.register.ui;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Html;
import android.text.InputType;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.FixedHoloDatePickerDialog;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.register.model.RegisterData;
import id.nata.access360.view.register.model.RegisterParam;
import id.nata.access360.view.register.process.RegisterFormPresenter;
import id.nata.access360.view.register.process.RegisterFormViewInterface;

import static id.nata.access360.config.General.EMAIL;
import static id.nata.access360.config.General.FULLNAME;
import static id.nata.access360.config.General.ID_FACEBOOK;
import static id.nata.access360.config.General.ID_GOOGLE;

/**
 * Created by "dwist14"
 * on Sep 9/6/2017 18:22.
 * Project : Napro
 */
public class RegisterFormActivity extends AppCompatActivity implements RegisterFormViewInterface,
        GoogleApiClient.OnConnectionFailedListener, AdapterView.OnItemSelectedListener {
    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.edt_kode_sponsor)
    EditText edtKodeSponsor;
    @BindView(R.id.edt_kodesponsor_layout)
    TextInputLayout edtKodeSponsorLayout;

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_name_layout)
    TextInputLayout edtNameLayout;

    @BindView(R.id.edt_birthdate)
    EditText edtBirthdate;
    @BindView(R.id.edt_birthdate_layout)
    TextInputLayout edtBirthdateLayout;

    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_email_layout)
    TextInputLayout edtEmailLayout;
    @BindView(R.id.edt_phone)
    EditText edtPhone;
    @BindView(R.id.edt_phone_layout)
    TextInputLayout edtPhoneLayout;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.edt_password_layout)
    TextInputLayout edtPasswordLayout;
    @BindView(R.id.edt_retype_password)
    EditText edtRetypePassword;
    @BindView(R.id.edt_retype_password_layout)
    TextInputLayout edtRetypePasswordLayout;
    @BindView(R.id.relative_layout)
    RelativeLayout relativeLayout;
    @BindView(R.id.btn_register)
    Button btnRegister;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.check_box)
    CheckBox checkBox;
    @BindView(R.id.txt_term_n_condition)
    TextView txtTermNCondition;
    @BindView(R.id.spinner)
    Spinner spinner;

    @Inject
    RegisterFormPresenter registerPresenter;

    String personalType, kdSponsor, personalName, personalHp, birthDate, username, password, reTypePassword;
    String strPsRef, strUsername, strPsName, strPsCode;
    String idGoogle, idFacebook;

    SessionManager sessionManager;

    GoogleApiClient mGoogleApiClient;

    private Pattern pattern;
    private Matcher matcher;

    private static final String PASSWORD_PATTERN =
            "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_?/.,]).{8,20})";

    boolean validatePassword = true;
    boolean valid = true;

    Calendar myCalendar = Calendar.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        ((BaseApplication) getApplication()).getDeps().inject(this);
        ButterKnife.bind(this);

        initAdapter();

        registerPresenter.setupView(this);

        sessionManager = new SessionManager(this);
        pattern = Pattern.compile(PASSWORD_PATTERN);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestIdToken(General.CLIENT_WEB)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

//        Intent i = getIntent();
//        email = i.getStringExtra(EMAIL);
//        fullname = i.getStringExtra(FULLNAME);
//        idFacebook = i.getStringExtra(ID_FACEBOOK);
//        idGoogle = i.getStringExtra(ID_GOOGLE);

        Utils.showLog("TAG", "idGoogle : " + idGoogle + "  idFacebook : " + idFacebook);

        edtBirthdate.setInputType(InputType.TYPE_NULL);
        edtBirthdate.setTextIsSelectable(true);
        edtBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogPicker();
            }
        });
        edtBirthdate.setFocusable(false);

//        if (email != null) {
//            edtEmail.setText(email);
//            edtEmail.setEnabled(false);
//            String randomPass = random();
//            edtPassword.setText(randomPass);
//            edtRetypePassword.setText(randomPass);
//
//            validatePassword = true;
//
//            edtPasswordLayout.setVisibility(View.GONE);
//            edtRetypePasswordLayout.setVisibility(View.GONE);
//        }
//
//        if (fullname != null)
//            edtName.setText(fullname);

        checkBox.setText("");
        txtTermNCondition.setText(Html.fromHtml(getString(R.string.q_term_n_condition) + " " +
                "<a href='" + General.LINK_TERM_AND_CONDITION + "://Kode'>" + getString(R.string.usage_rules) + "</a>"));
        txtTermNCondition.setClickable(true);
        txtTermNCondition.setMovementMethod(LinkMovementMethod.getInstance());

//        edtPasswordLayout.setError(getString(R.string.password_less_then_8_char_and_combinate));

    }

    private void initAdapter() {
        spinner.setOnItemSelectedListener(this);

        List<String> tipe_regist = new ArrayList<String>();
        tipe_regist.add("-- Pilih Tipe --");
        tipe_regist.add("Perorangan");
        tipe_regist.add("PT");

        ArrayAdapter<String> tipeAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tipe_regist);

        tipeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(tipeAdapter);
    }

    private void dialogPicker() {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        @SuppressLint("RestrictedApi") final Context themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog);
        final DatePickerDialog dialog = new FixedHoloDatePickerDialog(
                themedContext,
                date,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        dialog.show();
    }

    private void updateLabel(){
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        edtBirthdate.setText(sdf.format(myCalendar.getTime()));
    }

    //button daftar
    @OnClick(R.id.btn_register)
    public void btnRegisterOnClick() {
        Utils.hideSoftKeyboard(this);

//            Intent i = new Intent(getApplicationContext(), RegisterValidasiActivity.class);
//            startActivity(i);

        kdSponsor = edtKodeSponsor.getText().toString();
        personalName = edtName.getText().toString().trim();
        birthDate = edtBirthdate.getText().toString().trim();
        personalHp = edtPhone.getText().toString().trim();
        username = edtEmail.getText().toString().trim();
        password = edtPassword.getText().toString().trim();
        reTypePassword = edtRetypePassword.getText().toString().trim();

        //Utils.showLog("TAG", "post " + String.valueOf(validateInputData()));

        if (personalType.equals("0")){
            Utils.showSnackbar(layout, "Pilih tipe");
        } else if (personalName.equals("")){
            edtNameLayout.setError(getString(R.string.must_be_field));
            if (!personalName.equals("")){
                edtNameLayout.setError(null);
            }
        } else if (birthDate.equals("")){
            edtBirthdateLayout.setError(getString(R.string.must_be_field));
            if (!birthDate.equals("")){
                edtBirthdateLayout.setError(null);
            }
        } else if (personalHp.equals("")){
            edtPhoneLayout.setError(getString(R.string.must_be_field));
            if (!personalHp.equals("")){
                edtPhoneLayout.setError(null);
            }
        } else if (username.equals("")){
            edtEmailLayout.setError(getString(R.string.must_be_field));
            if (!username.equals("")){
                edtEmailLayout.setError(null);
            }
        } else if (password.equals("")) {
            edtPasswordLayout.setError(getString(R.string.must_be_field));
        } else if (reTypePassword.equals("")) {
            edtRetypePasswordLayout.setError(getString(R.string.must_be_field));
        } else if (!password.equals(reTypePassword)) {
            edtRetypePasswordLayout.setError(getString(R.string.must_be_same));
        } else {
            edtNameLayout.setError(null);
            edtBirthdateLayout.setError(null);
            edtPhoneLayout.setError(null);
            edtEmailLayout.setError(null);
            edtPasswordLayout.setError(null);
            edtRetypePasswordLayout.setError(null);

            RegisterParam param = new RegisterParam();
            param.setPersonalType(personalType);
            param.setPersonalName(personalName);
            param.setBirthDate(birthDate);
            param.setPersonalHp(personalHp);
            param.setUsername(username);
            param.setPassword(password);
            param.setKodeSponsor(kdSponsor);
            Utils.showLog("TAG", "post to service");
            registerPresenter.PostRegisterUserSVC(Utils.modelToJson(param));
        }

    }

//    private boolean validateInputData() {
//
//        if (personalName.equals("")) {
//            edtNameLayout.setError(getString(R.string.must_be_field));
//            valid = false;
//        } else {
//            edtNameLayout.setError(null);
//        }
//
//        if (username.equals("")) {
//            edtEmailLayout.setError(getString(R.string.must_be_field));
//            valid = false;
//        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(username).matches()) {
//            edtEmailLayout.setError(getString(R.string.email_not_valid));
//            valid = false;
//        } else {
//            edtEmailLayout.setError(null);
//        }
//
//        if (personalHp.equals("")) {
//            edtPhoneLayout.setError(getString(R.string.must_be_field));
//            valid = false;
//        } else {
//            edtPhoneLayout.setError(null);
//        }
//
//        // jika daftar pake google / fb validate pass true
//        if (validatePassword) {
//            if (password.equals("")) {
//                edtPasswordLayout.setError(getString(R.string.must_be_field));
//                valid = false;
//            } else {
//                if (validatePassword(password)) {
//                    edtPasswordLayout.setError(null);
//                } else {
//                    edtPasswordLayout.setError(getResources().getString(R.string.password_less_then_8_char_and_combinate));
//                    valid = false;
//                }
//            }
//        } else {
//            valid = true;
//        }
//
//        // jika daftar pake google / fb validate pass true
//        if (validatePassword) {
//            if (reTypePassword.equals("")) {
//                edtRetypePasswordLayout.setError(getString(R.string.must_be_field));
//                valid = false;
//            } else if (!password.equals(reTypePassword)) {
//                edtRetypePasswordLayout.setError(getString(R.string.must_be_same));
//                valid = false;
//            } else {
//                edtRetypePasswordLayout.setError(null);
//                if (validatePassword(password)) {
//                    edtPasswordLayout.setError(null);
//                } else {
//                    edtPasswordLayout.setError(getResources().getString(R.string.password_less_then_8_char_and_combinate));
//                    valid = false;
//                }
//            }
//        } else {
//            valid = true;
//        }
//
//        if (!checkBox.isChecked()) {
//            Utils.showSnackbar(layout, getString(R.string.not_check_term_condition));
//            valid = false;
//        } else {
//            valid = true;
//        }
//
//        return valid;
//    }

    @Override
    public void showLoader() {
        btnRegister.setText("");
        btnRegister.setEnabled(false);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissLoader() {
        btnRegister.setText(getString(R.string.Register));
        btnRegister.setEnabled(true);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onNetworkError(String appErrorMessage) {

    }

    @Override
    public void onSuccesss(RegisterData data, String message) {
        Utils.showSnackbar(relativeLayout, message);
//        if (data!=null){
            strPsRef = data.getPsRef();
            strUsername = data.getUsername();
            strPsName = data.getPsName();
            strPsCode = data.getPsCode();
            //sessionManager.setStringToSP(General.USER_REF, userRef);
//            sessionManager.setNeedValidasi(true);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    Intent i = new Intent(getApplicationContext(), RegisterValidasiActivity.class);
//                    startActivity(i);
//                    finish();
//                }
//            }, 3000);
            Intent intent = new Intent(this, RegisterValidasiActivity.class);
            intent.putExtra("psRef", strPsRef);
            intent.putExtra("username", strUsername);
            startActivity(intent);
//        }
    }

    @Override
    public void onFailedFromService(String message) {
        Utils.showSnackbar(relativeLayout, message);
    }

    @Override
    public void onAlreadyExist(String message) {
        Utils.showSnackbar(relativeLayout, message);
    }

    @Override
    public void onFailed(String paramStrErrorMessage) {
        Utils.showSnackbar(relativeLayout, paramStrErrorMessage);
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mGoogleApiClient.clearDefaultAccountAndReconnect();
        mGoogleApiClient.disconnect();

    }

    /**
     * Validate password with regular expression
     *
     * @param password password for validation
     * @return true valid password, false invalid password
     */
    public boolean validatePassword(final String password) {
        matcher = pattern.matcher(password);
        Utils.showLog("validatePassword pass", "is matcher " + matcher.matches());
        return matcher.matches();

    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        char tempChar;
        for (int i = 0; i < 8; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (i){
            case 0:
                personalType="0";
                break;
            case 1:
                personalType="1";
                break;
            case 2:
                personalType="2";
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
