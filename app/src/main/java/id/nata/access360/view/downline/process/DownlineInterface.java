package id.nata.access360.view.downline.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.downline.model.DownlineList;

public interface DownlineInterface extends BaseInterface {

    void onSuccesGetDownline(List<DownlineList> downlineList);
    void onEmptyGetDownline(String string);
}
