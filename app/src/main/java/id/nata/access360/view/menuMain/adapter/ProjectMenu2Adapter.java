package id.nata.access360.view.menuMain.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.List;

import id.nata.access360.R;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.view.downline.ui.DownlineActivity;
import id.nata.access360.view.hubungiKami.ui.HubungiKamiActivity;
import id.nata.access360.view.lead.ui.LeadActivity;
import id.nata.access360.view.lead.ui.LeadProjectActivity;
import id.nata.access360.view.menuBank.ui.InformasiBankActivity;
import id.nata.access360.view.menuMain.model.ProjectMenu2Model;
import id.nata.access360.view.menuProfile.ui.ProfileACActivity;
//import id.nata.access360.view.menuProfile.ui.ProfileActivity;
import id.nata.access360.view.nup.ui.NupActivity;

/**
 * Created by NATA on 2/8/19.
 */

public class ProjectMenu2Adapter extends RecyclerView.Adapter<ProjectMenu2Adapter.MainMenuHolder> {
//    public static final String TAG = "ProjectMenuAdapter";
    private List<ProjectMenu2Model> listMenu;
    public Context context;
    private Display display;
    SessionManager mSessionManager;
    String urlViewProject;

    public ProjectMenu2Adapter(Context context, List<ProjectMenu2Model> listMenu) {
        this.context = context;
        this.listMenu = listMenu;
    }

    @Override
    public MainMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.grid_layout_for_menu_360, null);

        return new MainMenuHolder(view);
    }

    @Override
    public void onBindViewHolder(MainMenuHolder holder, int position) {
        holder.onBindHolder(listMenu.get(position), position);
    }

    @Override
    public int getItemCount() {
        return listMenu.size();
    }

    class MainMenuHolder extends RecyclerView.ViewHolder {

        ImageView menuIcon;
        TextView menuTitle;
        RelativeLayout menuLayout;
        LinearLayout item;

        public MainMenuHolder(View itemView) {
            super(itemView);
            menuIcon = (ImageView) itemView.findViewById(R.id.btn_main_menu);
            menuTitle = (TextView) itemView.findViewById(R.id.btn_main_menu_title);
            menuLayout = (RelativeLayout) itemView.findViewById(R.id.main_menu_layout_item);
            item = (LinearLayout) itemView.findViewById(R.id.item);
        }

        public void onBindHolder(final ProjectMenu2Model projectMenu2Model, final int position) {
//            Point size = new Point();
//            display.getSize(size);
//            width = size.x;
//            result = width / 1.233333333333333;
//            widthButton = size.x / 2.5;
//            heightButton = widthButton / 1.5;

            mSessionManager = new SessionManager(context);
            urlViewProject = mSessionManager.getStringFromSP(General.URL_VP); // ((ProjectMenu2Activity)context).urlViewProject;

//            ViewGroup.LayoutParams paramsInfo = menuIcon.getLayoutParams();
//            paramsInfo.height = heightButton.intValue();
//            menuIcon.setLayoutParams(paramsInfo);
//            menuIcon.requestLayout();

            menuIcon.setBackground(context.getResources().getDrawable(projectMenu2Model.getMenuIcon()));
            menuTitle.setText(projectMenu2Model.getMenuName());
            menuIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (projectMenu2Model.getMenuTitle()) {
                        case "menuProfile":
                            Intent intent = new Intent(context, ProfileACActivity.class);
                            context.startActivity(intent);
                            break;
                        case "menuBank":
                            Intent intent2 = new Intent(context, InformasiBankActivity.class);
                            context.startActivity(intent2);
                            break;
                        case "menuVp":
                            Uri uri = Uri.parse(urlViewProject); // missing 'http://' will cause crashed
                            Intent intent3 = new Intent(Intent.ACTION_VIEW, uri);
                            context.startActivity(intent3);
                            break;
                        case "menuNup":
                            Intent intent4 = new Intent(context, NupActivity.class);
                            context.startActivity(intent4);
                            break;
                        case "menuDownline":
                            Intent intent5 = new Intent(context, DownlineActivity.class);
                            context.startActivity(intent5);
                            break;
                        case "menuLead":
                            Intent intent6 = new Intent(context, LeadProjectActivity.class);
                            context.startActivity(intent6);
                            break;
                        case "hubungiKami":
                            Intent intent7 = new Intent(context, HubungiKamiActivity.class);
                            context.startActivity(intent7);
                            break;
                    }
                }
            });

            //Untuk klik cardview-nya
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch (projectMenu2Model.getMenuTitle()) {
                        case "menuProfile":
                            Intent intent = new Intent(context, ProfileACActivity.class);
                            context.startActivity(intent);
                            break;
                        case "menuBank":
                            Intent intent2 = new Intent(context, InformasiBankActivity.class);
                            context.startActivity(intent2);
                            break;
                        case "menuVp":
                            Uri uri = Uri.parse(urlViewProject); // missing 'http://' will cause crashed
                            Intent intent3 = new Intent(Intent.ACTION_VIEW, uri);
                            context.startActivity(intent3);
                            break;
                        case "menuNup":
                            Intent intent4 = new Intent(context, NupActivity.class);
                            context.startActivity(intent4);
                            break;
                        case "menuDownline":
                            Intent intent5 = new Intent(context, DownlineActivity.class);
                            context.startActivity(intent5);
                            break;
                        case "menuLead":
                            Intent intent6 = new Intent(context, LeadActivity.class);
                            context.startActivity(intent6);
                            break;
                        case "hubungiKami":
                            Intent intent7 = new Intent(context, HubungiKamiActivity.class);
                            context.startActivity(intent7);
                            break;
                    }
                }
            });
        }
    }
}
