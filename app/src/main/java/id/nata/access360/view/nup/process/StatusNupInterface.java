package id.nata.access360.view.nup.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.nup.model.StatusNup;

/**
 * Created by NATA on 2/13/19.
 */

public interface StatusNupInterface extends BaseInterface {
    void onSuccessGetNup(List<StatusNup> statusNup);

    void onFailedGetNup(String string);

}
