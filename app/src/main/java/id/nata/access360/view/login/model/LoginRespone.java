package id.nata.access360.view.login.model;

/**
 * Created by "dwist14"
 * on Sep 9/15/2017 13:56.
 * Project : Napro
 */
public class LoginRespone {
    int status ;
    String message;
    LoginData dataLogin;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginData getDataLogin() {
        return dataLogin;
    }

    public void setDataLogin(LoginData dataLogin) {
        this.dataLogin = dataLogin;
    }
}
