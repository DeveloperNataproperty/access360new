package id.nata.access360.view.nup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.nup.model.StatusNup;

/**
 * Created by NATA on 2/13/19.
 */

public class NupAdapter extends BaseAdapter {

    private Context context;
    private List<StatusNup> list;
    private ListNupHolder holder;
    private String statusRef;

    public NupAdapter(Context context, List<StatusNup> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_mynup,null);
            holder = new ListNupHolder();
            holder.statusNameTxt = (TextView) convertView.findViewById(R.id.txt_status_name);
            holder.totalNupTxt = (TextView) convertView.findViewById(R.id.txt_total_nup);

            convertView.setTag(holder);
        }else{
            holder = (ListNupHolder) convertView.getTag();
        }

        StatusNup statusNup = list.get(pos);
        statusRef = statusNup.getStatusRef();
        holder.statusNameTxt.setText(statusNup.getStatusName());
        holder.totalNupTxt.setText(statusNup.getTotalNUP());

        return convertView;
    }

    private class ListNupHolder {
        TextView statusNameTxt, totalNupTxt;
    }
}
