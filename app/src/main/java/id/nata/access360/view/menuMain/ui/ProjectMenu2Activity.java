package id.nata.access360.view.menuMain.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.messaging.FirebaseMessaging;
import com.jaredrummler.android.device.DeviceName;
import com.tapadoo.alerter.Alerter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.GridSpacingItemDecoration;
import id.nata.access360.helper.Utils;
import id.nata.access360.helper.UtilsSnackbar;
import id.nata.access360.view.lead.ui.LeadActivity;
import id.nata.access360.view.menuMain.adapter.ProjectMenu2Adapter;
import id.nata.access360.view.menuMain.model.DataMenu;
import id.nata.access360.view.menuMain.model.ProjectMenu2Model;
import id.nata.access360.view.menuMain.process.MenuMainPresenter;
import id.nata.access360.view.menuMain.process.MenuMainViewInterface;
import id.nata.access360.view.menuProfile.ui.EditProfileImageActivity;

import static id.nata.access360.config.General.FCM_TOKEN;

public class ProjectMenu2Activity extends BaseActivity implements MenuMainViewInterface {

    private static final String TAG = ProjectMenu2Activity.class.getSimpleName();

    private String urlPhoto;
    private Display display;
    private Double result;
    private ProjectMenu2Adapter projectMenuAdapter;
    private RecyclerView recyclerViewMenu;
    private NestedScrollView nestedScrollView;
    private TextView title;
    Uri file, uri, selectedImage;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;

//    @BindView(R.id.toolbar)
//    Toolbar toolbar;
    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.profilName)
    TextView profilNameTxt;
    @BindView(R.id.image_profile)
    CircleImageView imageProfile;

    public String strPsRef, strFullname, strEmail, strPsCode, urlViewProject;
    private  String type, manufacturer, mTmpGalleryPicturePath;

    @Inject
    MenuMainPresenter menuMainPresenter;

    SessionManager mSessionManager;

    //fcm
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private String fcmToken;

    int menuIcon[] = new int[]{
            R.drawable.profil_btn,
            R.drawable.lead_1,
            R.drawable.nup_btn,
            R.drawable.vp_btn,
            R.drawable.downline_btn,
            R.drawable.bank_btn,

            //icon sementara hubungi kami
            R.drawable.contactus_btn
    };

    // list title menu
    String menuTitle[] = new String[]{
            "menuProfile",
            "menuLead",
            "menuNup",
            "menuVp",
            "menuDownline",
            "menuBank",
            "hubungiKami"
    };

    String menuName[]  = new String[]{
            "Profile",
            "Lead",
            "NUP",
            "Cari Project",
            "Downline",
            "Bank",
            "Hubungi Kami"
    };

    Integer flagg = 0;

    private List<ProjectMenu2Model> menulist = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_menu2);
        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        title = (TextView) toolbar.findViewById(R.id.title);

        menuMainPresenter = new MenuMainPresenter(this);
        menuMainPresenter.setupView(this);

        mSessionManager = new SessionManager(getApplicationContext());

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setIcon(R.drawable.ic_launcher_2);
        title.setText("Access360");
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        strPsRef = mSessionManager.getStringFromSP(General.PS_REF);
        strEmail = mSessionManager.getStringFromSP(General.EMAIL);
        strFullname = mSessionManager.getStringFromSP(General.FULLNAME);

        profilNameTxt.setText(strFullname);

        recyclerViewMenu = (RecyclerView) findViewById(R.id.listMenu);

//        initAdapter();
        if (flagg == 0) {
            getMenuInformation();
        }

        //push notifcation
        registrationBroadcastReceiver();
        SharedPreferences pref = getApplicationContext().getSharedPreferences(General.SHARED_PREF, 0);
        fcmToken = pref.getString(FCM_TOKEN, null);
        Utils.showLog(TAG, "FCM_TOKEN " + fcmToken);
        if (fcmToken != null) {
            menuMainPresenter.PostFCMTokenSVC(strPsRef, fcmToken);
        } else {
            Utils.showToast(this, "Token not registered");
        }

        //handle error upload image
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        StrictMode.VmPolicy.Builder newbuilder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(newbuilder.build());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (flagg == 1) {
            getMenuInformation();
        }
    }

    @OnClick(R.id.image_profile)
    public void onCLickImageProfile(){
        type = "1";
        selectImage();
    }


    private void getMenuInformation() {
        menuMainPresenter.GET_MenuInformation(strPsRef, strEmail);
    }

    @Override
    public void onSuccess(DataMenu dataMenu) {
        urlViewProject = dataMenu.getUrlViewProject();
        urlPhoto = dataMenu.getUrlPhoto();
        mSessionManager.urlViewProjectSession(urlViewProject);

        if (urlPhoto.equals("")){
            Glide.with(ProjectMenu2Activity.this).load(R.drawable.profile_image).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imageProfile);
        } else {
            Glide.with(ProjectMenu2Activity.this).load(urlPhoto).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true).into(imageProfile);
        }

        if (flagg == 0) {
            initAdapter();
        }

        flagg = 1;
    }

    private void initAdapter() {
        projectMenuAdapter = new ProjectMenu2Adapter(this, menulist);
        recyclerViewMenu.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerViewMenu.setHasFixedSize(true);
        recyclerViewMenu.setAdapter(projectMenuAdapter);
//        recyclerViewMenu.setNestedScrollingEnabled(false);

//        projectMenuAdapter = new ProjectMenu2Adapter(this, menulist);
//        recyclerViewMenu.setLayoutManager(new GridLayoutManager(this, 2));
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
//        recyclerViewMenu.setLayoutManager(mLayoutManager);
//        recyclerViewMenu.setHasFixedSize(true);
//        recyclerViewMenu.setAdapter(projectMenuAdapter);

        for (int i = 0; i < menuIcon.length; i++) {
            ProjectMenu2Model projectMenuModel = new ProjectMenu2Model();
            projectMenuModel.setMenuIcon(menuIcon[i]);
            projectMenuModel.setMenuTitle(menuTitle[i]);
            projectMenuModel.setMenuName(menuName[i]);
            menulist.add(projectMenuModel);
        }
        projectMenuAdapter.notifyDataSetChanged();
//        recyclerViewMenu.addItemDecoration(new GridItemDivider(numColumns, Utils.dpToPx(this, 10), false));
        int spacingInPixels = getResources().getDimensionPixelSize(R.dimen.dimen16);
        recyclerViewMenu.addItemDecoration(new GridSpacingItemDecoration(2, spacingInPixels, true, 0));
    }

    @Override
    public void onSuccessRegisterFCM(String paramStrSuccessrMessage) {
        Utils.showLog(TAG, "onSuccessRegisterFCM " + paramStrSuccessrMessage);
    }

    @Override
    public void onFailed(String paramStrErrorMessage) {
        UtilsSnackbar.showSnakbarTypeOne(layout, paramStrErrorMessage);
    }

    private void registrationBroadcastReceiver() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // checking for type intent filter
                if (intent.getAction().equals(General.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(General.TOPIC_GLOBAL);
                    String regId = mSessionManager.getStringFromSP(FCM_TOKEN);

                } else if (intent.getAction().equals(General.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    showAlerter(message);
//                    homePresenter.GetNotificationListSVC();
                }
            }
        };
    }

    private void showAlerter(String message) {
        Alerter.create(ProjectMenu2Activity.this)
                .setTitle(getString(R.string.app_name))
                .setText(message)
                .setIcon(getResources().getDrawable(R.drawable.ic_stat_notification))
                .setDuration(3000)
                .setBackgroundColorInt(getResources().getColor(R.color.color_alerter))
                .enableSwipeToDismiss()
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivityForResult(new Intent(getApplicationContext(), LeadActivity.class), 2);
                    }
                })
                .show();
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        UtilsSnackbar.showSnakbarTypeThree(layout, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_after_login, menu);
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.btn_logout:
                dialogLogout();
                return true;

            case R.id.btn_exit:
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void dialogLogout() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.question_logout))
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        mSessionManager.logoutUser();

                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // user doesn't want to logout
                    }
                })
                .show();
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Use Existing Foto"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(ProjectMenu2Activity.this);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    if (checkCameraPermission()) {
                        Toast.makeText(ProjectMenu2Activity.this, "Enable camera permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(ProjectMenu2Activity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA);
                    } else {
                        openIntentCamera();
                    }

                } else if (items[item].equals("Use Existing Foto")) {
                    if (checkWriteExternalPermission()) {
                        openIntentFile();
                        /*Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image*//*");
                        intent.putExtra("type", type);
                        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                        startActivityForResult(intent, SELECT_FILE);*/
                    } else {
                        ActivityCompat.requestPermissions(ProjectMenu2Activity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, SELECT_FILE);
                    }


                }
            }
        });
        builder.show();

    }

    public boolean checkCameraPermission() {
        return ContextCompat.checkSelfPermission(ProjectMenu2Activity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED;
    }

    public boolean checkWriteExternalPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        int res = ProjectMenu2Activity.this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    public void showDialogCekPermission() {
        if (ContextCompat.checkSelfPermission(ProjectMenu2Activity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(EditProfileActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(ProjectMenu2Activity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    EXTERNAL_STORAGE_PERMISSION_CONSTANT);
//            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == SELECT_FILE) {
                openIntentFile();
            } else if (requestCode == REQUEST_CAMERA) {
                openIntentCamera();
            }
        } else {
            proceedAfterPermissionDeny();
        }
    }

    private void proceedAfterPermissionDeny() {
        Toast.makeText(getBaseContext(), "We don't have the Storage Permission", Toast.LENGTH_LONG).show();
    }

    private void openIntentFile() {
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void openIntentCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "nataproperty");

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("picUri", file);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        file = savedInstanceState.getParcelable("picUri");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                //onSelectFromGalleryResult(data);
                if (Build.VERSION.SDK_INT < 19) {
                    handleGalleryResult18(data);
                } else {
                    handleGalleryResult19(data);
                }

            else if (requestCode == REQUEST_CAMERA) {
                if (type != null && type.equals("1")) {
                    Intent i = new Intent(this, EditProfileImageActivity.class);
                    i.putExtra("pathImage", file.getEncodedPath());
                    //i.putExtra("Image", byteArray);
                    startActivity(i);
                } else {
                    Toast.makeText(ProjectMenu2Activity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }
            }
            //onCaptureImageResult(data);

        }
    }

    private void handleGalleryResult19(Intent data) {
        uri = data.getData();
        //mTmpGalleryPicturePath = getPath(selectedImage);

        DeviceName.with(this).request(new DeviceName.Callback() {
            @Override
            public void onFinished(DeviceName.DeviceInfo info, Exception error) {
                manufacturer = info.manufacturer;

                mTmpGalleryPicturePath = getRealPathFromURI(ProjectMenu2Activity.this, uri);
                if (mTmpGalleryPicturePath == null) {
                    mTmpGalleryPicturePath = getPath(selectedImage);
                }

                Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                if (!mTmpGalleryPicturePath.equals("")) {
                    if (mTmpGalleryPicturePath != null) {
                        if (type != null && type.equals("1")) {
                            Intent i = new Intent(ProjectMenu2Activity.this, EditProfileImageActivity.class);
                            i.putExtra("pathImage", mTmpGalleryPicturePath);
                            startActivity(i);
                        } else {
                            Toast.makeText(ProjectMenu2Activity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        try {
                            InputStream is = getContentResolver().openInputStream(selectedImage);
                            //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                            mTmpGalleryPicturePath = selectedImage.getPath();
                            Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                            if (type != null && type.equals("1")) {
                                Intent i = new Intent(ProjectMenu2Activity.this, EditProfileImageActivity.class);
                                i.putExtra("pathImage", mTmpGalleryPicturePath);
                                startActivity(i);
                            } else {
                                Toast.makeText(ProjectMenu2Activity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(ProjectMenu2Activity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

//    public static Uri getImageContentUri(Context context, File imageFile) {
//        String filePath = imageFile.getAbsolutePath();
//        Cursor cursor = context.getContentResolver().query(
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                new String[]{MediaStore.Images.Media._ID},
//                MediaStore.Images.Media.DATA + "=? ",
//                new String[]{filePath}, null);
//
//        if (cursor != null && cursor.moveToFirst()) {
//            int id = cursor.getInt(cursor
//                    .getColumnIndex(MediaStore.MediaColumns._ID));
//            Uri baseUri = Uri.parse("content://media/external/images/media");
//            return Uri.withAppendedPath(baseUri, "" + id);
//        } else {
//            if (imageFile.exists()) {
//                ContentValues values = new ContentValues();
//                values.put(MediaStore.Images.Media.DATA, filePath);
//                return context.getContentResolver().insert(
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
//            } else {
//                return null;
//            }
//        }
//    }

    private void handleGalleryResult18(Intent data) {
        String mTmpGalleryPicturePath;
        Uri selectedImage = data.getData();
        mTmpGalleryPicturePath = getRealPathFromURI_API11to18(this, selectedImage);
        Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);

        if (!mTmpGalleryPicturePath.equals("")) {
            if (mTmpGalleryPicturePath != null) {
                if (type != null && type.equals("1")) {
                    Intent i = new Intent(this, EditProfileImageActivity.class);
                    i.putExtra("pathImage", mTmpGalleryPicturePath);
                    startActivity(i);
                } else {
                    Toast.makeText(ProjectMenu2Activity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                }

            } else {
                try {
                    InputStream is = getContentResolver().openInputStream(selectedImage);
                    //mImageView.setImageBitmap(BitmapFactory.decodeStream(is));
                    mTmpGalleryPicturePath = selectedImage.getPath();
                    Log.d(TAG, "mTmpGalleryPicturePath " + mTmpGalleryPicturePath);
                    if (type != null && type.equals("1")) {
                        Intent i = new Intent(this, EditProfileImageActivity.class);
                        i.putExtra("pathImage", mTmpGalleryPicturePath);
                        startActivity(i);
                    } else {
                        Toast.makeText(ProjectMenu2Activity.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                }
            }

        } else {
            Toast.makeText(ProjectMenu2Activity.this, "Upload failed", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private String getPath(Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);
            // Split at colon, use second item in the array
            String id = wholeID.indexOf(":") > -1 ? wholeID.split(":")[1] : wholeID.indexOf(";") > -1 ? wholeID
                    .split(";")[1] : wholeID;
            String[] column = {MediaStore.Images.Media.DATA};
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column,
                    sel, new String[]{id}, null);
            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = "";
        }
        return filePath;

    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }
        return result;
    }
}


