package id.nata.access360.view.register.process;


import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.login.model.LoginData;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface RegisterOptionViewInterface extends BaseInterface {

    void onFailed(String paramStrErrorMessage);

    void onSuccessAccountNotActive(LoginData data);

    void onSuccessAccountNotRegister(LoginData data);

    void onSuccessLoginUser(LoginData data);
}
