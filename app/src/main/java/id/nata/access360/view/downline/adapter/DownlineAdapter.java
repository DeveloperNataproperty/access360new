package id.nata.access360.view.downline.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.downline.model.ChildList;
import id.nata.access360.view.downline.model.DownlineList;

public class DownlineAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<DownlineList> downlineList;
    private DownlineAdapterHolder holder;
    private DownlineAdapterHolder subHolder;

    public DownlineAdapter(Context context, List<DownlineList> downlineList) {
        this.context = context;
        this.downlineList = downlineList;
    }

    @Override
    public int getGroupCount() {
        return downlineList.size();
    }

    @Override
    public int getChildrenCount(int groupPos) {
        List<ChildList> childList = downlineList.get(groupPos).getChildList();
        return childList.size();
    }

    @Override
    public Object getGroup(int groupPos) {
        return downlineList.get(groupPos);
    }

    @Override
    public Object getChild(int groupPos, int childPos) {
        List<ChildList> childList = downlineList.get(groupPos).getChildList();
        return childList.get(childPos);
    }

    @Override
    public long getGroupId(int groupPos) {
        return groupPos;
    }

    @Override
    public long getChildId(int groupPos, int childPos) {
        return childPos;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPos, boolean isLastChild, View view, ViewGroup parent) {

        final DownlineList downlineLists = (DownlineList) getGroup(groupPos);
        if (view==null){
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inf.inflate(R.layout.item_downline, null);
            holder = new DownlineAdapterHolder();
            holder.levelText = view.findViewById(R.id.txt_level);
            holder.nameText = view.findViewById(R.id.txt_name);
            holder.hpText = view.findViewById(R.id.txt_hp);
            holder.emailText = view.findViewById(R.id.txt_email);
            view.setTag(holder);
        } else {
            holder = (DownlineAdapterHolder) view.getTag();
        }

        holder.levelText.setText(downlineLists.getLevel());
        holder.nameText.setText(downlineLists.getPsName());
        holder.hpText.setText(downlineLists.getHandphone());
        holder.emailText.setText(downlineLists.getEmail());

        return view;
    }

    @Override
    public View getChildView(int groupPos, int childPos, boolean isLastChild, View view, ViewGroup parent) {

        final ChildList childLists = (ChildList) getChild(groupPos, childPos);
        if (view==null){
            LayoutInflater infalInflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = infalInflater.inflate(R.layout.item_sub_downline, null);
            subHolder = new DownlineAdapterHolder();
            subHolder.levelText = view.findViewById(R.id.txt_sublevel);
            subHolder.nameText = view.findViewById(R.id.txt_subname);
            subHolder.hpText = view.findViewById(R.id.txt_subhp);
            subHolder.emailText = view.findViewById(R.id.txt_subemail);
            view.setTag(subHolder);
        } else {
            subHolder = (DownlineAdapterHolder) view.getTag();
        }

        subHolder.levelText.setText(childLists.getLevel());
        subHolder.nameText.setText(childLists.getPsName());
        subHolder.hpText.setText(childLists.getHandphone());
        subHolder.emailText.setText(childLists.getEmail());
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPos, int childPos) {
        return true;
    }

    private class DownlineAdapterHolder {
        TextView levelText, nameText, hpText, emailText;
    }
}
