package id.nata.access360.view.menuMain.model;

public class MenuResponse {
    int status ;
    String message;
    DataMenu dataMenu;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataMenu getDataMenu() {
        return dataMenu;
    }

    public void setDataMenu(DataMenu dataMenu) {
        this.dataMenu = dataMenu;
    }
}
