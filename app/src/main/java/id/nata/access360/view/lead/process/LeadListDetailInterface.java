package id.nata.access360.view.lead.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.lead.model.ListResponse;
import id.nata.access360.view.lead.model.ListResponseGroup;

public interface LeadListDetailInterface extends BaseInterface {
    void onSuccessGetResponseGroup(List<ListResponseGroup> listResponseGroup);

    void onFailedGetResponseGroup(String string);

    void onSuccessGetListResponse(List<ListResponse> listResponse);

    void onSuccessUpdateLeadLanjut(String message);

    void onFailedUpdateLeadLanjut(String message);

    void onFailedUpdateLeadLanjutPhone(String message);

    void onSuccessUpdateLeadSelesai(String message);

    void onFailedUpdateLeadSelesai(String message);

    void onFailedUpdateLeadSelesaiPhone(String message);

    void onFailedUpdateLeadFromAndro(String string);

    void onSuccessUpdateCallLeadWa(String message);

    void onFailedUpdateCallLeadWa(String message);

    void onSuccessUpdateCallLeadPhone(String message);

    void onFailedUpdateCallLeadPhone(String message);
}
