package id.nata.access360.view.lead.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.lead.adapter.LeadProjectListAdapter;
import id.nata.access360.view.lead.model.LeadProjectList;
import id.nata.access360.view.lead.process.LeadProjectInterface;
import id.nata.access360.view.lead.process.LeadProjectPresenter;
import okhttp3.internal.Util;

public class LeadProjectActivity extends BaseActivity implements LeadProjectInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_lead_project)
    ListView listLeadProject;

    @Inject
    LeadProjectPresenter leadProjectPresenter;

    private SessionManager sessionManager;
    private String psRef, username;

    private LeadProjectListAdapter leadProjectListAdapter;

    private List<LeadProjectList> leadProjectLists = new ArrayList<>();

    private String projectRef, projectName, totalLead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lead_project);
        ButterKnife.bind(this);

        leadProjectPresenter = new LeadProjectPresenter(this);
        leadProjectPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Project Lead");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listLeadProject.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                projectRef = leadProjectLists.get(i).getProjectRef();
                projectName = leadProjectLists.get(i).getProjectName();
                totalLead = leadProjectLists.get(i).getTotalLead();

                Intent intent = new Intent(LeadProjectActivity.this, LeadActivity.class);
                intent.putExtra("projectRef", projectRef);
                intent.putExtra("projectName", projectName);
                intent.putExtra("totalLead", totalLead);
                startActivity(intent);
            }
        });
    }

    private void getLeadProject(){
        leadProjectPresenter.getLeadProjectListSVC(psRef, username);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessGetLeadProjectList(List<LeadProjectList> leadProjectList) {
        leadProjectListAdapter = new LeadProjectListAdapter(this, leadProjectList);
        listLeadProject.setAdapter(leadProjectListAdapter);
        leadProjectLists = leadProjectList;
    }

    @Override
    public void onFailedGetLeadProjectList(String string) {
        Utils.showSnackbar(layout, string);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLeadProject();
    }
}
