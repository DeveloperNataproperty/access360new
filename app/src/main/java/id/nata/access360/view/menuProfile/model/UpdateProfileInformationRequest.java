package id.nata.access360.view.menuProfile.model;

public class UpdateProfileInformationRequest {
    String psRef, username, psName, kodeMember, handphone, birthPlace, birthDate;
    String countryCode, provinceCode, cityCode, postCode, address;
    String personalType, sexRef, religionRef, maritalStatus, occupationRef, jobTitleRef, educationRef, incomeRef, nationRef;
    String kodeMemberUpline;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPsName() {
        return psName;
    }

    public void setPsName(String psName) {
        this.psName = psName;
    }

    public String getKodeMember() {
        return kodeMember;
    }

    public void setKodeMember(String kodeMember) {
        this.kodeMember = kodeMember;
    }

    public String getHandphone() {
        return handphone;
    }

    public void setHandphone(String handphone) {
        this.handphone = handphone;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPersonalType() {
        return personalType;
    }

    public void setPersonalType(String personalType) {
        this.personalType = personalType;
    }

    public String getSexRef() {
        return sexRef;
    }

    public void setSexRef(String sexRef) {
        this.sexRef = sexRef;
    }

    public String getReligionRef() {
        return religionRef;
    }

    public void setReligionRef(String religionRef) {
        this.religionRef = religionRef;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getOccupationRef() {
        return occupationRef;
    }

    public void setOccupationRef(String occupationRef) {
        this.occupationRef = occupationRef;
    }

    public String getJobTitleRef() {
        return jobTitleRef;
    }

    public void setJobTitleRef(String jobTitleRef) {
        this.jobTitleRef = jobTitleRef;
    }

    public String getEducationRef() {
        return educationRef;
    }

    public void setEducationRef(String educationRef) {
        this.educationRef = educationRef;
    }

    public String getIncomeRef() {
        return incomeRef;
    }

    public void setIncomeRef(String incomeRef) {
        this.incomeRef = incomeRef;
    }

    public String getNationRef() {
        return nationRef;
    }

    public void setNationRef(String nationRef) {
        this.nationRef = nationRef;
    }

    public String getKodeMemberUpline() {
        return kodeMemberUpline;
    }

    public void setKodeMemberUpline(String kodeMemberUpline) {
        this.kodeMemberUpline = kodeMemberUpline;
    }
}
