package id.nata.access360.view.downline.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.downline.model.DownlineList;
import id.nata.access360.view.downline.model.DownlineParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownlinePresenter implements BasePresenter<DownlineInterface> {

    @Inject
    NetworkService networkService;

    private DownlineInterface view;
    private Context context;

    public DownlinePresenter(Context context) {
        super();
        ((BaseApplication)context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void getDownlineSVC(String psRef, String username){
        view.showLoader();

        DownlineParam param = new DownlineParam();
        param.setPsRef(psRef);
        param.setUsername(username);

        Call<List<DownlineList>> callDownlineList = networkService.POST_DownlineList(Utils.modelToJson(param));
        callDownlineList.enqueue(new Callback<List<DownlineList>>() {
            @Override
            public void onResponse(Call<List<DownlineList>> call, Response<List<DownlineList>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccesGetDownline(response.body());
                    } else {
                        view.onEmptyGetDownline(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<DownlineList>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(DownlineInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
