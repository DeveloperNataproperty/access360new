package id.nata.access360.view.menuProfile.model;

public class DataOccupation {
    String occupationRef, occupationName;

    public String getOccupationRef() {
        return occupationRef;
    }

    public void setOccupationRef(String occupationRef) {
        this.occupationRef = occupationRef;
    }

    public String getOccupationName() {
        return occupationName;
    }

    public void setOccupationName(String occupationName) {
        this.occupationName = occupationName;
    }
}
