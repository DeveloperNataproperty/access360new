package id.nata.access360.view.login.process;


import id.nata.access360.base.BaseInterface;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface ForgotPasswordViewInterface extends BaseInterface {

    void onSuccess(String paramStrSuccessMessage);

    void onFailed(String paramStrErrorMessage);

}
