package id.nata.access360.view.history.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.history.model.PersonalLeadHistoryData;

public interface PersonalLeadHistoryInterface extends BaseInterface {

    void onSuccesGetPersonalLeadList(List<PersonalLeadHistoryData> callPersonalLeadHistory);

    void onFailedGetPersonalLeadList(String message);
}
