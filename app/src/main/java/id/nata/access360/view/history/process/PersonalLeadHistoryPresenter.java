package id.nata.access360.view.history.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.history.model.PersonalLeadHistoryData;
import id.nata.access360.view.history.model.PersonalLeadHistoryParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalLeadHistoryPresenter implements BasePresenter<PersonalLeadHistoryInterface> {

    @Inject
    NetworkService networkService;

    private PersonalLeadHistoryInterface view;
    private Context context;

    public PersonalLeadHistoryPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void getPersonalLeadHistorySVC(String psRef, String username, String projectRef){
        view.showLoader();

        PersonalLeadHistoryParam param = new PersonalLeadHistoryParam();
        param.setPsRef(psRef);
        param.setUsername(username);
        param.setProjectRef(projectRef);

        Call<List<PersonalLeadHistoryData>> callPersonalLeadHistorySVC = networkService.GET_PersonalLeadHistory(Utils.modelToJson(param));
        callPersonalLeadHistorySVC.enqueue(new Callback<List<PersonalLeadHistoryData>>() {
            @Override
            public void onResponse(Call<List<PersonalLeadHistoryData>> call, Response<List<PersonalLeadHistoryData>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccesGetPersonalLeadList(response.body());
                    } else {
                        view.onFailedGetPersonalLeadList(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<PersonalLeadHistoryData>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(PersonalLeadHistoryInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
