package id.nata.access360.view.history.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.history.adapter.CallHistoryAdapter;
import id.nata.access360.view.history.model.CallHistoryData;
import id.nata.access360.view.history.process.CallHistoryInterface;
import id.nata.access360.view.history.process.CallHistoryPresenter;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;

public class CallHistoryActivity extends BaseActivity implements CallHistoryInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_call_history)
    ListView listCallHistory;

    @Inject
    CallHistoryPresenter callHistoryPresenter;

    private SessionManager sessionManager;
    private String psRef, leadRef, projectName;

    private CallHistoryAdapter callHistoryAdapter;
    private List<CallHistoryData> callHistoryDataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_history);
        ButterKnife.bind(this);

        callHistoryPresenter = new CallHistoryPresenter(this);
        callHistoryPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);

        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                leadRef = getIntent().getStringExtra("leadRef");
                projectName = getIntent().getStringExtra("projectName");
            }
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(projectName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getCallHistory();
    }

    private void getCallHistory() {
        callHistoryPresenter.getListCallHistory(psRef, leadRef);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessGetCallHistory(List<CallHistoryData> callHistoryData) {
        callHistoryAdapter = new CallHistoryAdapter(this, callHistoryData);
        listCallHistory.setAdapter(callHistoryAdapter);

        callHistoryDataList = callHistoryData;
    }

    @Override
    public void onFailedGetCallHistory(String string) {
        Utils.showSnackbar(layout, string);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(CallHistoryActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
