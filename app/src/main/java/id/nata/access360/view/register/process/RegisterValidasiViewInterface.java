package id.nata.access360.view.register.process;


import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.login.model.LoginData;

/**
 * Created by "dwist14"
 * on Sep 9/7/2017 14:50.
 * Project : Napro
 */

public interface RegisterValidasiViewInterface extends BaseInterface {

    void onSuccess(LoginData data, String message);

    void onSuccessReSendCode(String message);

    void onFailedReSendCode(String message);

    void onSuccessExDate(String message);

    void onFailed(String paramStrErrorMessage);

    void onCodeFailed (String message);

    void showLoaderDialog(String message);

    void dismissLoaderDialog();
}
