package id.nata.access360.view.menuProfile.process;

import android.content.Context;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.menuProfile.model.CityModel;
import id.nata.access360.view.menuProfile.model.CountryModel;
import id.nata.access360.view.menuProfile.model.DataPersonal;
import id.nata.access360.view.menuProfile.model.ProfileACResponse;
import id.nata.access360.view.menuProfile.model.ProfileInfoResponse;
import id.nata.access360.view.menuProfile.model.ProvinceModel;
import id.nata.access360.view.menuProfile.model.UpdateProfileInformationRequest;
import id.nata.access360.view.menuProfile.model.UpdateProfileInformationResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileACPresenter implements BasePresenter<ProfileACViewInterface> {

    @Inject
    NetworkService networkService;

    private ProfileACViewInterface view;
    private Context mContext;

    public ProfileACPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.mContext = context;
    }

    public void GETProfileInfoSvc(String strPsRef, String strUsername) {
        view.showLoader();

        DataPersonal param = new DataPersonal();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);

        Call<ProfileInfoResponse> callInfo = networkService.GET_ProfileInformation(Utils.modelToJson(param));
        callInfo.enqueue(new Callback<ProfileInfoResponse>() {
            @Override
            public void onResponse(Call<ProfileInfoResponse> call, Response<ProfileInfoResponse> response) {
//                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccesProfileInfo(response.body().getDataPersonal());
                    } else {
                        view.onFailed(mContext.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ProfileInfoResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void GETListCountrySvc(String jsonIn) {
//        view.showLoader();

        Call<List<CountryModel>> callList = networkService.GET_ListCountry(jsonIn);
        callList.enqueue(new Callback<List<CountryModel>>() {
            @Override
            public void onResponse(Call<List<CountryModel>> call, Response<List<CountryModel>> response) {
//                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccesCountry(response.body());
                    } else {
                        view.onFailed(mContext.getString(R.string.no_data_available));
                    }
                }else{
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<CountryModel>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void GETListProvinceSvc(String countryCode) {
//        view.showLoader();

        ProvinceModel param = new ProvinceModel();
        param.setCountryCode(countryCode);

        Call<List<ProvinceModel>> callList = networkService.GET_ListProvince(Utils.modelToJson(param));
        callList.enqueue(new Callback<List<ProvinceModel>>() {
            @Override
            public void onResponse(Call<List<ProvinceModel>> call, Response<List<ProvinceModel>> response) {
//                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccesProvince(response.body());
                    } else {
                        view.onFailed(mContext.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<ProvinceModel>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void GETListCitySvc(String countryCode, String provinceCode) {
//        view.showLoader();

        CityModel param = new CityModel();
        param.setCountryCode(countryCode);
        param.setProvinceCode(provinceCode);

        Call<List<CityModel>> callCity = networkService.GET_ListCity(Utils.modelToJson(param));
        callCity.enqueue(new Callback<List<CityModel>>() {
            @Override
            public void onResponse(Call<List<CityModel>> call, Response<List<CityModel>> response) {
//                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccesCity(response.body());
                    } else {
                        view.onFailed(mContext.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<CityModel>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void GETProfileLookupSvc(String jsonIn) {
//        view.showLoader();
        Call<ProfileACResponse> callProf = networkService.GET_ProfileLookup(jsonIn);
        callProf.enqueue(new Callback<ProfileACResponse>() {
            @Override
            public void onResponse(Call<ProfileACResponse> call, Response<ProfileACResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        view.onSuccess(response.body().getDataType(), response.body().getDataSex(), response.body().getDataReligion(),
                                response.body().getDataMaritalStatus(), response.body().getDataOccupation(), response.body().getDataJobTitle(),
                                response.body().getDataEducation(), response.body().getDataIncome(), response.body().getDataNation());
                    } else {
                        view.onFailed(mContext.getString(R.string.no_data_available));
                    }
                }else{
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<ProfileACResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void POSTUpdateProfileInfoSvc(String strPsRef, String strUsername, String strPsName, String strKdMember, String strHp, String strBirthPlace,
                                         String strBirthDate, String strCountryCode, String strProvinceCode, String strCityCode, String strPostCode,
                                         String strAddress, String strPersonalType, String strSexRef, String strReligionRef, String strMaritalStatus,
                                         String strOccupationRef, String strJobTitleRef, String strEducationRef, String strIncomeRef, String strNationRef,
                                         String strKodeMemberUpline) {
        view.showLoader();

        UpdateProfileInformationRequest param = new UpdateProfileInformationRequest();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);
        param.setPsName(strPsName);
        param.setKodeMember(strKdMember);
        param.setHandphone(strHp);
        param.setBirthPlace(strBirthPlace);
        param.setBirthDate(strBirthDate);
        param.setCountryCode(strCountryCode);
        param.setProvinceCode(strProvinceCode);
        param.setCityCode(strCityCode);
        param.setPostCode(strPostCode);
        param.setAddress(strAddress);
        param.setPersonalType(strPersonalType);
        param.setSexRef(strSexRef);
        param.setReligionRef(strReligionRef);
        param.setMaritalStatus(strMaritalStatus);
        param.setOccupationRef(strOccupationRef);
        param.setJobTitleRef(strJobTitleRef);
        param.setEducationRef(strEducationRef);
        param.setIncomeRef(strIncomeRef);
        param.setNationRef(strNationRef);
        param.setKodeMemberUpline(strKodeMemberUpline);

        Call<UpdateProfileInformationResponse> callUpdateProfile = networkService.POST_UpdateProfileInformation(Utils.modelToJson(param));
        callUpdateProfile.enqueue(new Callback<UpdateProfileInformationResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileInformationResponse> call, Response<UpdateProfileInformationResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        view.onSuccesProfileInfo(response.body().getDataPersonal());
                    } else {
                        view.onFailed(mContext.getString(R.string.update_failed));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileInformationResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });

    }

    @Override
    public void setupView(ProfileACViewInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
