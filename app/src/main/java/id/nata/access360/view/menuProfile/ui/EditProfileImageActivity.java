package id.nata.access360.view.menuProfile.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.Hashtable;
import java.util.Map;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.menuProfile.process.ProfileImageInterface;
import id.nata.access360.view.menuProfile.process.ProfileImagePresenter;

/**
 * Created by UserModel on 5/6/2016.
 */
public class EditProfileImageActivity extends BaseActivity implements ProfileImageInterface {

    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    public static final String PREF_NAME = "pref";

    private String KEY_MEMBERREF = "memberRef";
    private String KEY_IMAGE = "image";

    String psRef, imagePath;

    SharedPreferences sharedPreferences;
    private Bitmap bitmap;

    @Inject
    ProfileImagePresenter profileImagePresenter;

    private SessionManager sessionManager;
    CropImageView cropImageView;
    Button upload;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_image);
        ButterKnife.bind(this);

        profileImagePresenter = new ProfileImagePresenter(this);
        profileImagePresenter.setupView(this);

        sessionManager = new SessionManager(this);
        psRef = sessionManager.getStringFromSP(General.PS_REF);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Crop Foto");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cropImageView = (CropImageView) findViewById(R.id.CropImageView);
        upload = (Button) findViewById(R.id.btn_upload);

        Intent intent = getIntent();
        imagePath = intent.getStringExtra("pathImage");
        //bitmap = (Bitmap) intent.getParcelableExtra("Image");

        Log.d("imagePath", "" + imagePath);
        /*byte[] byteArray = getIntent().getByteArrayExtra("Image");
        bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);*/
        cropImageView.setFixedAspectRatio(true);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                cropImageView.setAspectRatio(370, 503);
            }
        }, 500);
        cropImageView.setImageBitmap(decodeSampledBitmapFromResource(imagePath, 500, 500));

        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = cropImageView.getCroppedImage(500, 500);
                if (bitmap != null)
                    cropImageView.setImageBitmap(bitmap);
                cropImageView.setVisibility(View.GONE);
                upload.setVisibility(View.GONE);

                uploadImage();
            }
        });

    }

    public static Bitmap decodeSampledBitmapFromResource(String resId, int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

//    private void uploadImage() {
//        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, WebService.updateImageProfileSvc(),
//                new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        Log.d("TAG", "Upload image " + response.toString());
//                        loading.dismiss();
//
//                        try {
//                            JSONObject jo = new JSONObject(response);
//                            int status = jo.getInt("status");
//
//                            if (status == 200) {
//                                //Showing toast message of the response
//                                /*Intent intent = new Intent(EditProfileImageActivity.this, EditProfileActivity.class);
//                                EditProfileActivity.getInstance().finish();
//                                startActivity(intent);*/
//                                finish();
//                                Toast.makeText(EditProfileImageActivity.this, "Upload Profile Succes", Toast.LENGTH_LONG).show();
//
//                            } else {
//                                Toast.makeText(EditProfileImageActivity.this, "Upload Failed", Toast.LENGTH_LONG).show();
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        //Dismissing the progress dialog
//                        loading.dismiss();
//                        cropImageView.setVisibility(View.VISIBLE);
//                        upload.setVisibility(View.VISIBLE);
//                        NetworkResponse networkResponse = error.networkResponse;
//                        if (networkResponse != null) {
//                            Log.d("Volley", "Error. HTTP Status Code:" + networkResponse.statusCode);
//                        }
//                        if (error instanceof TimeoutError) {
//                            Log.d("Volley", "TimeoutError");
//                        } else if (error instanceof NoConnectionError) {
//                            Log.d("Volley", "NoConnectionError");
//                        } else if (error instanceof AuthFailureError) {
//                            Log.d("Volley", "AuthFailureError");
//                        } else if (error instanceof ServerError) {
//                            Log.d("Volley", "ServerError");
//                        } else if (error instanceof NetworkError) {
//                            Log.d("Volley", "NetworkError");
//                        } else if (error instanceof ParseError) {
//                            Log.d("Volley", "ParseError");
//                        }
//                    }
//                }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                //Converting Bitmap to String
//                String image = getStringImage(bitmap);
//
//                //Creating parameters
//                Map<String, String> params = new Hashtable<String, String>();
//
//                //Adding parameters
//                params.put(KEY_IMAGE, image);
//                params.put(KEY_MEMBERREF, memberRef);
//
//                Log.d("Param", memberRef);
//
//                //returning parameters
//                return params;
//            }
//        };
//
//        //Creating a Request Queue
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//
//        //Adding request to the queue
//        requestQueue.add(stringRequest);
//    }

    private void uploadImage(){
        String image = getStringImage(bitmap);
        profileImagePresenter.POST_ImageProfileSVC(psRef, image);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_rotate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_rotate:
                cropImageView.rotateImage(90);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessUpload(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onFailedUpload(String message) {
        Utils.showSnackbar(layout, message);
    }
}
