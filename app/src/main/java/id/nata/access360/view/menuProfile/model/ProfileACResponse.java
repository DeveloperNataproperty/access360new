package id.nata.access360.view.menuProfile.model;

import java.util.List;

public class ProfileACResponse {
    int status;
    String message;
    List<DataType> dataType;
    List<DataSex> dataSex;
    List<DataReligion> dataReligion;
    List<DataMaritalStatus> dataMaritalStatus;
    List<DataOccupation> dataOccupation;
    List<DataJobTitle> dataJobTitle;
    List<DataEducation> dataEducation;
    List<DataIncome> dataIncome;
    List<DataNation> dataNation;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DataType> getDataType() {
        return dataType;
    }

    public void setDataType(List<DataType> dataType) {
        this.dataType = dataType;
    }

    public List<DataSex> getDataSex() {
        return dataSex;
    }

    public void setDataSex(List<DataSex> dataSex) {
        this.dataSex = dataSex;
    }

    public List<DataReligion> getDataReligion() {
        return dataReligion;
    }

    public void setDataReligion(List<DataReligion> dataReligion) {
        this.dataReligion = dataReligion;
    }

    public List<DataMaritalStatus> getDataMaritalStatus() {
        return dataMaritalStatus;
    }

    public void setDataMaritalStatus(List<DataMaritalStatus> dataMaritalStatus) {
        this.dataMaritalStatus = dataMaritalStatus;
    }

    public List<DataOccupation> getDataOccupation() {
        return dataOccupation;
    }

    public void setDataOccupation(List<DataOccupation> dataOccupation) {
        this.dataOccupation = dataOccupation;
    }

    public List<DataJobTitle> getDataJobTitle() {
        return dataJobTitle;
    }

    public void setDataJobTitle(List<DataJobTitle> dataJobTitle) {
        this.dataJobTitle = dataJobTitle;
    }

    public List<DataEducation> getDataEducation() {
        return dataEducation;
    }

    public void setDataEducation(List<DataEducation> dataEducation) {
        this.dataEducation = dataEducation;
    }

    public List<DataIncome> getDataIncome() {
        return dataIncome;
    }

    public void setDataIncome(List<DataIncome> dataIncome) {
        this.dataIncome = dataIncome;
    }

    public List<DataNation> getDataNation() {
        return dataNation;
    }

    public void setDataNation(List<DataNation> dataNation) {
        this.dataNation = dataNation;
    }
}
