package id.nata.access360.view.menuProfile.model;

public class DataSex {
    String sexRef, sexName;

    public String getSexRef() {
        return sexRef;
    }

    public void setSexRef(String sexRef) {
        this.sexRef = sexRef;
    }

    public String getSexName() {
        return sexName;
    }

    public void setSexName(String sexName) {
        this.sexName = sexName;
    }
}
