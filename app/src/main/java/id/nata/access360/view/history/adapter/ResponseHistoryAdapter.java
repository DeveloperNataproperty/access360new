package id.nata.access360.view.history.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.history.model.ResponseHistoryData;

public class ResponseHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<ResponseHistoryData> list;
    private ResponseHistoryHolder holder;

    public ResponseHistoryAdapter(Context context, List<ResponseHistoryData> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_response_history,null);
            holder = new ResponseHistoryHolder();
            holder.nameText = (TextView) convertView.findViewById(R.id.txt_name);
            holder.responText = (TextView) convertView.findViewById(R.id.txt_respon);
            holder.tglText = (TextView) convertView.findViewById(R.id.txt_tgl);
            holder.notedTxt = (TextView) convertView.findViewById(R.id.txt_noted);

            convertView.setTag(holder);
        }else{
            holder = (ResponseHistoryHolder) convertView.getTag();
        }

        ResponseHistoryData responseHistoryList = list.get(pos);
        holder.nameText.setText(responseHistoryList.getMemberName());
        holder.responText.setText(responseHistoryList.getResponseName());
        holder.tglText.setText(responseHistoryList.getInputTime());
        holder.notedTxt.setText(responseHistoryList.getNoted());

        return convertView;
    }

    private class ResponseHistoryHolder {
        TextView nameText, responText, tglText, notedTxt;
    }
}
