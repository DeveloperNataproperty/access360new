package id.nata.access360.view.history.process;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.history.model.ResponseHistoryData;
import id.nata.access360.view.history.model.HistoryParam;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResponseHistoryPresenter implements BasePresenter<ResponseHistoryInterface> {

    @Inject
    NetworkService networkService;

    private ResponseHistoryInterface view;
    private Context context;

    public ResponseHistoryPresenter(Context context) {
        super();
        ((BaseApplication) context.getApplicationContext()).getDeps().inject(this);
        this.context = context;
    }

    public void getListResponseHistorySVC(String psRef, String leadRef){
        view.showLoader();

        HistoryParam param = new HistoryParam();
        param.setPsRef(psRef);
        param.setLeadRef(leadRef);

        Call<List<ResponseHistoryData>> callResponseHistory = networkService.GET_ResponseHistory(Utils.modelToJson(param));
        callResponseHistory.enqueue(new Callback<List<ResponseHistoryData>>() {
            @Override
            public void onResponse(Call<List<ResponseHistoryData>> call, Response<List<ResponseHistoryData>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccessGetResponseHistory(response.body());
                    } else {
                        view.onFailedGetResponseHistory(context.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(context.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<ResponseHistoryData>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(context.getResources().getString(R.string.network_error));
            }
        });
    }

    @Override
    public void setupView(ResponseHistoryInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
