package id.nata.access360.view.nup.process;

import java.util.List;

import id.nata.access360.base.BaseInterface;
import id.nata.access360.view.nup.model.ListNupDetail;

/**
 * Created by NATA on 2/14/19.
 */

public interface ListNupDetailInterface extends BaseInterface {
    void onSuccessGetNupDetail(List<ListNupDetail> listNupDetails);

    void onFailedGetNupDetail(String string);
}
