package id.nata.access360.view.lead.model;

public class ListResponseGroup {

    private String responseGroupRef;
    private String responseGroupName;

    public String getResponseGroupRef() {
        return responseGroupRef;
    }

    public void setResponseGroupRef(String responseGroupRef) {
        this.responseGroupRef = responseGroupRef;
    }

    public String getResponseGroupName() {
        return responseGroupName;
    }

    public void setResponseGroupName(String responseGroupName) {
        this.responseGroupName = responseGroupName;
    }
}
