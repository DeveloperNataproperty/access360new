package id.nata.access360.view.menuMain.model;

/**
 * Created by NATA on 2/9/19.
 */

public class ProjectMenu2Model {
    private int menuIcon;
    private String menuTitle;
    private String menuName;

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuTitle() {
        return menuTitle;
    }

    public void setMenuTitle(String menuTitle) {
        this.menuTitle = menuTitle;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }
}
