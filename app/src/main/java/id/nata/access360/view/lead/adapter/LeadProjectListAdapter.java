package id.nata.access360.view.lead.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import id.nata.access360.R;
import id.nata.access360.view.lead.model.LeadProjectList;

public class LeadProjectListAdapter extends BaseAdapter {

    private Context context;
    private List<LeadProjectList> list;
    private LeadProjectListHolder holder;

    public LeadProjectListAdapter(Context context, List<LeadProjectList> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context)
                    .inflate(R.layout.item_list_lead_project,null);
            holder = new LeadProjectListHolder();
            holder.projectName = (TextView) convertView.findViewById(R.id.txt_project_name);
            holder.totalLead = (TextView) convertView.findViewById(R.id.txt_total_lead);

            convertView.setTag(holder);
        }else{
            holder = (LeadProjectListHolder) convertView.getTag();
        }

        LeadProjectList leadProjectList = list.get(pos);
        holder.projectName.setText(leadProjectList.getProjectName());
        holder.totalLead.setText(leadProjectList.getTotalLead());

        return convertView;
    }

    private class LeadProjectListHolder {
        TextView projectName, totalLead;
    }
}
