package id.nata.access360.view.menuProfile.model;

public class DataEducation {
    String educationRef, educationName;

    public String getEducationRef() {
        return educationRef;
    }

    public void setEducationRef(String educationRef) {
        this.educationRef = educationRef;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }
}
