package id.nata.access360.view.hubungiKami.model;

public class ContactUsParam {
    private String psRef;
    private String message;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
