package id.nata.access360.view.nup.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.nata.access360.R;
import id.nata.access360.base.BaseActivity;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.config.General;
import id.nata.access360.config.SessionManager;
import id.nata.access360.helper.Utils;
import id.nata.access360.view.menuMain.ui.ProjectMenu2Activity;
import id.nata.access360.view.nup.adapter.NupAdapter;
import id.nata.access360.view.nup.model.StatusNup;
import id.nata.access360.view.nup.model.StatusNupParam;
import id.nata.access360.view.nup.process.StatusNupInterface;
import id.nata.access360.view.nup.process.StatusNupPresenter;

public class NupActivity extends BaseActivity implements StatusNupInterface {

    @BindView(R.id.layout)
    View layout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.list_nup_status)
    ListView listStatusNup;

    @Inject
    StatusNupPresenter statusNupPresenter;

    private NupAdapter nupAdapter;
    private SessionManager sessionManager;
    private String psRef, username;
    List<StatusNup> listStatusNups = new ArrayList<StatusNup>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nup);
//        ((BaseApplication) getApplication()).getDeps().inject(this);
        ButterKnife.bind(this);

        statusNupPresenter = new StatusNupPresenter(this);
        statusNupPresenter.setupView(this);

        sessionManager = new SessionManager(getApplicationContext());
        psRef = sessionManager.getStringFromSP(General.PS_REF);
        username = sessionManager.getStringFromSP(General.EMAIL);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.status_nup));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getStatusNup();

        listStatusNup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                psRef = listStatusNups.get(i).getStatusRef();

                Intent intent = new Intent(NupActivity.this, NupListActivity.class);
                intent.putExtra("psRefKey", psRef);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_top_right, menu);
        return true;
    }

    private void getStatusNup(){

        statusNupPresenter.getStatusNupSVC(psRef, username);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.action_top_right:
                Intent intentProjectMenu = new Intent(NupActivity.this, ProjectMenu2Activity.class);
                intentProjectMenu.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intentProjectMenu);
                break;
        }
        return true;
    }

    @Override
    public void showLoader() {
        viewLoader();
    }

    @Override
    public void dismissLoader() {
        hiddenLoader();
    }

    @Override
    public void onNetworkError(String appErrorMessage) {
        Utils.showSnackbar(layout, appErrorMessage);
    }

    @Override
    public void onSuccessGetNup(List<StatusNup> statusNup) {
        setAdapterStatusNup(statusNup);
    }

    @Override
    public void onFailedGetNup(String string) {
        Utils.showSnackbar(layout, string);
    }

    private void setAdapterStatusNup(List<StatusNup> statusNup){
        nupAdapter = new NupAdapter(this, statusNup);
        listStatusNup.setAdapter(nupAdapter);
        listStatusNups = statusNup;
    }
}
