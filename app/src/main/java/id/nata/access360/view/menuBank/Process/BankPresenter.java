package id.nata.access360.view.menuBank.Process;

import android.content.Context;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import id.nata.access360.R;
import id.nata.access360.base.BaseApplication;
import id.nata.access360.base.BasePresenter;
import id.nata.access360.config.General;
import id.nata.access360.helper.Utils;
import id.nata.access360.network.NetworkService;
import id.nata.access360.view.menuBank.Model.BankInformationData;
import id.nata.access360.view.menuBank.Model.BankInformationParam;
import id.nata.access360.view.menuBank.Model.BankInformationResponse;
import id.nata.access360.view.menuBank.Model.BankListData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by NATA on 2/12/19.
 */

public class BankPresenter implements BasePresenter<BankInterface> {


    @Inject
    NetworkService networkService;

    private BankInterface view;
    private Context mContext;

    public BankPresenter(Context context){
        super();
        ((BaseApplication) context).getDeps().inject(this);
        this.mContext = context;
    }

    public void getBankInformationSVC(String strPsRef, String strUsername){
        view.showLoader();

        BankInformationParam param = new BankInformationParam();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);

        Call<BankInformationResponse> callBankInformation = networkService.GET_BankInformation(Utils.modelToJson(param));
        callBankInformation.enqueue(new Callback<BankInformationResponse>() {
            @Override
            public void onResponse(Call<BankInformationResponse> call, Response<BankInformationResponse> response) {
//                view.dismissLoader();
                if (response.isSuccessful()){
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS){
                        view.onSuccessBankInfo(response.body().getDataBank(), response.body().getMessage());
                    } else if (response.body().getStatus() == General.API_REQUEST_FAILED){
                        view.onFailedBankInfo(response.body().getMessage());
                    } else {
                        view.onFailedBankInfo(response.body().getMessage());
                    }
                } else{
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<BankInformationResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void getBankListSVC(String jsonIn){
//        view.showLoader();
        Call<List<BankListData>> callBankList = networkService.GET_BankList(jsonIn);
        callBankList.enqueue(new Callback<List<BankListData>>() {
            @Override
            public void onResponse(Call<List<BankListData>> call, Response<List<BankListData>> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        view.onSuccess(response.body());
                    } else {
                        view.onFailed(mContext.getString(R.string.no_data_available));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<List<BankListData>> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });
    }

    public void POSTUpdatePersonalBank(String strPsRef, String strUsername, String strBankRef,
                                       String strBankAccNo, String strBankAccName, String strBankBranch) {
        view.showLoader();

        BankInformationData param = new BankInformationData();
        param.setPsRef(strPsRef);
        param.setUsername(strUsername);
        param.setBankRef(strBankRef);
        param.setBankAccNo(strBankAccNo);
        param.setBankAccName(strBankAccName);
        param.setBankBranch(strBankBranch);

        Call<BankInformationResponse> callUpdateBank = networkService.POST_UpdatePersonalBank(Utils.modelToJson(param));
        callUpdateBank.enqueue(new Callback<BankInformationResponse>() {
            @Override
            public void onResponse(Call<BankInformationResponse> call, Response<BankInformationResponse> response) {
                view.dismissLoader();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == General.API_REQUEST_SUCCESS) {
                        Toast.makeText(mContext, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        view.onSuccessBankInfo(response.body().getDataBank(), response.body().getMessage());
                    } else {
                        view.onFailed(mContext.getString(R.string.update_failed));
                    }
                } else {
                    view.onNetworkError(mContext.getResources().getString(R.string.network_error));
                }
            }

            @Override
            public void onFailure(Call<BankInformationResponse> call, Throwable t) {
                view.dismissLoader();
                view.onNetworkError(mContext.getResources().getString(R.string.network_error));
            }
        });

    }

    @Override
    public void setupView(BankInterface view) {
        this.view = view;
    }

    @Override
    public void clearView() {
        view = null;
    }
}
