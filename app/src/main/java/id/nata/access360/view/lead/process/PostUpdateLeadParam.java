package id.nata.access360.view.lead.process;

public class PostUpdateLeadParam {
    private String psRef;
    private String username;
    private String leadRef;
    private String responseGroupRef;
    private String responseRef;
    private String isFinish;
    private String noted;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }

    public String getResponseGroupRef() {
        return responseGroupRef;
    }

    public void setResponseGroupRef(String responseGroupRef) {
        this.responseGroupRef = responseGroupRef;
    }

    public String getResponseRef() {
        return responseRef;
    }

    public void setResponseRef(String responseRef) {
        this.responseRef = responseRef;
    }

    public String getIsFinish() {
        return isFinish;
    }

    public void setIsFinish(String isFinish) {
        this.isFinish = isFinish;
    }

    public String getNoted() {
        return noted;
    }

    public void setNoted(String noted) {
        this.noted = noted;
    }
}
