package id.nata.access360.view.note.model;

public class NoteExpandParam {
    private String psRef;
    private String leadRef;

    public String getPsRef() {
        return psRef;
    }

    public void setPsRef(String psRef) {
        this.psRef = psRef;
    }

    public String getLeadRef() {
        return leadRef;
    }

    public void setLeadRef(String leadRef) {
        this.leadRef = leadRef;
    }
}
